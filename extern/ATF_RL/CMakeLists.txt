cmake_minimum_required(VERSION 2.8.11)
project(ATF_RL)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -fPIC -ftemplate-depth=1024")

# OpenCL
find_package(OpenCL REQUIRED)
include_directories(${OpenCL_INCLUDE_DIR})
link_directories(${OpenCL_LIBRARY})

# Python
find_package(PythonLibs 2.7 REQUIRED)
include_directories(${PYTHON_INCLUDE_DIR})
link_directories(${PYTHON_LIBRARY})

# argparse
include_directories(${ARGPARSE_INCLUDE_DIR})

include_directories(include)

file(GLOB_RECURSE HEADER_FILES include/*.hpp)
file(GLOB_RECURSE SOURCE_FILES src/abort_conditions.cpp src/ocl_wrapper.cpp src/op_wrapper.cpp src/process_wrapper_helper.cpp src/tp_value.cpp src/tp_value_node.cpp src/tuner_with_constraints.cpp src/tuner_without_constraints.cpp src/value_type.cpp)

add_library(atf_rl SHARED ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(atf_rl
        pthread
        z
        dl
        tinfo
        )
get_filename_component(ATF_RL_INCLUDE_DIR_ABSOLUTE . ABSOLUTE)
set(ATF_RL_INCLUDE_DIRS ${ATF_RL_INCLUDE_DIR_ABSOLUTE} ${OpenCL_INCLUDE_DIR} ${PYTHON_INCLUDE_DIR} ${ARGPARSE_INCLUDE_DIR} PARENT_SCOPE)
if (CUDA_FOUND)
    target_link_libraries(atf_rl ${CUDA_CUDA_LIBRARY} ${CUDA_CUDART_LIBRARY} ${CUDA_NVRTC_LIBRARY})
endif()

add_executable(ocl_md_hom_initial_process_wrapper src/ocl_md_hom_process_wrapper.cpp ${SOURCE_FILES})
target_compile_definitions(ocl_md_hom_initial_process_wrapper PRIVATE BUFFER_TYPE=float BUFFER_TYPE_ID=0 MD_HOM_INITIAL)
target_link_libraries(ocl_md_hom_initial_process_wrapper ${OpenCL_LIBRARY})
target_link_libraries(ocl_md_hom_initial_process_wrapper
        pthread
        z
        dl
        tinfo
        )

add_executable(ocl_md_hom_new_process_wrapper src/ocl_md_hom_process_wrapper.cpp ${SOURCE_FILES})
target_compile_definitions(ocl_md_hom_new_process_wrapper PRIVATE BUFFER_TYPE=float BUFFER_TYPE_ID=0 MD_HOM_NEW)
target_link_libraries(ocl_md_hom_new_process_wrapper ${OpenCL_LIBRARY})
target_link_libraries(ocl_md_hom_new_process_wrapper
        pthread
        z
        dl
        tinfo
        )