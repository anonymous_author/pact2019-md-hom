//
// Created by   on 15.02.18.
//

#ifndef MA__PROCESS_WRAPPER_HELPER_HPP
#define MA__PROCESS_WRAPPER_HELPER_HPP

#include <netinet/in.h>
#include <sys/un.h>
#include <string>

namespace atf {
namespace cf {

enum PROCESS_WRAPPER_TYPE {
    NONE, LOCAL, REMOTE
};
enum CHECK_INTERVAL {
    CHECK_NONE, CHECK_FINAL, CHECK_ALL
};
typedef struct {
    PROCESS_WRAPPER_TYPE type;
    std::string command_prefix;
    std::string result_file_name;
    CHECK_INTERVAL check_interval;
    std::string buffer_fill_algorithm;
} process_wrapper_info;

typedef union {
    struct sockaddr_in addr_in;
    struct sockaddr_un addr_un;
} addr;

enum TIMEOUT_TYPE {
    FACTOR, ABSOLUTE
};
typedef union {
    float factor;
    unsigned long long absolute;
} timeout_value;
typedef struct {
    TIMEOUT_TYPE type;
    timeout_value value;
} timeout;

// for tuning stencils with process wrapper
struct WEIGHTS_33 {
    float values[3][3];
};
struct WEIGHTS_333 {
    float values[3][3][3];
};
struct WEIGHTS_55 {
    float values[5][5];
};


void ns_sleep(unsigned long long ns);

}
}
#endif //MA__PROCESS_WRAPPER_HELPER_HPP
