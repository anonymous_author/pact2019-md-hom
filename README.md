>
> # NEW
> 
> New Experimental Results for: 
>
> 1\) **MDH vs. TVM**
>    
>    We provide new [experimental results](https://gitlab.com/anonymous_author/pact2019-md-hom/tree/master/evaluation/TVM) for our MDH approach as compared to the TVM framework. To make comparison challenging for us, we compare the performance of our generated and auto-tuned code to the TVM's original sample of "Tuning High Performance Convolution on NVIDIA GPUs" on the sample's original input size as provided in [their GitHub repository](https://docs.tvm.ai/tutorials/autotvm/tune_conv2d_cuda.html). We use for both, TVM and MDH, the same auto-tuning time: evaluating 10.000 configurations (note: in TVM, the tuning time is set as number of configurations to evaluate, rather than as a time interval, e.g., 5h).
> 
>    On NVIDIA Tesla V100-SXM2-16GB GPU, our MDH approach reaches a speedup of 2.75x as compared to TVM, because we provide a more-general code generation approach than TVM (e.g., a multi-layered, multi-dimensional tiling strategy). 
>    
>    
> 2\) **MDH vs. TACO**
>    
>    We provide new [experimental results](https://gitlab.com/anonymous_author/pact2019-md-hom/tree/master/evaluation/TACO) for our MDH approach as compared to the TACO framework using the example of "dense GEMM". With our MDH approach, we achieve speedups of >11x over TACO on Intel(R) Xeon(R) CPU E5-2640 v2 CPU. This is because TACO is optimized for sparse computations. 
>    We consider expressing and generating code for sparse computations as future work, because the MDH approach has to be slightly extended for expressing sparse computations.  
>    
>    
> 3\) **MDH for GEMM on ALL input sizes used in simaese neural network**
>    
>    We provide results for GEMM on NVIDIA Tesla V100-SXM2-16GB GPU for *all the input sizes* used in the original real-world siamese sample of the deep learning framework Caffe. The input sizes are listed in [pact2019-md-hom/evaluation/md_hom_new/gemm.cpp](https://gitlab.com/anonymous_author/pact2019-md-hom/blob/fb1bf0ba495ecc31d821b5b5d14117c1acacb9f0/evaluation/md_hom_new/gemm.cpp). We provide *better results* than all references for 15 of the in total 19 input sizes (the corresponding bars are highlighted green). For a *challenging comparison*, we provide for all references the best available runtime, e.g., for cuBLASLt's gemm routine, we have exhaustively tested all possible so-called "algorithm parameters" and present for each input size the results for the best-performing algorithm (which speedups the cuBLASLt's gemm routine up to 50x as compared to the algorithm parameter that NVIDIA uses per default for the routine).
>     
>    ![](plots/rebuttal_gemm_gpu.png)
>    
>    We provide results on Intel(R) Xeon(R) CPU E5-2640 v2 CPU for 10 input sizes of the siamese sample -- the performance results for the 9 remaining sizes will follow soon. Lift fails for all input sizes, because its search space has been pruned by the Lift expert with focus on power-of-two input sizes to make it automatically explorable. Note that we provide *competitive and sometimes even better performance than Intel MKL* for nearly all input sizes. Lower performance of our code as compared to MKL is often due to too small input sizes (e.g., IS12 where (M,N,K) = (64,2,10) ): this causes the OpenCL kernel start overhead to significantly influence runtime — we will solve this issue soon by providing our code generation approach also in OpenMP which for CPUs does not have a kernel start overhead like OpenCL.
>    
>    ![](plots/rebuttal_gemm_cpu.png)
>    
>    
> 4\) **Auto-tuning for average high performance over different input sizes**
>    
>    We provide performance results for our generated code when it is auto-tuned for an average high performance over different input size using the example of GEMV. We demonstrate the results for the two input sizes used in the Lift's original artifact implementation [48]. Here, i) "md_hom(IS)" denotes the performance of our generated code when it is specifically auto-tuned for the input size, and ii) "md_hom(avg)" denotes our code's performance when it is auto-tuned for an average high performance over different input sizes. Our code *provides higher performance than Lift and has competitive performance to MKL in all cases*. We will provide further experimental results soon, e.g., for GEMM. 
>    
>    |Intel CPU|NVIDIA GPU|
>    |-----------|------------|
>    |![](plots/rebuttal_gemv_gpu.png)|![](plots/rebuttal_gemv_cpu.png)|
>    
>    
>    
> --- 
> 
> **In case of any further requests for experimental results, please feel free to open an issue; we will try to promptly provide them.**
> 
> ---

# Generating Performance-Portable Code for Multi-and Many-Core Architectures

This preliminary artifact contains the workflow to reproduce the results shown in the paper *Generating Performance-Portable Code for Multi-and Many-Core Architectures*. The reviewer is invited to perform the steps described below. In case of **any problems**, please feel free to **open an issue** in order to get in contact with the authors.

In addition to the application examples discussed in the paper (BLAS routines, stencil computations, data mining and machine learning), this repository also contains our preliminary implementations for several other applications, which confirm the wide applicability of our approach: Ensemble of Classifier Chains (ECC), Multi-Layer Perceptron (MLP), and Support Vector Machines (SVM). The source code for these examples can be found in the `preliminary` folder.

## Software Requirements

- an OpenCL driver and runtime environment
- OpenGL (libmesa)
- CMake 3.8 or higher
- a compiler supporting C++14 or higher
- Boost 1.56 or higher
- OpenSSL
- finger
- Python 2.7 (not Python 3.x)
- tabulate Python package
- OpenTuner 0.8.0
- Java 8 SDK
- Intel MKL
- NVIDIA cuBLAS
- NVIDIA cuDNN
- Tensor Comprehensions
- LLVM 4.0 or higher (only necessary when executing TVM benchmarks)

## Workflow

The workflow of this artifact is divided into three main steps: **installation**, **tuning**, and **benchmarking**. Note that the tuning step may take a long time, because for each routine and input size both md_hom and some of the references are tuned for several hours. To reduce the overall runtime of the artifact workflow, the tuning step can be omitted and the tuning results found on the system described in the paper are used instead. **Be aware that - in case the artifact is executed on devices different from the ones listed in the paper - the tuning step has to be exectued in order to achieve the best, and thus portable, performance. Omitting the tuning step and using our provided parameter values may cause suboptimal performance on a different device and, therefore, prevent the reviewer from reproducing the results shown in the paper.**

All experiments are compiled with the `-O3` flag. Additionally, for the Intel MKL experiments, we use the following flags, as advised by the [Intel Math Kernel Library Link Line Advisor](https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor):

`-Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl`

### Step 1: Installation

Before installing the artifact, the following dependencies have to be installed:

- **OpenCL driver and runtime:**
  
  Download and install the OpenCL driver and runtime from the vendor website of the utilized hardware.
  
- **OpenGL (libmesa)**:
  
  `sudo apt-get install libgl1-mesa-dev`
  
- **CMake 3.8 or higher**:

  Download CMake from the developer website:
  
  `wget https://cmake.org/files/v3.13/cmake-3.13.0-rc3-Linux-x86_64.sh`
  
  Install CMake locally. If asked if you want to include the subdirectory in the installation path, type `y`:
  
  `/bin/bash cmake-3.13.0-rc3-Linux-x86_64.sh`
  
  Make CMake available:
  
  ``export PATH=`pwd`/cmake-3.13.0-rc3-Linux-x86_64/bin:$PATH``
  
- **A compiler supporting C++14 or higher**:
  
  `sudo apt-get install gcc g++`

- **Boost 1.56 or higher**:

  `sudo apt-get install libboost-all-dev`
  
- **OpenSSL**:

  `sudo apt-get install libssl-dev`
  
- **finger**:

  `sudo apt-get install finger`

- **Python 2.7 (not Python 3.x)**:

  `sudo apt-get install python-dev python-pip`
  
- **tabulate Python package**:

  `sudo pip install tabulate`

- **OpenTuner 0.8.0**:

  OpenTuner has dependencies itself. The OpenTuner dependencies can be installed with:
  
  `sudo apt-get install sqlite3 libsqlite3-dev`
  
  Afterwards, install OpenTuner by executing:
  
  `sudo pip install opentuner`
  
- **Java 8 SDK**:

  The Oracle Java SDK can be installed as follows:
  
  ```
  sudo su
  echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee /etc/apt/sources.list.d/webupd8team-java.list
  echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
  apt-get update
  apt-get install oracle-java8-installer
  exit
  ```
  
  After installing Java, please restart your terminal session.
  
- **Intel MKL**:

  Intel MKL can be downloaded at [https://software.intel.com/en-us/mkl/choose-download/linux](https://software.intel.com/en-us/mkl/choose-download/linux). After registering, the Intel MKL library can be downloaded and installed using the provided installation scripts.


- **NVIDIA cuBLAS**:

  NVIDIA cuBLAS is bundled into NVIDIA CUDA Toolkit. The CUDA Toolkit can be downloaded at [https://developer.nvidia.com/cuda-downloads](https://developer.nvidia.com/cuda-downloads). Choose your preferred download type and follow the instructions to install NVIDIA CUDA Toolkit.

- **NVIDIA cuDNN**:

  NVIDIA cuDNN has to be manually installed into an existing NVIDIA CUDA Toolkit installation. The required steps can be found at [https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html).
  
- **Tensor Comprehensions**:

  Tensor Comprehensions can be installed as a conda package. Installation instructions for conda can be found at [https://conda.io/projects/conda/en/latest/user-guide/install/index.html](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). Afterwards, Tensor Comprehensions can be installed as follows:
  
  `conda install -y -c pytorch -c tensorcomp tensor comprehensions`
  
- **LLVM**:

  LLVM can be downloaded as pre-built binaries or source code including installation instructions from [https://releases.llvm.org/download.html](https://releases.llvm.org/download.html).
  
- **Installing the artifact**:
  
  Clone the artifact repository:
  
  `git clone https://gitlab.com/anonymous_author/pact2019-md-hom.git`
  
  Change into the artifact directory:
  
  `cd pact2019-md-hom`
  
  Edit the `environment.env` configuration file. Enter the OpenCL platform and device ids of the CPU and GPU device you wish to evaluate on. In case the system is not equipped with a GPU or you do not wish to evaluate on the CPU or GPU, remove the comment symbol (`#`) on the corresponding line. Make sure to set the LLVM_CONFIG variable in case you wish to execute the TVM benchmarks.
  
  ```
  # CPU Evaluation
  # uncomment if you do not want to evaluate on a CPU
  #export DISABLE_CPU=1
  # platform and device id of the OpenCL cpu to evaluate on
  export OCL_CPU_PLATFORM_ID=0
  export OCL_CPU_DEVICE_ID=0
  # MKL root directory
  export MKLROOT=/opt/intel/mkl
  
  # GPU Evaluation
  # uncomment if you do not want to evaluate on a GPU
  #export DISABLE_GPU=1
  # platform and device id of the OpenCL gpu to evaluate on
  export OCL_GPU_PLATFORM_ID=0
  export OCL_GPU_DEVICE_ID=0
  export CUDA_GPU_DEVICE_ID=0
  # workspace size for cuBLASLt
  export CUBLASLT_WORKSPACE_SIZE=5368709120
  
  # TVM
  # set the path to llvm-config to use for TVM (only necessary if evaluating on GPU)
  export LLVM_CONFIG=/path/to/llvm/config
  
  export ARTIFACT_ROOT=`pwd`
  ``` 
  
  Evaluate the `environment.env` configuration file:
  
  `source environment.env`
  
  In case you want to compare with COGENT, also adapt the `CUDA_ARCH` variable in `pact2019-md-hom/evaluation/cogent/Makefile` to match your NVIDIA GPU.


### Step 2: Tuning OR Using Default Values

The scripts for tuning or using default values are provided in two variants: 1) the ones for tuning and using default values on the cpu (containing `cpu` in their names), and 2) the equivalents on the gpu (containing `gpu` in their names). The reviewer is free to choose for wich device type to evaluate by executing only the scripts for the desired device type.

#### Step 2a: Tuning

Note that this step may take a long time, because for each routine and input size both md_hom and some of the references are tuned for several hours. To reduce the overall runtime of the artifact workflow, the tuning step can be omitted and the tuning results found on the system described in the paper are used instead. Continue with step 2b in case you want to omit the tuning step. **Be aware that - in case the artifact is executed on devices different from the ones listed in the paper - the tuning step has to be exectued in order to achieve the best, and thus portable, performance. Omitting the tuning step and using our provided parameter values may cause suboptimal performance on a different device and, therefore, prevent the reviewer from reproducing the results shown in the paper.**

- Tune md_hom:

  `scripts/tune_cpu_md_hom.sh`
  
  `scripts/tune_gpu_md_hom.sh`

- Tune references:

  `scripts/tune_cpu_references.sh`
  
  `scripts/tune_gpu_references.sh`

#### Step 2b: Use Default Values

- Execute the script to use the default values on CPU and/or GPU:

  `scripts/use_cpu_defaults.sh`
  
  `scripts/use_gpu_defaults.sh`

### Step 3: Benchmarking

- Execute md_hom:
  
  `scripts/run_cpu_md_hom.sh`
  
  `scripts/run_gpu_md_hom.sh`

- Execute references:
  
  `scripts/run_cpu_references.sh`
  
  `scripts/run_gpu_references.sh`

- Print results:
  
  `scripts/print_results.sh`
  
  The best found configurations for our implementations and the ones for the tunable references can be found in the `results` folder. Additionally, we provide the best found configurations for the hardware used in the paper in the `defaults` folder.
