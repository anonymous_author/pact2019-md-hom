#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  cd build/evaluation/md_hom_new &&
  mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/gemm_10x500x64_runtime &> /dev/null
  ./md_hom_new_gemm --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 10 500 64

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/gemm_1024x1024x1024_runtime &> /dev/null
  ./md_hom_new_gemm --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1024 1024 1024

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/gemv_4096x4096_runtime &> /dev/null
  ./md_hom_new_gemv --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4096 4096

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/gemv_8192x8192_runtime &> /dev/null
  ./md_hom_new_gemv --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 8192 8192

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/gaussian_static_224x224_runtime &> /dev/null
  ./md_hom_new_gaussian_static --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/gaussian_static_4096x4096_runtime &> /dev/null
  ./md_hom_new_gaussian_static --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/gaussian_dynamic_224x224_runtime &> /dev/null
  ./md_hom_new_gaussian_dynamic --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/gaussian_dynamic_4096x4096_runtime &> /dev/null
  ./md_hom_new_gaussian_dynamic --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/j3d7pt_256x256x256_runtime &> /dev/null
  ./md_hom_new_j3d7pt --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 254 254 254

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/j3d7pt_512x512x512_runtime &> /dev/null
  ./md_hom_new_j3d7pt --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 510 510 510

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/multi_channel_convolution_8x64x54x54x64_runtime &> /dev/null
  ./md_hom_new_multi_channel_convolution --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 8 64 54 54 64

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/multi_channel_convolution_1x512x5x5x512_runtime &> /dev/null
  ./md_hom_new_multi_channel_convolution --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1 512 5 5 512

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_gdab_efgc_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdab efgc --input-size 24 16 16 16 24 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_gdac_efgb_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdac efgb --input-size 24 16 16 16 24 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_gdbc_efga_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdbc efga --input-size 24 16 16 16 24 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_geab_dfgc_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims geab dfgc --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_geac_dfgb_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims geac dfgb --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_gebc_dfga_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gebc dfga --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_gfab_degc_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfab degc --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_gfac_degb_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfac degb --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/tc_abcdef_gfbc_dega_runtime &> /dev/null
  ./md_hom_new_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfbc dega --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/rl_*_runtime &> /dev/null
  ./md_hom_new_rl --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1024 1024

  printf "\n\nmd_hom tuning successful!\n"
} || {
  printf "\n\nmd_hom tuning failed!\n"
  exit 1
}