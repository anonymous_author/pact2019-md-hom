#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  cd build/evaluation/lift_stencil/artifact/ &&
  source environment.env &&
  export OCL_PLATFORM_ID=$OCL_GPU_PLATFORM_ID &&
  export OCL_DEVICE_ID=$OCL_GPU_DEVICE_ID &&
  {
    rm ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_224x224_runtime &> /dev/null
    ./scripts/run_workflow2_figure8.sh nvidia gaussian small &&
    mkdir -p ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
    cp benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/best.cl ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_224x224_kernel.cl &&
    hash=`grep -oP "(?<=// Low-level hash: ).*" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/best.cl` &&
    tmp=`grep -oP "(?<=$hash.cl -> )\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$ls_0;)\\d+" benchmarks/figure8/workflow2/gaussian/small/kepler/gaussianCl/summary.txt` &&
    printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1\n}" > ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_224x224_config.json
  }
  {
    rm ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_4096x4096_runtime &> /dev/null
    ./scripts/run_workflow2_figure8.sh nvidia gaussian large &&
    mkdir -p ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
    cp benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/best.cl ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_4096x4096_kernel.cl &&
    hash=`grep -oP "(?<=// Low-level hash: ).*" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/best.cl` &&
    tmp=`grep -oP "(?<=$hash.cl -> )\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$ls_0;)\\d+" benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl/summary.txt` &&
    printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1\n}" > ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gaussian_4096x4096_config.json
  }
  {
    rm ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_256x256x256_runtime &> /dev/null
    ./scripts/run_workflow2_figure8.sh nvidia j3d7pt small &&
    mkdir -p ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
    cp benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/best.cl ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_256x256x256_kernel.cl &&
    hash=`grep -oP "(?<=// Low-level hash: ).*" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/best.cl` &&
    tmp=`grep -oP "(?<=$hash.cl -> )\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    gs_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    ls_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;$ls_1;)\\d+" benchmarks/figure8/workflow2/j3d7pt/small/kepler/j3d7ptCl/summary.txt` &&
    printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"GLOBAL_SIZE_2\": $gs_2,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1,\n    \"LOCAL_SIZE_2\": $ls_2\n}" > ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_256x256x256_config.json
  }
  {
    rm ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_512x512x512_runtime &> /dev/null
    ./scripts/run_workflow2_figure8.sh nvidia j3d7pt large &&
    mkdir -p ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
    cp benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/best.cl ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_512x512x512_kernel.cl &&
    hash=`grep -oP "(?<=// Low-level hash: ).*" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/best.cl` &&
    tmp=`grep -oP "(?<=$hash.cl -> )\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    gs_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    ls_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;$ls_1;)\\d+" benchmarks/figure8/workflow2/j3d7pt/big/kepler/j3d7ptCl/summary.txt` &&
    printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"GLOBAL_SIZE_2\": $gs_2,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1,\n    \"LOCAL_SIZE_2\": $ls_2\n}" > ${ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/j3d7pt_512x512x512_config.json
  }

  # md_hom_initial
  cd ${ARTIFACT_ROOT}/build/evaluation/md_hom_initial
  mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gemm_10x500x64_runtime &> /dev/null
  ./md_hom_initial_gemm --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 10 500 64

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gemm_1024x1024x1024_runtime &> /dev/null
  ./md_hom_initial_gemm --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1024 1024 1024

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gemv_4096x4096_runtime &> /dev/null
  ./md_hom_initial_gemv --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4096 4096

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gemv_8192x8192_runtime &> /dev/null
  ./md_hom_initial_gemv --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 8192 8192

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gaussian_static_224x224_runtime &> /dev/null
  ./md_hom_initial_gaussian_static --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gaussian_static_4096x4096_runtime &> /dev/null
  ./md_hom_initial_gaussian_static --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gaussian_dynamic_224x224_runtime &> /dev/null
  ./md_hom_initial_gaussian_dynamic --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/gaussian_dynamic_4096x4096_runtime &> /dev/null
  ./md_hom_initial_gaussian_dynamic --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/j3d7pt_256x256x256_runtime &> /dev/null
  ./md_hom_initial_j3d7pt --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 254 254 254

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/j3d7pt_512x512x512_runtime &> /dev/null
  ./md_hom_initial_j3d7pt --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 510 510 510

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_gdab_efgc_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdab efgc --input-size 24 16 16 16 24 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_gdac_efgb_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdac efgb --input-size 24 16 16 16 24 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_gdbc_efga_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdbc efga --input-size 24 16 16 16 24 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_geab_dfgc_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims geab dfgc --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_geac_dfgb_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims geac dfgb --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_gebc_dfga_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gebc dfga --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_gfab_degc_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfab degc --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_gfac_degb_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfac degb --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/tc_abcdef_gfbc_dega_runtime &> /dev/null
  ./md_hom_initial_tc --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfbc dega --input-size 24 16 16 24 16 16 24

  rm -rf opentuner.*
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/rl_*_runtime &> /dev/null
  ./md_hom_initial_rl --mode tune --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1024 1024

  # TC
  cd ${ARTIFACT_ROOT}/build/evaluation/tensor_comprehensions
  mkdir -p $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_gdab_efgc* &> /dev/null
  # execute 20 iterations in separate tuner invocations to be able to save tuning results of failed tuning runs
  for g in `seq 1 20`
  do
    python tune_input_tccg_1.py
  done
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_gdac_efgb* &> /dev/null
  for g in `seq 1 20`
  do
    python tune_input_tccg_2.py
  done
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_gdbc_efga* &> /dev/null
  for g in `seq 1 20`
  do
    python tune_input_tccg_3.py
  done
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_geab_dfgc* &> /dev/null
  for g in `seq 1 20`
  do
    python tune_input_tccg_4.py
  done
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_geac_dfgb* &> /dev/null
  for g in `seq 1 20`
  do
    python tune_input_tccg_5.py
  done
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_gebc_dfga* &> /dev/null
  for g in `seq 1 20`
  do
    python tune_input_tccg_6.py
  done
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_gfab_degc* &> /dev/null
  for g in `seq 1 20`
  do
    python tune_input_tccg_7.py
  done
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_gfac_degb* &> /dev/null
  for g in `seq 1 20`
  do
    python tune_input_tccg_8.py
  done
  rm $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/tc_abcdef_gfbc_dega* &> /dev/null
  for g in `seq 1 20`
  do
    python tune_input_tccg_9.py
  done

  # TVM
  rm $ARTIFACT_ROOT/results/gpu/tvm/multi_channel_convolution_1x512x5x5x512* &> /dev/null
  python tune_conv2d_cuda.py

  printf "\n\nReference tuning successful!\n"
} || {
  printf "\n\nReference tuning failed!\n"
  exit 1
}