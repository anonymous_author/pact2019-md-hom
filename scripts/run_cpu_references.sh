#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_CPU_PLATFORM_ID?"Please set the environment variable OCL_CPU_PLATFORM_ID."}
if [ -z "$OCL_CPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_CPU_DEVICE_ID?"Please set the environment variable OCL_CPU_DEVICE_ID."}
if [ -z "$OCL_CPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  # Intel MKL
  cd $ARTIFACT_ROOT/build/evaluation/mkl &&
  mkdir -p ${ARTIFACT_ROOT}/results/cpu/mkl
  LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_gemm --input-size 10 500 64
  LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_gemm --input-size 1024 1024 1024
  LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_gemv --input-size 4096 4096
  LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_gemv --input-size 8192 8192

  # Intel MKL-JIT
  cd $ARTIFACT_ROOT/build/evaluation/mkl_jit &&
  mkdir -p ${ARTIFACT_ROOT}/results/cpu/mkl_jit
  LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_jit_gemm --input-size 10 500 64
  LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_jit_gemm --input-size 1024 1024 1024

  # Lift BLAS
  cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
  (
    export PARENT_ARTIFACT_ROOT=$ARTIFACT_ROOT
    export ARTIFACT_ROOT=`pwd`
    {
      lift/scripts/MatrixVector --iterations 210 --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel generated_kernels/gemv/gemv_N.cl -p $OCL_CPU_PLATFORM_ID -d $OCL_CPU_DEVICE_ID > gemv_lift_4096x4096.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemv_lift_4096x4096.log > ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/gemv_4096x4096_runtime
    }
    {
      lift/scripts/MatrixVector --iterations 210 --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel generated_kernels/gemv/gemv_N.cl -p $OCL_CPU_PLATFORM_ID -d $OCL_CPU_DEVICE_ID > gemv_lift_8192x8192.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemv_lift_8192x8192.log > ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/gemv_8192x8192_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_CPU_PLATFORM_ID -d $OCL_CPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 10 -s 500 -s 64 > gemm_lift_10x500x64.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_10x500x64.log > ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/gemm_10x500x64_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_CPU_PLATFORM_ID -d $OCL_CPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 1024 -s 1024 -s 1024 > gemm_lift_1024x1024x1024.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_1024x1024x1024.log > ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/gemm_1024x1024x1024_runtime
    }
  )

  # Intel MKL-DNN
  cd $ARTIFACT_ROOT/build/evaluation/mkldnn &&
  mkdir -p ${ARTIFACT_ROOT}/results/cpu/mkldnn
  ./mkldnn_gaussian --input-size 220 220
  ./mkldnn_gaussian --input-size 4092 4092
  ./mkldnn_multi_channel_convolution --input-size 8 64 54 54 64

  # Lift stencil
  cd $ARTIFACT_ROOT/build/evaluation/lift_stencil &&
  (
    ./bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --application gaussian --input-size small
    ./bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --application gaussian --input-size large
    ./bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --application j3d7pt --input-size small
    ./bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --application j3d7pt --input-size large
  )

  # md_hom_initial
  cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
  mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/

  ./md_hom_initial_gemm --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 10 500 64
  ./md_hom_initial_gemm --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 1024 1024 1024
  ./md_hom_initial_gemv --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 4096 4096
  ./md_hom_initial_gemv --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 8192 8192
  ./md_hom_initial_gaussian_static --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 220 220
  ./md_hom_initial_gaussian_static --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 4092 4092
  ./md_hom_initial_gaussian_dynamic --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 220 220
  ./md_hom_initial_gaussian_dynamic --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 4092 4092
  ./md_hom_initial_j3d7pt --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 254 254 254
  ./md_hom_initial_j3d7pt --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 510 510 510
  ./md_hom_initial_rl --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 32768 32768
  ./md_hom_initial_rl --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 65536 65536
  ./md_hom_initial_rl --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 131072 131072
  ./md_hom_initial_rl --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 262144 262144
  ./md_hom_initial_rl --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 524288 524288
  ./md_hom_initial_rl --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 1048576 1048576

  # EKR
  cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
  mkdir -p $ARTIFACT_ROOT/results/cpu/ekr/
  threads=`nproc --all`
  threads=`echo "$threads*2" | bc`
  java -Xms512m -Xmx14g -jar record-linkage-java.jar 32768 32768 $threads | grep -oP "[0-9]+" > $ARTIFACT_ROOT/results/cpu/ekr/rl_32768x32768_runtime
  java -Xms512m -Xmx14g -jar record-linkage-java.jar 65536 65536 $threads | grep -oP "[0-9]+" > $ARTIFACT_ROOT/results/cpu/ekr/rl_65536x65536_runtime
  java -Xms512m -Xmx14g -jar record-linkage-java.jar 131072 131072 $threads | grep -oP "[0-9]+" > $ARTIFACT_ROOT/results/cpu/ekr/rl_131072x131072_runtime
  java -Xms512m -Xmx14g -jar record-linkage-java.jar 262144 262144 $threads | grep -oP "[0-9]+" > $ARTIFACT_ROOT/results/cpu/ekr/rl_262144x262144_runtime
  java -Xms512m -Xmx14g -jar record-linkage-java.jar 524288 524288 $threads | grep -oP "[0-9]+" > $ARTIFACT_ROOT/results/cpu/ekr/rl_524288x524288_runtime
  java -Xms512m -Xmx14g -jar record-linkage-java.jar 1048576 1048576 $threads | grep -oP "[0-9]+" > $ARTIFACT_ROOT/results/cpu/ekr/rl_1048576x1048576_runtime

  # TACO
  cd $ARTIFACT_ROOT/build/bin/ &&
  mkdir -p $ARTIFACT_ROOT/results/cpu/taco/
  ./matrix_times_matrix --input-size 10 500 64
  ./matrix_times_matrix --input-size 1024 1024 1024

  printf "\n\nReference execution successful!\n"
} || {
  printf "\n\nReference execution failed!\n"
  exit 1
}