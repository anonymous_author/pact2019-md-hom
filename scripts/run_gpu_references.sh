#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi
: ${CUDA_GPU_DEVICE_ID?"Please set the environment variable CUDA_GPU_DEVICE_ID."}
if [ -z "$CUDA_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable CUDA_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  # NVIDIA cuBLAS
  cd $ARTIFACT_ROOT/build/evaluation/cublas &&
  mkdir -p ${ARTIFACT_ROOT}/results/gpu/cublas
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 10 500 64
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 1024 1024 1024
  ./cublas_gemv --device-id $CUDA_GPU_DEVICE_ID --input-size 4096 4096
  ./cublas_gemv --device-id $CUDA_GPU_DEVICE_ID --input-size 8192 8192

  # NVIDIA cuBLASLt
  cd $ARTIFACT_ROOT/build/evaluation/cublaslt &&
  mkdir -p ${ARTIFACT_ROOT}/results/gpu/cublaslt
  ./cublaslt_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 10 500 64
  ./cublaslt_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 1024 1024 1024

  # Lift BLAS
  cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
  (
    export PARENT_ARTIFACT_ROOT=$ARTIFACT_ROOT
    export ARTIFACT_ROOT=`pwd`
    {
      lift/scripts/MatrixVector --iterations 210 --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel generated_kernels/gemv/gemv_N.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID > gemv_lift_4096x4096.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemv_lift_4096x4096.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemv_4096x4096_runtime
    }
    {
      lift/scripts/MatrixVector --iterations 210 --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel generated_kernels/gemv/gemv_N.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID > gemv_lift_8192x8192.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemv_lift_8192x8192.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemv_8192x8192_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 10 -s 500 -s 64 > gemm_lift_10x500x64.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_10x500x64.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_10x500x64_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 1024 -s 1024 -s 1024 > gemm_lift_1024x1024x1024.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_1024x1024x1024.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_1024x1024x1024_runtime
    }
  )

  # NVIDIA cuDNN
  cd $ARTIFACT_ROOT/build/evaluation/cudnn &&
  mkdir -p ${ARTIFACT_ROOT}/results/gpu/cudnn
  ./cudnn_gaussian --device-id $CUDA_GPU_DEVICE_ID --input-size 220 220
  ./cudnn_gaussian --device-id $CUDA_GPU_DEVICE_ID --input-size 4092 4092
  ./cudnn_multi_channel_convolution --device-id $CUDA_GPU_DEVICE_ID --input-size 8 64 54 54 64

  # Lift stencil
  cd $ARTIFACT_ROOT/build/evaluation/lift_stencil &&
  (
    ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application gaussian --input-size small
    ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application gaussian --input-size large
    ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application j3d7pt --input-size small
    ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application j3d7pt --input-size large
  )

  # md_hom_initial
  cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
  mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/

  ./md_hom_initial_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 10 500 64
  ./md_hom_initial_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1024 1024 1024
  ./md_hom_initial_gemv --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4096 4096
  ./md_hom_initial_gemv --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 8192 8192
  ./md_hom_initial_gaussian_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220
  ./md_hom_initial_gaussian_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092
  ./md_hom_initial_gaussian_dynamic --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 220 220
  ./md_hom_initial_gaussian_dynamic --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 4092 4092
  ./md_hom_initial_j3d7pt --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 254 254 254
  ./md_hom_initial_j3d7pt --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 510 510 510
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdab efgc --input-size 24 16 16 16 24 16 24
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdac efgb --input-size 24 16 16 16 24 16 24
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gdbc efga --input-size 24 16 16 16 24 16 24
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims geab dfgc --input-size 24 16 16 24 16 16 24
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims geac dfgb --input-size 24 16 16 24 16 16 24
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gebc dfga --input-size 24 16 16 24 16 16 24
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfab degc --input-size 24 16 16 24 16 16 24
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfac degb --input-size 24 16 16 24 16 16 24
  ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims gfbc dega --input-size 24 16 16 24 16 16 24
  ./md_hom_initial_rl --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 32768 32768
  ./md_hom_initial_rl --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 65536 65536
  ./md_hom_initial_rl --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 131072 131072
  ./md_hom_initial_rl --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 262144 262144
  ./md_hom_initial_rl --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 524288 524288
  ./md_hom_initial_rl --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 1048576 1048576

  # COGENT
  cd ${ARTIFACT_ROOT}/build/evaluation/cogent
  mkdir -p $ARTIFACT_ROOT/results/gpu/cogent/
  ./bench_fb.sh
  cat cogent_fb_results.txt | head -n 1 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_gdab_efgc_runtime
  cat cogent_fb_results.txt | head -n 2 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_gdac_efgb_runtime
  cat cogent_fb_results.txt | head -n 3 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_gdbc_efga_runtime
  cat cogent_fb_results.txt | head -n 4 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_geab_dfgc_runtime
  cat cogent_fb_results.txt | head -n 5 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_geac_dfgb_runtime
  cat cogent_fb_results.txt | head -n 6 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_gebc_dfga_runtime
  cat cogent_fb_results.txt | head -n 7 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_gfab_degc_runtime
  cat cogent_fb_results.txt | head -n 8 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_gfac_degb_runtime
  cat cogent_fb_results.txt | head -n 9 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_abcdef_gfbc_dega_runtime

  # TC
  cd ${ARTIFACT_ROOT}/build/evaluation/tensor_comprehensions
  mkdir -p $ARTIFACT_ROOT/results/gpu/tensor_comprehensions/
  python bench_input_tccg_1.py
  python bench_input_tccg_2.py
  python bench_input_tccg_3.py
  python bench_input_tccg_4.py
  python bench_input_tccg_5.py
  python bench_input_tccg_6.py
  python bench_input_tccg_7.py
  python bench_input_tccg_8.py
  python bench_input_tccg_9.py

  # TVM
  cd ${ARTIFACT_ROOT}/build/evaluation/TVM
  mkdir -p $ARTIFACT_ROOT/results/gpu/tvm/
  python bench_conv2d_cuda.py

  printf "\n\nReference execution successful!\n"
} || {
  printf "\n\nReference execution failed!\n"
  exit 1
}