# README

### Dependencies
- Java
- OpenCL

### Options
The description of the options can be viewed by executing the .jar file.

### Training
SVM: java -jar mdSVM-train.jar [options]
MLP: java -jar mdMLP-train.jar [options]

### Classifiying
SVM: java -jar mdSVM-classify.jar [options]
MLP: java -jar mdMLP-classify.jar [options]

### Training + Classifiying
SVM: java -jar mdSVM-evaluate.jar [options]
MLP: java -jar mdMLP-evaluate.jar [options]

### Using tuning files
The tuning files for MLP or ECC+MLP have to be placed to /tuning_results/eccmlp/ and for SVM to /tuning_results/svm/.
