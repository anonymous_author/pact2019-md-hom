#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&

DEFAULTVALUE="inputs.json"
INPUT=${1:-$DEFAULTVALUE}

DEFAULT_OUTPUT="."
OUTPUT_PATH=${2:-$DEFAULT_OUTPUT}

$DIR/scripts/01_forward.sh $INPUT $OUTPUT_PATH 
$DIR/scripts/02_forward_last.sh $INPUT $OUTPUT_PATH
$DIR/scripts/03_backward.sh $INPUT $OUTPUT_PATH
$DIR/scripts/04_weight.sh $INPUT $OUTPUT_PATH
$DIR/scripts/05_bias.sh $INPUT $OUTPUT_PATH
$DIR/scripts/06_forward_batch.sh $INPUT $OUTPUT_PATH
