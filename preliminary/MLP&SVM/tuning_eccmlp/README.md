# README

### Dependencies
- OpenTuner (http://opentuner.org/tutorial/setup/)

### Build tuner files of atf:
- Open `atf_funer` folder.
- Build tuner files following the `README.md`.
- Copy files with `copy_eccmlp.sh` in `atf_tuner` folder.

### Configure platform and device:
Edit `./settings.json` and fill in the platform and device id and device type (gpu, cpu or acc).

### Configure MLP:
Edit `./inputs.json`:
- "layers": Layersizes separated with semicolons (first layer is the number of attributes and the last layer should be one)
- "chain_count": Number of classifier chains in ECC model
- "batch_size": Size of batch when classifying test instances

### Configure Tuning:
Edit `./inputs.json`:
- {KERNEL-NUMBER}_{KERNEL-NAME}_tuning_configs: Maximal number of configs to tune
- {KERNEL-NUMBER}_{KERNEL-NAME}_max_time: Maximal time to tune

### Tune kernel for ECC+MLP:

Tune all kernel:`./01_tuning.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Default of `INPUT_FILE`: `./inputs.json`
- Default of `OUTPUT_FOLDER_NAME`: `.`

The optimal tuning configs found will be stored in `./tuning_results/{OUTPUT_FOLDER_NAME}` as JSON files.

### Tune specific kernel for ECC+MLP:

- Tune Forward: `./scripts/01_forward.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune Forward Last: `./scripts/02_forward_last.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune Backward: `./scripts/03_backward.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune Weight Update: `./scripts/04_weight.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune Bias Update: `./scripts/05_bias.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune Forward Batch: `./scripts/06_forward_batch.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
