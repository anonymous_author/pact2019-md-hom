package tasks.routines;

import executor.training.BiasExecutor;
import opencl.Kernel;
import opencl.OCLUtil;
import executor.training.WeightUpdateExecutor;
import tasks.training.GpuTrainingPtr;

public class WeightUpdate {
    private int[] layer;

    private long[] prevGradient;
    private long[] prevGradientTmp;

    private float learningRate;
    private float momentum;

    private WeightUpdateExecutor[] executors;
    private BiasExecutor[] biasExecutors;

    private GpuTrainingPtr gpuPtr;

    public WeightUpdate(int chainCount, int[] layer, float learningRate, float momentum, GpuTrainingPtr gpuPtr) {
        this.layer = layer;
        this.learningRate = learningRate;
        this.momentum = momentum;
        this.gpuPtr = gpuPtr;

        prevGradient = new long[layer.length - 1];
        prevGradientTmp = new long[layer.length - 1];

        executors = new WeightUpdateExecutor[layer.length - 1];
        biasExecutors = new BiasExecutor[layer.length - 1];

        for(int i = 0; i < layer.length - 1; i++) {
            executors[i] = new WeightUpdateExecutor(chainCount, layer[i], layer[i + 1]);
            biasExecutors[i] = new BiasExecutor(chainCount, layer[i + 1]);

            // initialize prev gradient values with 0 and tmp prev gradient
            prevGradient[i] = OCLUtil.createZeroFloatBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layer[i] * layer[i + 1]);
            prevGradientTmp[i] = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layer[i] * layer[i + 1] * 4);
        }
    }

    public void execute(long instances) {
        for(int i = 0; i < layer.length - 1; i++) {
            long input = i == 0 ? instances : gpuPtr.Z(i - 1);

            // calculate gradient and update weights
            executors[i].execute(input, gpuPtr.D(i), gpuPtr.weights(i), prevGradient[i], prevGradientTmp[i],
                    learningRate, momentum);

            // replace prev gradient with values of new prev gradients in tmp pointer
            long tmp = prevGradient[i];
            prevGradient[i] = prevGradientTmp[i];
            prevGradientTmp[i] = tmp;

            // calculate new bias value: B = B + learning_rate * D + momentum * old_D
            biasExecutors[i].execute(gpuPtr.B(i), gpuPtr.D(i), gpuPtr.oldD(i), learningRate, momentum);

            //OCLUtil.debugPrint(gpuPtr.B(i), 5, 1);
        }
    }

    public void finish() {
        OCLUtil.releaseMem(prevGradient);
        OCLUtil.releaseMem(prevGradientTmp);

        for(int i = 0; i < layer.length - 1; i++) {
            executors[i].release();
            biasExecutors[i].release();
        }
    }
}
