package tasks.routines;

import executor.training.ForwardLastExecutor;
import executor.training.ForwardExecutor;
import tasks.training.GpuTrainingPtr;

public class Forward {
    private int[] layer;

    private ForwardExecutor[] forward;
    private ForwardLastExecutor forwardLast;

    private GpuTrainingPtr gpuPtr;

    public Forward(int chainCount, int layer[], GpuTrainingPtr gpuPtr) {
        this.layer = layer;
        this.gpuPtr = gpuPtr;

        forward = new ForwardExecutor[layer.length - 2];

        for(int i = 0; i < layer.length - 1; i++) {
            if(i == layer.length - 2) {
                forwardLast = new ForwardLastExecutor(chainCount, layer[layer.length - 1], layer[layer.length - 2]);
            } else {
                forward[i] = new ForwardExecutor(chainCount, layer[i + 1], layer[i]);
            }
        }
    }

    public void execute(long instances, long label) {
        for(int i = 0; i < layer.length - 1; i++) {
            long inputPtr = i == 0 ? instances : gpuPtr.Z(i - 1);

            if(i == layer.length - 2) {

                // compute D (skip computing S and Z of next layer in separate kernel)
                forwardLast.execute(inputPtr, gpuPtr.weights(i), label, gpuPtr.D(layer.length - 2), gpuPtr.B(i));
            } else {

                // compute S
                forward[i].execute(inputPtr, gpuPtr.weights(i), gpuPtr.S(i), gpuPtr.Z(i), gpuPtr.B(i));
            }
        }
    }

    public void finish() {
        for(int i = 0; i < layer.length - 1; i++) {
            if(i == layer.length - 2) {
                forwardLast.release();
            } else {
                forward[i].release();
            }
        }
    }
}
