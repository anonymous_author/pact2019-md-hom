package tasks.routines;

import executor.classification.ForwardBatchExecutor;
import opencl.OCLUtil;
import tasks.classification.GpuClassificationPtr;
import util.PaddingUtil;

public class ForwardBatch {
    private int[] layer;

    private ForwardBatchExecutor[] forward;

    private GpuClassificationPtr gpuPtr;

    public ForwardBatch(int chainCount, int batchSize, int layer[], boolean usePadding, GpuClassificationPtr gpuPtr) {
        this.layer = layer;
        this.gpuPtr = gpuPtr;

        forward = new ForwardBatchExecutor[layer.length - 1];

        for(int i = 0; i < layer.length - 1; i++) {
            int padCur = usePadding ? PaddingUtil.getPadding(i, this.layer) : layer[i];
            int padNext = usePadding ? PaddingUtil.getPadding(i + 1, this.layer) : layer[i + 1];

            forward[i] = new ForwardBatchExecutor(chainCount, batchSize, padNext, padCur);
        }
    }

    public void executeForward(long instances) {
        for (int i = 0; i < layer.length - 1; i++) {
            long input = (i == 0) ? instances : gpuPtr.Z(i - 1);

            // compute Z of next layer
            forward[i].execute(input, gpuPtr.weights(i), gpuPtr.Z(i), gpuPtr.B(i));

            //int padCur = PaddingUtil.getPadding(i, layer);
            //int padNext = PaddingUtil.getPadding(i + 1, layer);
            //OCLUtil.debugPrint(gpuPtr.Z(i), 10, padNext);
        }
    }

    public void finish() {
        for(int i = 0; i < layer.length - 1; i++) {
            forward[i].release();
        }
    }
}
