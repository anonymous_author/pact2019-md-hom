package tasks.classification;

import model.EccLabelResults;
import model.ModelChainMLP;
import model.ModelEccMLP;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opencl.CL10;
import tasks.routines.ForwardBatch;
import util.PaddingUtil;

import java.nio.FloatBuffer;

public class BatchClassificationEcc extends AbstractClassification {
    private int chainCount;
    private int currentLabel;

    private EccLabelResults eccLabelResults;

    public BatchClassificationEcc(ModelEccMLP model, int currentLabel, int batchSize, float threshold,
                                  boolean usePadding, EccLabelResults eccLabelResults) {
        super(batchSize, threshold, usePadding);
        this.layer = model.getModel(currentLabel).getLayers();
        this.chainCount = model.getChainCount();
        this.currentLabel = currentLabel;
        this.eccLabelResults = eccLabelResults;
        this.gpuPtr = new GpuClassificationPtr(layer);

        initializeWeights(model.getModel(currentLabel));
        initializeBias(model.getModel(currentLabel));
        initializeZ(chainCount);

        forwardBatch = new ForwardBatch(chainCount, batchSize, layer, usePadding, gpuPtr);
    }

    public void classify(float[][] instances) {
        int numInstances = instances.length;

        // calculate number of iterations needed
        int numBatches = (int) Math.ceil(numInstances * 1.0f / batchSize);

        // initialize buffer for results of batches
        int layerSize = usePadding ? PaddingUtil.getPadding(layer.length - 1, layer) : layer[layer.length - 1];
        FloatBuffer results = BufferUtils.createFloatBuffer(chainCount * batchSize * layerSize);

        for (int batchId = 0; batchId < numBatches; batchId++) {

            // calculate size of batch and start id of batch
            int start = batchId * batchSize;
            int batch = start + batchSize < numInstances ? batchSize : numInstances - start;

            // load instances to GPU
            long instancesPtr = initInstances(instances, start, batch);

            // forward steps
            forwardBatch.executeForward(instancesPtr);

            // read result from Z of last layer
            OCLUtil.read(results, gpuPtr.Z(layer.length - 2), CL10.CL_TRUE);

            for(int j = 0; j < chainCount; j++) {
                for(int k = 0; k < batchSize; k++) {
                    for(int layerId = 0; layerId < layerSize; layerId ++) {
                        if(layerId == 0) {
                            float val = results.get();
                            float label = val > threshold ? 1 : 0;
                            if (k < batch) eccLabelResults.set(j, batchId * batchSize + k, currentLabel, label);
                        } else {
                            results.get();
                        }
                    }
                }
            }
            results.flip();

            // release memory of instances
            OCLUtil.releaseMem(instancesPtr);
        }
    }

    private long initInstances(float[][] instances, int start, int batch) {
        int layerSize = usePadding ? PaddingUtil.getPadding(0, layer) : layer[0];

        FloatBuffer instancesBuffer = BufferUtils.createFloatBuffer(chainCount * batchSize * layerSize);

        for(int i = 0; i < chainCount; i++) {
            for(int k = 0; k < batchSize; k++) {
                if(k < batch) {

                    // set attributes in buffer
                    instancesBuffer.put(instances[start + k]);

                    // set already predicted labels as attributes in buffer
                    instancesBuffer.put(eccLabelResults.get(i, start + k), 0, currentLabel);

                    // set 0 values for padding borders
                    if(layer[0] < layerSize)
                        instancesBuffer.put(new float[layerSize - layer[0]], 0, layerSize - layer[0]);

                } else {

                    // fill last instances of batch with 0
                    instancesBuffer.put(new float[layerSize], 0, layerSize);
                }
            }
        }
        instancesBuffer.flip();

        return OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, instancesBuffer);
    }

    private void initializeWeights(ModelChainMLP model) {
        for(int i = 0; i < layer.length - 1; i++) {
            int curLayerSize = usePadding ? PaddingUtil.getPadding(i, layer) : layer[i];
            int nextLayerSize = usePadding ? PaddingUtil.getPadding(i + 1, layer) : layer[i + 1];

            FloatBuffer weightsBuffer = BufferUtils.createFloatBuffer(chainCount * curLayerSize * nextLayerSize);

            // load weights between layer[i] and layer[i + 1]
            for(int chainId = 0; chainId < chainCount; chainId++) {
                for (int layerId2 = 0; layerId2 < nextLayerSize; layerId2++) {

                    if(layerId2 < layer[i + 1]) {
                        int id = chainId * layer[i] * layer[i + 1] + layerId2 * layer[i];

                        // set weights for layer[i] - layer[i + 1]
                        weightsBuffer.put(model.getWeightLayer(i), id, layer[i]);

                        // set padding for next layer
                        if (layer[i] < curLayerSize) {
                            weightsBuffer.put(new float[curLayerSize - layer[i]], 0, curLayerSize - layer[i]);
                        }

                    } else {
                        // set padding for current layer
                        weightsBuffer.put(new float[curLayerSize], 0, curLayerSize);
                    }
                }
            }
           // weightsBuffer.put(model.getWeightLayer(i), 0, chainCount * layer[i] * layer[i + 1]);

            weightsBuffer.flip();
            gpuPtr.setWeights(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, weightsBuffer));
        }
    }

    private void initializeBias(ModelChainMLP model) {
        for(int i = 0; i < layer.length - 1; i++) {
            int layerSize = usePadding ? PaddingUtil.getPadding(i + 1, layer) : layer[i + 1];

            FloatBuffer biasBuffer = BufferUtils.createFloatBuffer(chainCount * layerSize);

            // load bias of layer[i + 1]
            for(int chainId = 0; chainId < chainCount; chainId++) {
                biasBuffer.put(model.getBiasLayer(i + 1), chainId * layer[i + 1], layer[i + 1]);

                // set padding for next layer
                PaddingUtil.fillPaddingNegInf(biasBuffer, layer[i + 1], layerSize);
                //if(layer[i + 1] < layerSize) {
                //    biasBuffer.put(new float[layerSize - layer[i + 1]], 0, layerSize - layer[i + 1]);
                //}
            }

            biasBuffer.flip();
            gpuPtr.setB(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, biasBuffer));
        }
    }
}
