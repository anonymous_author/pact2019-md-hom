package tasks.classification;

import model.LabelResults;
import model.ModelChainMLP;
import model.ModelMultiLabelMLP;
import model.InputData;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opencl.CL10;
import tasks.routines.ForwardBatch;

import java.nio.FloatBuffer;

public class BatchClassificationMl extends AbstractClassification {

    public BatchClassificationMl(ModelMultiLabelMLP model, int batchSize, float threshold, boolean usePadding) {
        super(batchSize, threshold, usePadding);
        this.layer = model.getLayers();
        this.gpuPtr = new GpuClassificationPtr(layer);

        initializeZ(1);
        initializeWeights(model);
        initializeBias(model);

        forwardBatch = new ForwardBatch(1, batchSize, model.getLayers(), usePadding, gpuPtr);
    }

    public LabelResults classify(InputData input) {
        int numInstances = input.getNumInstances();
        int numAttributes = input.getNumAttributes();

        // initialize results
        LabelResults results = new LabelResults(numInstances, input.getNumLabels());

        // calculate number of iterations needed
        int numIterations = (int) Math.ceil(numInstances * 1.0f / batchSize);

        for(int iter = 0; iter < numIterations; iter++) {

            // calculate size of batch and start id of batch
            int batch = (iter * batchSize + batchSize < numInstances) ? batchSize : (numInstances - iter * batchSize);
            int start = iter * batchSize;

            // load instances to GPU memory
            long instances = initInstances(start, batch, input, numAttributes);

            // forward steps
            forwardBatch.executeForward(instances);

            // read result from Z
            FloatBuffer buffer = BufferUtils.createFloatBuffer(batchSize * layer[layer.length - 1]);
            OCLUtil.read(buffer, gpuPtr.Z(layer.length - 2), CL10.CL_TRUE);

            for(int i = 0; i < batchSize; i++) {
                for(int j = 0; j < layer[layer.length - 1]; j ++) {
                    float label = (buffer.get() > threshold) ? 1 : 0;
                    if (i < batch) results.set(start + i, j, label);
                }
            }

            // release memory of instances
            OCLUtil.releaseMem(instances);
        }

        return results;
    }

    private long initInstances(int start, int batch, InputData input, int numAttributes) {
        FloatBuffer instancesBuffer = BufferUtils.createFloatBuffer(batchSize * numAttributes);

        for(int i = 0; i < batchSize; i++) {
            if(i < batch) {
                for (int j = 0; j < numAttributes; j++) {
                    instancesBuffer.put(input.getAttribute(start + i, j));
                }
            } else {
                for (int j = 0; j < numAttributes; j++) {
                    instancesBuffer.put(0);
                }
            }
        }
        instancesBuffer.flip();

        return OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, instancesBuffer);
    }

    private void initializeWeights(ModelMultiLabelMLP model) {
        for(int i = 0; i < layer.length - 1; i++) {
            FloatBuffer weightsBuffer = BufferUtils.createFloatBuffer(layer[i] * layer[i + 1]);
            float[][] weightLayer = model.getWeightLayer(i);

            for (int j = 0; j < layer[i + 1]; j++) {
                for (int k = 0; k < layer[i]; k++) {
                    weightsBuffer.put(weightLayer[k][j]);
                }
            }
            weightsBuffer.flip();

            gpuPtr.setWeights(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, weightsBuffer));
        }
    }

    private void initializeBias(ModelMultiLabelMLP model) {
        for(int i = 0; i < layer.length - 1; i++) {
            FloatBuffer biasBuffer = BufferUtils.createFloatBuffer(layer[i + 1]);

            // load weights between layer[i] and layer[i + 1]
            biasBuffer.put(model.getBiasLayer(i + 1), 0, layer[i + 1]);

            biasBuffer.flip();
            gpuPtr.setB(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, biasBuffer));
        }
    }
}
