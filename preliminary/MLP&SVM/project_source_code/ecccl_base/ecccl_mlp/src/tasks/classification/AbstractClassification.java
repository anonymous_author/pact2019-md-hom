package tasks.classification;

import opencl.Kernel;
import opencl.OCLUtil;
import tasks.routines.ForwardBatch;
import util.PaddingUtil;

public abstract class AbstractClassification {
    protected int layer[];
    protected int batchSize;
    protected float threshold;
    protected boolean usePadding;

    // pointer to gpu memory
    protected GpuClassificationPtr gpuPtr;

    // routine
    protected ForwardBatch forwardBatch;

    AbstractClassification(int batchSize, float threshold, boolean usePadding) {
        this.batchSize = batchSize;
        this.threshold = threshold;
        this.usePadding = usePadding;
    }

    public void finish() {
        forwardBatch.finish();

        gpuPtr.release();
    }

    protected void initializeZ(int modelCount) {
        for(int i = 0; i < layer.length - 1; i++) {
            int layerSize = usePadding ? PaddingUtil.getPadding(i + 1, layer) : layer[i + 1];

            gpuPtr.setZ(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, modelCount * batchSize * layerSize * 4));
        }
    }
}
