package tasks.training;

import model.ModelChainMLP;
import tasks.routines.Backward;
import tasks.routines.Forward;
import tasks.routines.WeightUpdate;
import model.InputData;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opencl.CL10;

import java.nio.FloatBuffer;

public class TrainingEcc extends AbstractTraining {
    private int[][] chainInstances;
    private int[][] chainLabelOrder;
    private int chainCount;
    private int currentLabel;
    private int subsetSize;

    private long[] instancesArr;
    private long[] labelArr;

    public TrainingEcc(InputData input, int[] layer, float learningRate, float momentum, int seed,
                       int subsetSize, int chainCount, int currentLabel, int[][] chainInstances, int[][] chainLabelOrder) {
        super(input, layer, learningRate, momentum, seed);

        this.subsetSize = subsetSize;
        this.chainInstances = chainInstances;
        this.chainLabelOrder = chainLabelOrder;
        this.chainCount = chainCount;
        this.currentLabel = currentLabel;

        initializeWeights(chainCount);
        initialize_S_Z_D(chainCount);

        forward = new Forward(chainCount, layer, gpuPtr);
        backward = new Backward(chainCount, layer, gpuPtr);
        weightUpdate = new WeightUpdate(chainCount, layer, learningRate, momentum, gpuPtr);
    }

    public void train(int epoch) {
        for(int ep = 0; ep < epoch; ep++) {
            for (int inst = 0; inst < subsetSize; inst++) {

                // forward step
                forward.execute(instancesArr[inst], labelArr[inst]);

                // backward step
                backward.execute();

                // weight update step
                weightUpdate.execute(instancesArr[inst]);

                // save delta as old delta values for bias update with momentum
                gpuPtr.swapDelta();
            }
        }
    }

    public void loadInput(int subsetSize) {

        // instances and label pointer to gpu memory
        instancesArr = new long[subsetSize];
        labelArr = new long[subsetSize];

        // initialize instance of each chain and corresponding label
        for (int i = 0; i < subsetSize; i++) {
            instancesArr[i] = initInstances(chainCount, i, input, currentLabel);
            labelArr[i] = initLabel(chainCount, i, input, currentLabel);
        }
    }

    public void releaseInput() {
        OCLUtil.releaseMem(instancesArr);
        OCLUtil.releaseMem(labelArr);
    }

    public ModelChainMLP getModel() {
        ModelChainMLP model = new ModelChainMLP(layer);

        for(int layerId = 0; layerId < layer.length - 1; layerId++) {

            // initialize buffer for weights between layer[layerId] and layer[layerId + 1] and bias
            FloatBuffer weightsBuffer = BufferUtils.createFloatBuffer(chainCount * layer[layerId] * layer[layerId + 1]);
            FloatBuffer biasBuffer = BufferUtils.createFloatBuffer(chainCount * layer[layerId + 1]);

            // read weights between layer[layerId] and layer[layerId + 1] and bias of layerId + 1
            OCLUtil.read(weightsBuffer, gpuPtr.weights(layerId), CL10.CL_TRUE);
            OCLUtil.read(biasBuffer, gpuPtr.B(layerId), CL10.CL_TRUE);

            // put values from buffer into array
            float[] weightLayer = new float[chainCount * layer[layerId + 1] * layer[layerId]];
            weightsBuffer.get(weightLayer);

            float[] biasLayer = new float[chainCount * layer[layerId + 1]];
            biasBuffer.get(biasLayer);

            // add weights and bias to model
            model.addWeightLayer(weightLayer);
            model.addBiasLayer(biasLayer);
        }

        return model;
    }

    private long initLabel(int chainCount, int iter, InputData input, int currentLabel) {
        FloatBuffer labelBuffer = BufferUtils.createFloatBuffer(chainCount);

        for(int i = 0; i < chainCount; i++) {
            // get id of current instance
            int instId = chainInstances[i][iter];

            float label = input.getLabel(instId, chainLabelOrder[i][currentLabel]);
            labelBuffer.put(label == -1 ? 0 : label);
        }

        labelBuffer.flip();
        return OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, labelBuffer);
    }

    private long initInstances(int chainCount, int iter, InputData input, int labelCount) {
        FloatBuffer instancesBuffer = BufferUtils.createFloatBuffer(chainCount * (input.getNumAttributes() + labelCount));

        for(int i = 0; i < chainCount; i++) {
            // get id of current instance
            int instId = chainInstances[i][iter];

            // set attributes in buffer
            instancesBuffer.put(input.getInstance(instId), 0, input.getNumAttributes());

            // set labels of previous classifier from chain in buffer
            for(int j = 0; j < labelCount; j++) {
                instancesBuffer.put(input.getLabelAsAttr(instId, chainLabelOrder[i][j]));
            }
        }

        instancesBuffer.flip();
        return OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, instancesBuffer);
    }
}
