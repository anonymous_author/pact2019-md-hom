import opencl.PlatformUtil;
import reporter.EccMLPReporter;
import reporter.MultiLabelMLPReporter;
import util.Config;
import model.InputData;
import util.WekaLoader;

import java.io.File;

public class MainMLPReporter {
    private static final int[] layerSizes = new int[]{-1};
    private static final float learningRate = 0.3f;
    private static final float momentum = 0.2f;

    private static int chainCount = 10;
    private static final int seed = 1;
    private static final int subsetPercentage = 100;

    private static final int batchSize = 256;
    private static final float thresholdMLP = 0.5f;
    private static final float thresholdECC = 0.5f;

    private static final int epoch = 300;

    private static final String dataset = "yeast";

    public static void main(String[] args) throws Exception {
        InputData train = WekaLoader.load("./data/ecccl/yeast-train-norm", 14);
        InputData test = WekaLoader.load("./data/ecccl/yeast-test-norm", 14);

        PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

        String resultFileECC = "./compare-results/" + dataset + "-accuracy-ecc-" + seed + "-" + chainCount + "-train.log";
        reportECC(train, train, resultFileECC);

//        chainCount = 32;
//
//        resultFileECC = "./compare-results/" + dataset + "-accuracy-ecc-" + seed + "-" + chainCount + "-low.log";
//        reportECC(train, test, resultFileECC);

        String resultFileML = "./compare-results/" + dataset + "-accuracy-ml-" + seed + "-train.log";
        reportMultiLabel(train, train, resultFileML);

        PlatformUtil.requestShutdown();
    }

    private static void reportECC(InputData train, InputData test, String resultFileECC) {
        File outFile = new File(resultFileECC);
        if(outFile.exists()) outFile.delete();

        EccMLPReporter eccReporter = new EccMLPReporter.Builder(layerSizes, epoch)
                .setChainCount(chainCount)
                .setSeed(seed)
                .setSubsetPercentage(subsetPercentage)
                .setLearningRate(learningRate)
                .setMomentum(momentum)
                .setBatchSize(batchSize)
                .setThresholdECC(thresholdECC)
                .setThresholdMLP(thresholdMLP)
                .build();

        long start = System.currentTimeMillis();
        eccReporter.reportAccuracies(train, test, resultFileECC);
        long end = System.currentTimeMillis();

        System.out.println("ECC Reporting Time: " + (end - start) + " ms");
    }

    private static void reportMultiLabel(InputData train, InputData test, String resultFileML) {
        File outFileMl = new File(resultFileML);
        if(outFileMl.exists()) outFileMl.delete();

        MultiLabelMLPReporter mlReporter = new MultiLabelMLPReporter.Builder(layerSizes, epoch)
                .setSeed(seed)
                .setLearningRate(learningRate)
                .setMomentum(momentum)
                .setBatchSize(batchSize)
                .setThreshold(thresholdMLP)
                .build();

        long startMl = System.currentTimeMillis();
        mlReporter.reportAccuracies(train, test, resultFileML);
        long endMl = System.currentTimeMillis();

        System.out.println("MultiLabel Reporting Time: " + (endMl - startMl) + " ms");
    }
}
