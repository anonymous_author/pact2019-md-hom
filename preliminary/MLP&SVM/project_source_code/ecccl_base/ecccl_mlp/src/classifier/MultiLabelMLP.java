package classifier;

import model.ModelMultiLabelMLP;
import opencl.PlatformUtil;
import tasks.training.TrainingMl;
import model.InputData;
import util.Config;
import util.MLPBenchmark;

public class MultiLabelMLP extends AbstractMLP {

    private MultiLabelMLP(Builder builder) {
        super(builder);
    }

    public ModelMultiLabelMLP train(InputData input) {
        int[] layer = initializeLayer(input);

        // initialize handler for training
        TrainingMl handler = new TrainingMl(input, layer, learningRate, momentum, seed);

        // train with all instances in input epoch-times
        handler.train(epoch);
        ModelMultiLabelMLP model = handler.getModel();
        handler.finish();

        if(Config.PRINT_LEVEL > 1 && PlatformUtil.PROFILING_ENABLED) {
            System.out.println("\nForward: Build " + MLPBenchmark.forwardBuild / 1000000+ " ms, Kernel: " +
                    MLPBenchmark.forwardKernelTime / 1000000 + " ms");
            System.out.println("Activation Build: " + MLPBenchmark.forwardActivationBuild / 1000000 + " ms, Kernel: " +
                    MLPBenchmark.forwardActivationKernelTime / 1000000 + " ms");
            System.out.println("Backprop Build: " + MLPBenchmark.backwardBuild / 1000000 + " ms, Kernel: " +
                    MLPBenchmark.backwardKernelTime / 1000000 + " ms");
            System.out.println("Weight Build: " + MLPBenchmark.weightUpdateBuild / 1000000 + " ms, Kernel: " +
                    MLPBenchmark.weightUpdateKernelTime / 1000000 + " ms");
        }

        return model;
    }

    private int[] initializeLayer(InputData input) {
        int[] layer = new int[hiddenLayer.length + 2];
        layer[0] = input.getNumAttributes();
        layer[layer.length - 1] = input.getNumLabels();
        for(int j = 0; j < hiddenLayer.length; j++) {
            if (hiddenLayer[j] == -1) layer[j + 1] = (layer[j] + 1) / 2;
            else layer[j + 1] = hiddenLayer[j];
        }
        return layer;
    }

    public static class Builder extends AbstractBuilder<Builder> {
        public Builder(int[] hiddenLayer, int epoch) {
            super(hiddenLayer, epoch);
        }

        @Override
        protected Builder getInstance() {
            return this;
        }

        public MultiLabelMLP build() {
            return new MultiLabelMLP(this);
        }
    }
}
