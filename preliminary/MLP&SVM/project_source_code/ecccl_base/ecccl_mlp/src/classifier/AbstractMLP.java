package classifier;

public abstract class AbstractMLP {
    protected int seed;
    protected int epoch;
    
    // parameter of network
    protected int[] hiddenLayer;
    protected float learningRate;
    protected float momentum;

    protected AbstractMLP(AbstractBuilder<?> builder) {
        this.seed = builder.seed;
        this.epoch = builder.epoch;
        this.hiddenLayer = builder.hiddenLayer;
        this.learningRate = builder.learningRate;
        this.momentum = builder.momentum;
    }

    public abstract static class AbstractBuilder<T extends AbstractBuilder<T>> {
        protected int seed = 0;

        // hyper parameter
        protected int epoch;
        protected float learningRate = 0.3f;
        protected float momentum = 0.2f;
        protected int[] hiddenLayer;

        protected abstract T getInstance();

        AbstractBuilder(int[] hiddenLayer, int epoch) {
            this.epoch = epoch;
            this.hiddenLayer = hiddenLayer;
        }

        public T setSeed(int seed) {
            this.seed = seed;
            return getInstance();
        }

        public T setLearningRate(float learningRate) {
            this.learningRate = learningRate;
            return getInstance();
        }

        public T setMomentum(float momentum) {
            this.momentum = momentum;
            return getInstance();
        }
    }
}
