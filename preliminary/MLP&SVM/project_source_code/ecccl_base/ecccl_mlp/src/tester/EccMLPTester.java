package tester;

import model.EccLabelResults;
import model.LabelResults;
import model.ModelEccMLP;
import tasks.classification.BatchClassificationEcc;
import model.InputData;
import opencl.PlatformUtil;
import util.Config;
import util.MLPBenchmark;

public class EccMLPTester {
    private int batchSize;
    private float thresholdECC;
    private float thresholdMLP;
    private boolean usePadding;

    private EccMLPTester(Builder builder) {
        this.batchSize = builder.batchSize;
        this.thresholdECC = builder.thresholdECC;
        this.thresholdMLP = builder.thresholdMLP;
        this.usePadding = builder.usePadding;
    }

    public LabelResults classify(InputData input, ModelEccMLP model) {
        if(Config.PRINT_LEVEL > 0) System.out.print("\nClassifying: ");

        // initialize ecc results
        EccLabelResults eccLabelResults = new EccLabelResults(model.getChainCount(), input.getNumInstances(),
                model.getLabelCount(), model.getChainLabelOrder());

        // convert instances to attributes for fast setting up buffers
        float[][] instances = getInstances(input);

        // classify labels on device
        classifyLabels(model, eccLabelResults, instances);

        // get label results from ecc results
        LabelResults results = new LabelResults(eccLabelResults, input.getNumInstances(), input.getNumLabels());

        // threshold results
        results.threshold(model.getChainCount(), thresholdECC);

        if(Config.PRINT_LEVEL > 1 && PlatformUtil.PROFILING_ENABLED) {
            System.out.println("\nClassify Forward Build: " + MLPBenchmark.forwardBatchBuild / 1000000 + " ms, Kernel: " +
                    MLPBenchmark.forwardBatchKernelTime / 1000000 + " ms");
        }

        return results;
    }

    private void classifyLabels(ModelEccMLP model, EccLabelResults eccLabelResults, float[][] instances) {

        for(int labelId = 0; labelId < model.getLabelCount(); labelId++) {
            BatchClassificationEcc handler = new BatchClassificationEcc(model, labelId, batchSize, thresholdMLP, usePadding, eccLabelResults);
            handler.classify(instances);
            handler.finish();

            if(Config.PRINT_LEVEL > 0) System.out.print(".");
        }
        if(Config.PRINT_LEVEL > 0) System.out.println();
    }

    /**
     * Converts input data to an array which is holding only attributes of the instances.
     *
     * @param input data set
     * @return attributes of instances
     */
    private float[][] getInstances(InputData input) {
        int numInstances = input.getNumInstances();
        int numAttributes = input.getNumAttributes();

        float[][] instances = new float[numInstances][numAttributes];
        for (int instanceId = 0; instanceId < numInstances; instanceId++) {
            System.arraycopy(input.getInstance(instanceId), 0, instances[instanceId], 0, numAttributes);
        }

        return instances;
    }

    public static class Builder {
        private int batchSize = 32;
        private float thresholdECC = 0.5f;
        private float thresholdMLP = 0.5f;
        private boolean usePadding = false;

        public EccMLPTester build() {
            return new EccMLPTester(this);
        }

        public Builder setBatchSize(int batchSize) {
            this.batchSize = batchSize;
            return this;
        }

        public Builder setThresholdMLP(float thresholdMLP) {
            this.thresholdMLP = thresholdMLP;
            return this;
        }

        public Builder setThresholdECC(float thresholdECC) {
            this.thresholdECC = thresholdECC;
            return this;
        }

        public Builder setUsePadding(boolean usePadding) {
            this.usePadding = usePadding;
            return this;
        }
    }
}
