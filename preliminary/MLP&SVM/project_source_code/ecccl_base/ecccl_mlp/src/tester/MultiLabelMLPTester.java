package tester;

import model.LabelResults;
import model.ModelMultiLabelMLP;
import tasks.classification.BatchClassificationMl;
import model.InputData;
import opencl.PlatformUtil;
import util.Config;
import util.MLPBenchmark;

public class MultiLabelMLPTester {
    private boolean usePadding;
    private int batchSize;
    private float threshold;

    public MultiLabelMLPTester(int batchSize, float threshold, boolean usePadding) {
        this.batchSize = batchSize;
        this.threshold = threshold;
        this.usePadding = usePadding;
    }

    public LabelResults classify(InputData test, ModelMultiLabelMLP model) {
        // initialize handler for batch classification
        BatchClassificationMl handler = new BatchClassificationMl(model, batchSize, threshold, usePadding);

        // classify instances in batches
        LabelResults results = handler.classify(test);
        handler.finish();

        if(Config.PRINT_LEVEL > 1 && PlatformUtil.PROFILING_ENABLED) {
            System.out.println("Classify Forward Build: " + MLPBenchmark.forwardBatchBuild + " ms, Kernel: " +
                    MLPBenchmark.forwardBatchKernelTime / 1000000 + " ms");
        }

        return results;
    }

    public float evaluate(InputData test, LabelResults results) {
        System.out.println();
        System.out.println("Evaluation: ");
        float mean = 0;
        for(int i = 0; i < test.getNumLabels(); i++) {
            int hits = 0;
            for(int j = 0; j < test.getNumInstances(); j++) {
                if(results.get(j, i) == 0 && (test.getLabel(j, i) == 0 || test.getLabel(j, i) == -1)) {
                    hits++;
                } else if (results.get(j, i) == 1 && test.getLabel(j, i) == 1) {
                    hits++;
                }
            }

            System.out.println("Label " + i + ": " + hits + "/" + test.getNumInstances()
                    + " (" + String.format("%.3f", hits * 1.0 / test.getNumInstances()) + ")");
            mean += hits * 1.0 / test.getNumInstances();
        }
        mean = mean / test.getNumLabels();
        System.out.println("\nHamming Score: " + String.format("%.3f", mean));
        return mean;
    }
}
