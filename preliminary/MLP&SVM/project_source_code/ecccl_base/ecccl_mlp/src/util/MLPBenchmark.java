package util;

public class MLPBenchmark {
    // Training
    public static long forwardBuild = 0;
    public static long forwardActivationBuild = 0;
    public static long backwardBuild = 0;
    public static long weightUpdateBuild = 0;

    public static long forwardKernelTime = 0;
    public static long forwardActivationKernelTime = 0;
    public static long backwardKernelTime = 0;
    public static long weightUpdateKernelTime = 0;

    // Testing
    public static long forwardBatchBuild = 0;

    public static long forwardBatchKernelTime = 0;

    private static void resetTraining() {
         forwardBuild = 0;
         forwardActivationBuild = 0;
         backwardBuild = 0;
         weightUpdateBuild = 0;

         forwardKernelTime = 0;
         forwardActivationKernelTime = 0;
         backwardKernelTime = 0;
         weightUpdateKernelTime = 0;
    }

    private static void resetTesting() {
         forwardBatchBuild = 0;
         forwardBatchKernelTime = 0;
    }

    public static void reset() {
        resetTraining();
        resetTesting();
    }

    public static long getBuildTimeTraining() {
        return (forwardBuild + forwardActivationBuild + backwardBuild + weightUpdateBuild) / 1000000;
    }

    public static long getRunTimeTraining() {
        return (forwardKernelTime + forwardActivationKernelTime + backwardKernelTime + weightUpdateKernelTime) / 1000000;
    }

    public static long getBuildTimeTesting() {
        return forwardBatchBuild / 1000000;
    }

    public static long getRunTimeTesting() {
        return forwardBatchKernelTime / 1000000;
    }
}
