import classifier.MultiLabelMLP;
import model.LabelResults;
import model.ModelMultiLabelMLP;
import opencl.PlatformUtil;
import tester.MultiLabelMLPTester;
import util.Config;
import model.InputData;
import util.MLPBenchmark;
import util.WekaLoader;

public class MainMultiLabelMLP {
    public static void main(String[] args) throws Exception {
        InputData train = WekaLoader.load("./data/ecccl/yeast-train-norm", 14);
        InputData test = WekaLoader.load("./data/ecccl/yeast-test-norm", 14);
        if (train == null || test == null) return;
        //NormalizeUtil.normalizeInput(train, test, NormalizeUtil.NormType.FEATURE_SCALING_0_1);

        PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

        int[] layerSizes = new int[]{-1};
        float learningRate = 0.3f;
        float momentum = 0.2f;
        int epoch = 5;
        int seed = 0;

        int batchSize = 32;
        float thresholdMLP = 0.5f;

        //Thread.sleep(10000);

        // initialize classifier for training
        MultiLabelMLP mlMLP =  new MultiLabelMLP.Builder(layerSizes, epoch)
                .setSeed(seed)
                .setLearningRate(learningRate)
                .setMomentum(momentum)
                .build();

        long startTrain = System.currentTimeMillis();
        ModelMultiLabelMLP model = mlMLP.train(train);
        long endTrain = System.currentTimeMillis();

        System.out.println();
        System.out.println("Runtime Training: " + (endTrain - startTrain) + " ms");
        System.out.println("Runtime Training Kernel: " + MLPBenchmark.getRunTimeTraining() + " ms");
        System.out.println("Runtime Training (ohne Bauen): " + (endTrain - startTrain - MLPBenchmark.getBuildTimeTraining()) + " ms");

        // initialize tester for testing
        MultiLabelMLPTester mlMLPTester = new MultiLabelMLPTester(batchSize, thresholdMLP, false);

        long startClassBatch = System.currentTimeMillis();
        LabelResults results = mlMLPTester.classify(test, model);
        long endClassBatch = System.currentTimeMillis();

        System.out.println();
        System.out.println("Runtime Batch Classifying: " + (endClassBatch - startClassBatch) + " ms");
        System.out.println("Runtime Batch Classifying Kernel: " + MLPBenchmark.getRunTimeTesting() + " ms");
        System.out.println("Runtime Batch Classifying (ohne Bauen): " + (endClassBatch - startClassBatch - MLPBenchmark.getBuildTimeTesting()) + " ms");

        mlMLPTester.evaluate(test, results);

        PlatformUtil.requestShutdown();
    }
}
