package model;

import java.util.ArrayList;
import java.util.List;

public class ModelChainMLP {
    private int[] layers;
    private List<float[]> weightsLayer;
    private List<float[]> biasLayer;

    public ModelChainMLP(int[] layers) {
        this.weightsLayer = new ArrayList<>();
        this.biasLayer = new ArrayList<>();
        this.layers = layers.clone();
    }

    public void addWeightLayer(float[] weights) {
        weightsLayer.add(weights);
    }

    public void addBiasLayer(float[] bias) {
        biasLayer.add(bias);
    }

    public float[] getWeightLayer(int layer) {
        return weightsLayer.get(layer);
    }

    public float[] getBiasLayer(int layer) {
        return biasLayer.get(layer - 1);
    }

    public int[] getLayers() {
        return layers;
    }
}
