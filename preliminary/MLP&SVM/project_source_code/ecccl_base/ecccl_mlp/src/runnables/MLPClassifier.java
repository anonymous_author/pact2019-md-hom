package runnables;

import evaluator.Evaluator;
import model.InputData;
import model.LabelResults;
import model.ModelEccMLP;
import opencl.PlatformUtil;
import org.apache.commons.cli.*;
import util.CliUtil;
import tester.EccMLPTester;
import util.*;

import java.io.IOException;

public class MLPClassifier {
    private static final String JAR_NAME = "mdMLP-classify.jar";
    private static final String DEFAULT_OUTPUT_PATH = "./outputs/";
    private static final String DEFAULT_MODEL_FILE = "./models/result.model";

    public static void main(String[] args) {

        // Input and Output options
        Options optionsIO = new Options();
        optionsIO.addOption(Option.builder("test").hasArg().desc("test data file (required)").required().build());
        optionsIO.addOption(Option.builder("l").longOpt("label").hasArg().desc("number of labels (required)").required().build());
        optionsIO.addOption(Option.builder("m").longOpt("model").hasArg().desc("model file (default "+ DEFAULT_MODEL_FILE + ")").build());
        optionsIO.addOption(Option.builder("o").longOpt("output").hasArg().desc("output path (default ./outputs/)").build());
        optionsIO.addOption(Option.builder("q").longOpt("quiet").desc("quiet mode").build());

        // Platform options
        Options optionsPlatform = CliUtil.getPlatformOptions();

        // MLP parameter options
        Options optionsParameter = CliMLPUtil.getMLPClassifyOptions();

        // collect options
        Options options = new Options();
        for(Option option : optionsIO.getOptions()) options.addOption(option);
        for(Option option : optionsPlatform.getOptions()) options.addOption(option);
        for(Option option : optionsParameter.getOptions()) options.addOption(option);

        // parsing options and executing classifier
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            // parse i/o
            String testStr = cmd.getOptionValue("test");
            int labelCount = Integer.parseInt(cmd.getOptionValue("l"));
            String modelFile = cmd.hasOption("m") ? cmd.getOptionValue("m") : DEFAULT_MODEL_FILE;
            String outputPath = cmd.hasOption("o") ? cmd.getOptionValue("o") : DEFAULT_OUTPUT_PATH;

            // quiet mode
            if(cmd.hasOption("q")) Config.PRINT_LEVEL = 0;

            // parse platform
            CliUtil.parsePlatformOptions(cmd);
            PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

            // load input and test data set
            InputData test = WekaLoader.load(testStr, labelCount);

            // get testing class
            EccMLPTester eccMLPTester = CliMLPUtil.getMLPTesterFromOptions(cmd);

            // restore model
            ModelEccMLP model = ModelIOUtil.restoreModel(modelFile);

            // run testing
            long startClassBatch = System.currentTimeMillis();
            LabelResults resultsBatch = eccMLPTester.classify(test, model);
            long endClassBatch = System.currentTimeMillis();

            Evaluator.evaluate(test, resultsBatch, outputPath);

            System.out.println("\nClassifying: " + (endClassBatch - startClassBatch) + " ms");
            System.out.println("Classifying (without building kernels): " + (endClassBatch - startClassBatch - MLPBenchmark.getBuildTimeTesting()) + " ms");

            if(Config.PRINT_LEVEL > 0) System.out.println("\nClassifying of labels finished.");

        } catch (ParseException e) {

            // show error and command line options
            System.err.println("Error: " + e.getMessage());
            CliMLPUtil.formatHelp(optionsIO, optionsPlatform, optionsParameter, CliMLPUtil.getClassifyComparator(),
                    "java -jar " + JAR_NAME + " [options] -test test_file -l label_count");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PlatformUtil.requestShutdown();
        }
    }
}
