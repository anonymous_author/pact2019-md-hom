package runnables;

import classifier.EccMLP;
import model.InputData;
import model.ModelEccMLP;
import opencl.PlatformUtil;
import org.apache.commons.cli.*;
import util.CliUtil;
import util.*;

public class MLPTraining {
    private static final String JAR_NAME = "mdMLP-train.jar";
    private static final String DEFAULT_MODEL_PATH = "./models/result.model";

    public static void main(String[] args) {

        // Input and Output options
        Options optionsIO = new Options();
        optionsIO.addOption(Option.builder("train").hasArg().desc("training data file (required)").required().build());
        optionsIO.addOption(Option.builder("l").longOpt("label").hasArg().desc("number of labels (required)").required().build());
        optionsIO.addOption(Option.builder("o").longOpt("output").hasArg().desc("model path (default " + DEFAULT_MODEL_PATH + ")").build());
        optionsIO.addOption(Option.builder("q").longOpt("quiet").desc("quiet mode").build());

        // Platform options
        Options optionsPlatform = CliUtil.getPlatformOptions();

        // MLP parameter options
        Options optionsParameter = CliMLPUtil.getMLPOptions();

        // collect options
        Options options = new Options();
        for(Option option : optionsIO.getOptions()) options.addOption(option);
        for(Option option : optionsPlatform.getOptions()) options.addOption(option);
        for(Option option : optionsParameter.getOptions()) options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            // parse i/o
            String trainStr = cmd.getOptionValue("train");
            int labelCount = Integer.parseInt(cmd.getOptionValue("l"));
            String modelPath = cmd.hasOption("o") ? cmd.getOptionValue("o") : DEFAULT_MODEL_PATH;

            // quiet mode
            if(cmd.hasOption("q")) Config.PRINT_LEVEL = 0;

            // parse platform
            CliUtil.parsePlatformOptions(cmd);
            PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

            // load train data set
            InputData train = WekaLoader.load(trainStr, labelCount);

            // get training class
            EccMLP eccMLP = CliMLPUtil.getMLPFromOptions(cmd);

            // run training
            long startTrain = System.currentTimeMillis();
            ModelEccMLP model = eccMLP.train(train);
            long endTrain = System.currentTimeMillis();

            // store model
            ModelIOUtil.storeModel(model, modelPath);

            System.out.println("\nTraining: " + (endTrain - startTrain) + " ms");
            System.out.println("Training (without building kernels): " + (endTrain - startTrain - MLPBenchmark.getBuildTimeTraining()) + " ms");

            if(Config.PRINT_LEVEL > 0) System.out.println("\nTraining of labels finished.");

            PlatformUtil.requestShutdown();

        } catch (ParseException e) {
            CliMLPUtil.formatHelp(optionsIO, optionsPlatform, optionsParameter, CliMLPUtil.getTrainComparator(),
                    "java -jar " + JAR_NAME + " [options] -train training_file -l label_count");
            System.err.println("Error: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
