package runnables;

import classifier.EccMLP;
import evaluator.Evaluator;
import model.InputData;
import model.LabelResults;
import model.ModelEccMLP;
import opencl.PlatformUtil;
import org.apache.commons.cli.*;
import util.CliUtil;
import tester.EccMLPTester;
import util.*;

public class MLPEvaluator {
    private static final String JAR_NAME = "mdMLP-evaluate.jar";
    private static final String DEFAULT_OUTPUT_PATH = "./outputs/";

    public static void main(String[] args) {

        // Input and Output options
        Options optionsIO = new Options();
        optionsIO.addOption(Option.builder("train").hasArg().desc("training data file (required)").required().build());
        optionsIO.addOption(Option.builder("test").hasArg().desc("test data file (required)").required().build());
        optionsIO.addOption(Option.builder("l").longOpt("label").hasArg().desc("number of labels (required)").required().build());
        optionsIO.addOption(Option.builder("o").longOpt("output").hasArg().desc("output path (default ./outputs/)").build());
        optionsIO.addOption(Option.builder("q").longOpt("quiet").desc("quiet mode").build());

        // Platform options
        Options optionsPlatform = CliUtil.getPlatformOptions();

        // MLP parameter options
        Options optionsParameter = CliMLPUtil.getMLPEvalOptions();

        // collect options
        Options options = new Options();
        for(Option option : optionsIO.getOptions()) options.addOption(option);
        for(Option option : optionsPlatform.getOptions()) options.addOption(option);
        for(Option option : optionsParameter.getOptions()) options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            // parse i/o
            String trainStr = cmd.getOptionValue("train");
            String testStr = cmd.getOptionValue("test");
            int labelCount = Integer.parseInt(cmd.getOptionValue("l"));
            String outputPath = cmd.hasOption("o") ? cmd.getOptionValue("o") : DEFAULT_OUTPUT_PATH;

            // quiet mode
            if(cmd.hasOption("q")) Config.PRINT_LEVEL = 0;

            // parse platform
            CliUtil.parsePlatformOptions(cmd);
            PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

            // load input and test data set
            InputData train = WekaLoader.load(trainStr, labelCount);
            InputData test = WekaLoader.load(testStr, labelCount);

            // get training class
            EccMLP eccMLP = CliMLPUtil.getMLPFromOptions(cmd);

            // get testing class
            EccMLPTester eccMLPTester = CliMLPUtil.getMLPTesterFromOptions(cmd);

            // run training
            long startTrain = System.currentTimeMillis();
            ModelEccMLP model = eccMLP.train(train);
            long endTrain = System.currentTimeMillis();

            // run testing
            long startClassBatch = System.currentTimeMillis();
            LabelResults resultsBatch = eccMLPTester.classify(test, model);
            long endClassBatch = System.currentTimeMillis();

            Evaluator.evaluate(test, resultsBatch, outputPath);

            System.out.println("\nTraining: " + (endTrain - startTrain) + " ms");
            System.out.println("Training (without building kernels): " + (endTrain - startTrain - MLPBenchmark.getBuildTimeTraining()) + " ms");

            System.out.println("\nClassifying: " + (endClassBatch - startClassBatch) + " ms");
            System.out.println("Classifying (without building kernels): " + (endClassBatch - startClassBatch - MLPBenchmark.getBuildTimeTesting()) + " ms");

            if(Config.PRINT_LEVEL > 0) System.out.println("\nTraining & Classifying of labels finished.");

        } catch (ParseException e) {

            // show error and command line options
            System.err.println("Error: " + e.getMessage());
            CliMLPUtil.formatHelp(optionsIO, optionsPlatform, optionsParameter, CliMLPUtil.getEvalComparator(),
                    "java -jar " + JAR_NAME + " [options] -train training_file -test test_file -l label_count");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PlatformUtil.requestShutdown();
        }
    }
}
