package executor.training;

import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params_dynamic.ParamsL1L2R1;
import util.MLPBenchmark;

public class ForwardLastExecutor {
    private Kernel kernel, kernel2;
    private ParamsL1L2R1 params;

    private int L1, L2;

    private interface K1_ARGS {
        int SIZE = 6;
        int Z = 0, W = 1, B = 2, L = 3, RES_G = 4, RESULT = 5;
    }

    private interface K2_ARGS {
        int SIZE = 5;
        int INT_RES = 0, B = 1, L = 2, RES_G = 3, RESULT = 4;
    }

    private ForwardLastExecutor(int L1, int L2, int R1, ParamsL1L2R1 params, boolean useCache) {
        this.L1 = L1;
        this.L2 = L2;
        this.params = params;

        long start = System.nanoTime();
        buildKernel(L1, L2, R1, useCache);
        long end = System.nanoTime();
        MLPBenchmark.forwardBuild += end - start;

        initArguments();
    }

    public ForwardLastExecutor(int L1, int L2, int R1) {
        this(L1, L2, R1, ParamsL1L2R1.getParamsForwardLastGemm(), true);
    }

    public ForwardLastExecutor(int L1, int L2, int R1, ParamsL1L2R1 params) {
        this(L1, L2, R1, params, false);
    }

    private void buildKernel(int L1, int L2, int R1, boolean useCache) {
        String options = params.getOptions(L1, L2, R1);
        long program = PlatformUtil.buildProgram("ocl_kernel/mlp/mlp_forward_last_1", options, useCache);

        kernel = new Kernel(program, "mlp_forward_last_1");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0, params.LOCAL_SIZE_DIM_1, params.LOCAL_SIZE_DIM_2);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0, params.GLOBAL_SIZE_DIM_1, params.GLOBAL_SIZE_DIM_2);
        kernel.setWorkDim(params.WORK_DIM);

        if(params.NUM_WG_R_1 > 1) {
            long program1 = PlatformUtil.buildProgram("ocl_kernel/mlp/mlp_forward_last_2", options, useCache);

            kernel2 = new Kernel(program1, "mlp_forward_last_2");
            kernel2.setLocalSize(params.K2_LOCAL_SIZE_DIM_0, params.K2_LOCAL_SIZE_DIM_1, params.K2_LOCAL_SIZE_DIM_2);
            kernel2.setGlobalSize(params.K2_GLOBAL_SIZE_DIM_0, params.K2_GLOBAL_SIZE_DIM_1, params.K2_GLOBAL_SIZE_DIM_2);
            kernel2.setWorkDim(params.WORK_DIM);
        }
    }

    private void initArguments() {
        kernel.initArgs(K1_ARGS.SIZE);
        kernel.setInputOutputArg(K1_ARGS.RES_G, 1, Kernel.TypeFlag.CACHE);

        if(params.NUM_WG_R_1 > 1) {
            kernel2.initArgs(K2_ARGS.SIZE);
            kernel2.setInputOutputArg(K2_ARGS.RES_G, 1, Kernel.TypeFlag.CACHE);
        }
    }

    private void updateArguments(long Z, long W, long L, long D, long B) {
        kernel.setArg(K1_ARGS.Z, Z, Kernel.TypeFlag.SHARED);
        kernel.setArg(K1_ARGS.W, W, Kernel.TypeFlag.SHARED);
        kernel.setArg(K1_ARGS.B, B, Kernel.TypeFlag.SHARED);
        kernel.setArg(K1_ARGS.L, L, Kernel.TypeFlag.SHARED);

        if(params.NUM_WG_R_1 == 1) {
            kernel.setArg(K1_ARGS.RESULT, D, Kernel.TypeFlag.SHARED);

            kernel.setArgs();
        } else {
            long k1_result = kernel.setIOArg(K1_ARGS.RESULT,L1 * L2 * 4 * params.NUM_WG_R_1, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.INT_RES, k1_result, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.B, B, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.L, L, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.RESULT, D, Kernel.TypeFlag.SHARED);

            kernel.setArgs();
            kernel2.setArgs();
        }
    }

    public void execute(long Z, long W, long L, long D, long B) {
        updateArguments(Z, W, L, D, B);

        if (PlatformUtil.PROFILING_ENABLED) {
            MLPBenchmark.forwardKernelTime += kernel.executeKernelProfile();

            if(params.NUM_WG_R_1 > 1) {
                MLPBenchmark.forwardKernelTime += kernel2.executeKernelProfile();
                kernel2.releaseArg(K2_ARGS.INT_RES);
            }
        } else {
            kernel.executeKernel();

            if(params.NUM_WG_R_1 > 1) {
                kernel2.executeKernel();
                kernel2.releaseArg(K2_ARGS.INT_RES);
            }
        }
    }

    public long profile(long Z, long W, long L, long D, long B) {
        updateArguments(Z, W, L, D, B);

        long runtime = kernel.executeKernelProfile();

        if(params.NUM_WG_R_1 > 1) {
            runtime += kernel2.executeKernelProfile();
            kernel2.releaseArg(K2_ARGS.INT_RES);
        }

        return runtime;
    }

    public void release() {
        if (kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
        if (kernel2 != null) {
            kernel2.releaseArgs();
            kernel2.releaseKernel();
        }
    }
}
