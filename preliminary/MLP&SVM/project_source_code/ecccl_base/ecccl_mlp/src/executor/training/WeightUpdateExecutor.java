package executor.training;

import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params_dynamic.ParamsL1L2L3;
import org.lwjgl.BufferUtils;
import util.MLPBenchmark;

import java.nio.FloatBuffer;

public class WeightUpdateExecutor {
    private ParamsL1L2L3 params;
    private Kernel kernel;

    // buffer for learning rate and momentum
    private FloatBuffer learningRateBuffer, momentumBuffer;

    public interface ARGS {
        int SIZE = 7;
        int Z = 0, D = 1, PREV_GRAD = 2, WEIGHT = 3, LEARNING_RATE = 4, MOMENTUM = 5, PREV_GRAD_TMP = 6;
    }

    private WeightUpdateExecutor(int L1, int L2, int L3, ParamsL1L2L3 params, boolean useCache) {
        this.params = params;

        long start = System.nanoTime();
        buildKernel(L1, L2, L3, useCache);
        long end = System.nanoTime();
        MLPBenchmark.weightUpdateBuild += end - start;

        initArgs();
    }

    public WeightUpdateExecutor(int L1, int L2, int L3) {
        this(L1, L2, L3, new ParamsL1L2L3(ParamsL1L2L3.TUNING_FILE_WEIGHT_UPDATE), true);
    }

    public WeightUpdateExecutor(int L1, int L2, int L3, ParamsL1L2L3 params) {
        this(L1, L2, L3, params, false);
    }

    private void initArgs() {
        learningRateBuffer = BufferUtils.createFloatBuffer(1);
        momentumBuffer = BufferUtils.createFloatBuffer(1);

        kernel.initArgs(ARGS.SIZE);
        kernel.setScalarArg(ARGS.LEARNING_RATE, learningRateBuffer);
        kernel.setScalarArg(ARGS.MOMENTUM, momentumBuffer);
    }

    private void buildKernel(int L1, int L2, int L3, boolean useCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/mlp/mlp_weight_update", params.getOptions(L1, L2, L3), useCache);

        kernel = new Kernel(program, "mlp_weight_update_1");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0, params.LOCAL_SIZE_DIM_1, params.LOCAL_SIZE_DIM_2);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0, params.GLOBAL_SIZE_DIM_1, params.GLOBAL_SIZE_DIM_2);
        kernel.setWorkDim(params.WORK_DIM);
    }

    private void updateArguments(long Z, long D, long weights, long prevGrad, long prevGradTmp, float learningRate, float momentum) {
        learningRateBuffer.put(learningRate).flip();
        momentumBuffer.put(momentum).flip();

        kernel.setArg(ARGS.Z, Z, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.D, D, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.PREV_GRAD, prevGrad, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.PREV_GRAD_TMP, prevGradTmp, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.WEIGHT, weights, Kernel.TypeFlag.SHARED);
        kernel.setArgs();
    }

    public void execute(long Z, long D, long weights, long prevGrad, long prevGradTmp, float learningRate, float momentum) {
        updateArguments(Z, D, weights, prevGrad, prevGradTmp, learningRate, momentum);

        if(PlatformUtil.PROFILING_ENABLED) {
            MLPBenchmark.weightUpdateKernelTime += kernel.executeKernelProfile();
        } else {
            kernel.executeKernel();
        }
    }

    public long profile(long Z, long D, long weights, long prevGrad, long prevGradTmp, float learningRate, float momentum) {
        updateArguments(Z, D, weights, prevGrad, prevGradTmp, learningRate, momentum);

        return kernel.executeKernelProfile();
    }

    public void release() {
        if (kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
    }
}
