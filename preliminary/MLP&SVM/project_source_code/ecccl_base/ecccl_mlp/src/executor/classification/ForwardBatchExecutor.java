package executor.classification;

import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params_dynamic.ParamsL1L2L3R1;
import util.MLPBenchmark;

public class ForwardBatchExecutor {
    private Kernel kernel, kernel2;
    private ParamsL1L2L3R1 params;

    private int L1, L2, L3, R1;

    private interface K1_ARGS {
        int SIZE = 5;
        int Z = 0, W = 1, B = 2, RES_G = 3, RESULT = 4;
    }

    private interface K2_ARGS {
        int SIZE = 4;
        int INT_RES = 0, B = 1, RES_G = 2, RESULT = 3;
    }

    private ForwardBatchExecutor(int L1, int L2, int L3, int R1, ParamsL1L2L3R1 params, boolean useCache) {
        this.L1 = L1;
        this.L2 = L2;
        this.L3 = L3;
        this.R1 = R1;
        this.params = params;

        long start = System.nanoTime();
        buildKernel(useCache);
        long end = System.nanoTime();
        MLPBenchmark.forwardBatchBuild += end - start;

        initArguments();
    }

    public ForwardBatchExecutor(int L1, int L2, int L3, int R1) {
        this(L1, L2, L3, R1, ParamsL1L2L3R1.getParamsForwardBatch(R1), true);
    }

    public ForwardBatchExecutor(int L1, int L2, int L3, int R1, ParamsL1L2L3R1 params) {
        this(L1, L2, L3, R1, params, false);
    }

    private void buildKernel(boolean useCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/mlp/mlp_forward_batch_bias_1",
                params.getOptions(L1, L2, L3, R1), useCache);

        kernel = new Kernel(program, "mlp_forward_batch_1");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0, params.LOCAL_SIZE_DIM_1, params.LOCAL_SIZE_DIM_2);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0, params.GLOBAL_SIZE_DIM_1, params.GLOBAL_SIZE_DIM_2);
        kernel.setWorkDim(params.WORK_DIM);

        if(params.NUM_WG_R_1 > 1) {
            long program1 = PlatformUtil.buildProgram("ocl_kernel/mlp/mlp_forward_batch_bias_2",
                    params.getOptions(L1, L2, L3, R1), useCache);

            kernel2 = new Kernel(program1, "mlp_forward_batch_2");
            kernel2.setLocalSize(params.K2_LOCAL_SIZE_DIM_0, params.K2_LOCAL_SIZE_DIM_1, params.K2_LOCAL_SIZE_DIM_2);
            kernel2.setGlobalSize(params.K2_GLOBAL_SIZE_DIM_0, params.K2_GLOBAL_SIZE_DIM_1, params.K2_GLOBAL_SIZE_DIM_2);
            kernel2.setWorkDim(params.WORK_DIM);
        }
    }

    private void initArguments() {
        kernel.initArgs(K1_ARGS.SIZE);
        kernel.setInputOutputArg(K1_ARGS.RES_G,1, Kernel.TypeFlag.CACHE);

        if(params.NUM_WG_R_1 > 1) {
            kernel2.initArgs(K2_ARGS.SIZE);
            kernel2.setInputOutputArg(K2_ARGS.RES_G,1, Kernel.TypeFlag.CACHE);
        }
    }

    private void updateArguments(long Z, long W, long nextZ, long B) {
        kernel.setArg(K1_ARGS.Z, Z, Kernel.TypeFlag.SHARED);
        kernel.setArg(K1_ARGS.B, B, Kernel.TypeFlag.SHARED);
        kernel.setArg(K1_ARGS.W, W, Kernel.TypeFlag.SHARED);

        if(params.NUM_WG_R_1 == 1) {
            kernel.setArg(K1_ARGS.RESULT, nextZ, Kernel.TypeFlag.SHARED);
            kernel.setArgs();
        } else {
            long k1_result = kernel.setIOArg(K1_ARGS.RESULT,L1 * L2 * L3 * 4 * params.NUM_WG_R_1, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.INT_RES, k1_result, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.B, B, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.RESULT, nextZ, Kernel.TypeFlag.SHARED);

            kernel.setArgs();
            kernel2.setArgs();
        }
    }

    public long execute(long Z, long W, long nextZ, long B) {
        updateArguments(Z, W, nextZ, B);

        if(PlatformUtil.PROFILING_ENABLED) {
            MLPBenchmark.forwardBatchKernelTime += kernel.executeKernelProfile();

            if(params.NUM_WG_R_1 > 1) {
                MLPBenchmark.forwardBatchKernelTime += kernel2.executeKernelProfile();
                kernel2.releaseArg(K2_ARGS.INT_RES);
            }
        } else {
            kernel.executeKernel();
            if(params.NUM_WG_R_1 > 1) {
                kernel2.executeKernel();
                kernel2.releaseArg(K2_ARGS.INT_RES);
            }
        }

        return nextZ;
    }

    public long profile(long Z, long W, long nextZ, long B) {
        updateArguments(Z, W, nextZ, B);

        long runtime = kernel.executeKernelProfile();

        if(params.NUM_WG_R_1 > 1) {
            runtime += kernel2.executeKernelProfile();

            kernel2.releaseArg(K2_ARGS.INT_RES);
        }

        return runtime;
    }

    public void release() {
        if (kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
        if (kernel2 != null) {
            kernel2.releaseArgs();
            kernel2.releaseKernel();
        }
    }
}
