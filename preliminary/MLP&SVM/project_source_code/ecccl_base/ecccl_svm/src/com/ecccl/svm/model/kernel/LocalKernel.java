package com.ecccl.svm.model.kernel;

public abstract class LocalKernel {
    private KernelVersion kernelVersion;

    public LocalKernel(KernelVersion kernelVersion) {
        this.kernelVersion = kernelVersion;
    }

    public KernelVersion getKernelVersion() {
        return kernelVersion;
    }

    public abstract float computeKernel(float[] attr1, float[] attr2, float[] label1, float[] label2,
                                        int initNumAttr, int numAttrAsLabels);
}
