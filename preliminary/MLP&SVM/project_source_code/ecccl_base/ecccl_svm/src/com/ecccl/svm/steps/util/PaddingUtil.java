package com.ecccl.svm.steps.util;

import opencl.params.ParamsL1L2R1;
import opencl.params.ParamsL1R1;

public class PaddingUtil {

    public static int getPaddingSV(int numSupportVectors) {
        ParamsL1L2R1 paramRBF = ParamsL1L2R1.getParamsRbf();
        ParamsL1R1 paramScore = ParamsL1R1.getParamsRbfScore();

        // get biggest factor for number of sv
        int biggestFactorSV = paramRBF.L_CB_SIZE_L_2 > paramScore.L_CB_SIZE_R_1
                ? paramRBF.L_CB_SIZE_L_2 : paramScore.L_CB_SIZE_R_1;

        // get biggest num wg: NUM_WG must be a factor of numSV / biggestFactorSV
        //int biggestNumWG = paramsKernel.NUM_WG_L_2 > paramsScore.NUM_WG_R_1
         //       ? paramsKernel.NUM_WG_L_2 : paramsScore.NUM_WG_R_1;

        int multiplierPadding = (int) Math.ceil((double) numSupportVectors / biggestFactorSV) * biggestFactorSV;

        //int numSVPadding = (int) Math.ceil((double) multiplierPadding / biggestNumWG) * biggestNumWG * biggestFactorSV;


        return multiplierPadding;
    }
}
