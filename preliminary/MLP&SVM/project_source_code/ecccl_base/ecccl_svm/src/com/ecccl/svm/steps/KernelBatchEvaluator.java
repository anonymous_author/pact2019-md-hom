package com.ecccl.svm.steps;

import model.InputData;
import model.ChainResults;
import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.executor.classifier.EvalKernelRBFExecutor;
import com.ecccl.svm.steps.util.PaddingUtil;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

public class KernelBatchEvaluator {
    private EvalKernelRBFExecutor kernelExecutor;

    private long instances;
    private long supportVectors;
    private long resultBatch;

    public KernelBatchEvaluator(InputData input, ModelSVM model, int batchSize, ChainResults chainResults) {

        // get meta data from model
        int numSupportVectors = model.getNumSupportVectors();
        int numAttributes = model.getNumAttributes();

        // calculate padding to make sure md_hom can execute step
        int numSVPadding = PaddingUtil.getPaddingSV(numSupportVectors);

        // load data to device
        loadInputToDevice(input, numAttributes, batchSize, chainResults);
        loadSupportVectorsToDevice(model.getSupportVectors(), model.getNumAttributes(), numSupportVectors, numSVPadding);

        allocateResultOnDevice(batchSize, numSVPadding);

        // initialize executor
        kernelExecutor = EvalKernelRBFExecutor.getInstance();
        kernelExecutor.updateArgs(numSVPadding, numAttributes, batchSize, instances, supportVectors, resultBatch);
    }

    public long getResultBatch() {
        return resultBatch;
    }

    /**
     * Executes the kernels to calculate the squared euclidean distance part of the
     * radial basis function evaluation.
     *
     * @param index index in input to start from (evaluates @code{batchSize} instances)
     */
    public void execute(int index) {
        kernelExecutor.execute(index);
    }

    public void release() {
        if(instances != 0L) OCLUtil.releaseMem(instances);
        if(supportVectors != 0L) OCLUtil.releaseMem(supportVectors);
        if(resultBatch != 0L) OCLUtil.releaseMem(resultBatch);
    }

    private void loadInputToDevice(InputData input, int numAttributes, int batchSize, ChainResults chainResults) {

        // make buffer big enough to cover last range execution
        int size = input.length();
        int rest = input.length() % batchSize;
        if(rest != 0){
            size += (batchSize - rest);
        }

        FloatBuffer testDataBuffer = BufferUtils.createFloatBuffer(size * numAttributes);
        for(int instanceIndex = 0; instanceIndex < input.length(); ++instanceIndex){

            // load initial attributes of instance
            testDataBuffer.put(input.getInstances()[instanceIndex], 0, input.getNumAttributes());

            // load label of already predicted classifiers of current chain
            for(int label = 0; label < numAttributes - input.getNumAttributes(); ++label){
                testDataBuffer.put(chainResults.getWithOrder(label, instanceIndex));
            }
        }

        // fill rest of batch size with 0
        testDataBuffer.put(new float[(size - input.length()) * numAttributes]);

        testDataBuffer.flip();
        instances = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, testDataBuffer);
    }

    private void loadSupportVectorsToDevice(float[] supportVectorsData, int numAttrSV, int numSV, int numSVPadding) {
        FloatBuffer inputSVBuffer = BufferUtils.createFloatBuffer(numSVPadding * numAttrSV);

        // load support vector data to gpu
        inputSVBuffer.put(supportVectorsData, 0, numSV * numAttrSV);
        inputSVBuffer.put(new float[(numSVPadding - numSV) * numAttrSV]);

        inputSVBuffer.flip();
        supportVectors = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, inputSVBuffer);
    }

    private void allocateResultOnDevice(int batchSize, int numSVPadding) {
        resultBatch = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, batchSize * numSVPadding * 4);
    }
}
