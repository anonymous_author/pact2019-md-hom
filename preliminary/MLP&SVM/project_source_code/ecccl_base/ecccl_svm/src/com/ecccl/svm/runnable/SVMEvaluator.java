package com.ecccl.svm.runnable;

import com.ecccl.svm.EccSVM;
import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.EccSVMTester;
import com.ecccl.svm.util.SVMBenchmark;
import evaluator.Evaluator;
import model.LabelResults;
import opencl.PlatformUtil;
import org.apache.commons.cli.*;
import com.ecccl.svm.util.CliSVMUtil;
import util.Config;
import model.InputData;
import util.WekaLoader;

import java.util.Arrays;
import java.util.List;

public class SVMEvaluator {
    private static final String JAR_NAME = "mdSVM-evaluate.jar";
    private static final String DEFAULT_OUTPUT_PATH = "./outputs/";

    private static final float DEFAULT_THRESHOLD_ECC = 0.5f;

    public static void main(String[] args) {

        // Input and Output options
        Options optionsIO = new Options();
        optionsIO.addOption(Option.builder("train").hasArg().desc("training data file (required)").required().build());
        optionsIO.addOption(Option.builder("test").hasArg().desc("test data file (required)").required().build());
        optionsIO.addOption(Option.builder("l").longOpt("label").hasArg().desc("number of labels (required)").required().build());
        optionsIO.addOption(Option.builder("o").longOpt("output").hasArg().desc("output path (default ./outputs/)").build());
        optionsIO.addOption(Option.builder("q").longOpt("quiet").desc("quiet mode").build());

        // Platform options
        Options optionsPlatform = CliSVMUtil.getPlatformOptions();

        // SVM parameter options
        Options optionsParameter = CliSVMUtil.getSVMOptions();
        optionsParameter.addOption(Option.builder("tecc").longOpt("threshold")
                .desc("threshold for ECC (default: " + DEFAULT_THRESHOLD_ECC + ")").build());

        // collect all options
        Options options = new Options();
        for(Option option : optionsIO.getOptions()) options.addOption(option);
        for(Option option : optionsPlatform.getOptions()) options.addOption(option);
        for(Option option : optionsParameter.getOptions()) options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            // parse i/o
            String input = cmd.getOptionValue("train");
            String testStr = cmd.getOptionValue("test");
            int labelCount = Integer.parseInt(cmd.getOptionValue("l"));
            String outputPath = cmd.hasOption("o") ? cmd.getOptionValue("o") : DEFAULT_OUTPUT_PATH;

            // quiet mode
            boolean quietMode = cmd.hasOption("q");
            if(quietMode) Config.PRINT_LEVEL = 0;

            // parse platform
            CliSVMUtil.parsePlatformOptions(cmd);
            PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

            // load input and test data set
            InputData train = WekaLoader.load(input, labelCount);
            InputData test = WekaLoader.load(testStr, labelCount);

            // create classifier from options
            int numCC = CliSVMUtil.parseInt("n", cmd, CliSVMUtil.DEFAULT_NUM_CC);
            float thresholdECC = CliSVMUtil.parseFloat("tecc", cmd, CliSVMUtil.DEFAULT_THRESHOLD_ECC);
            EccSVM eccSVM = CliSVMUtil.getSVMFromOptions(cmd);

            // run training
            long startTrain = System.currentTimeMillis();
            List<ModelSVM> models = eccSVM.train(train);
            long endTrain = System.currentTimeMillis();

            // calculate average and median support vector size
            int averageSV = 0;
            int[] numSV = new int[models.size()];
            for(int i = 0; i < models.size(); i++) {
                averageSV += models.get(i).getNumSupportVectors();
                numSV[i] = models.get(i).getNumSupportVectors();
            }
            Arrays.sort(numSV);
            int medianSV = numSV[models.size() / 2];
            averageSV /= models.size();

            // create tester
            EccSVMTester eccSVMTester = new EccSVMTester(numCC, thresholdECC);

            // run classification
            long startEval = System.currentTimeMillis();
            LabelResults results = eccSVMTester.classifyECC(test, models);
            long endEval = System.currentTimeMillis();

            // run evaluation
            Evaluator.evaluate(test, results, outputPath);

            // output runtime
            System.out.println("\nTraining: " + (endTrain - startTrain) + " ms");
            System.out.println("Training (without building kernels): " + (endTrain - startTrain - SVMBenchmark.getBuildTimeTraining()) + " ms");

            if(Config.PRINT_LEVEL > 0) System.out.println("\nAverage support vectors: " + averageSV);
            if(Config.PRINT_LEVEL > 0) System.out.println("Median support vectors: " + medianSV);

            System.out.println("\nClassifying: " + (endEval - startEval) + " ms");
            System.out.println("Classifying (without building kernels): " + (endEval - startEval - SVMBenchmark.getBuildTimeTesting()) + " ms");

            if(Config.PRINT_LEVEL > 0) System.out.println("\nTraining & Classifying of labels finished.");

        } catch (ParseException e) {

            // show error and command line options
            System.err.println("Error: " + e.getMessage());
            CliSVMUtil.formatHelp(optionsIO, optionsPlatform, optionsParameter,
                    CliSVMUtil.ORDER_INPUT_EVALUATION, CliSVMUtil.ORDER_EVALUATION,
                    "java -jar " + JAR_NAME + " [options] -train training_file -test test_file -l label_count");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PlatformUtil.requestShutdown();
        }
    }
}
