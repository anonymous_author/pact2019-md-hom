package com.ecccl.svm.steps;

import com.ecccl.svm.executor.training.UpdateBiasExecutor;
import com.ecccl.svm.tasks.util.BiasResult;

public class WorkingSetSelection {

    // Executor
    private UpdateBiasExecutor updateBiasExecutor;

    public WorkingSetSelection(int numInstances, long list, long error) {

        updateBiasExecutor = UpdateBiasExecutor.getInstance(numInstances, list, error);
    }

    /**
     * Executes the Working Set Selection and selects the two indices
     * for the next iteration of the algorithm.
     *
     * @return next i_up and i_low
     */
    public BiasResult run() {
        return updateBiasExecutor.execute();
    }
}
