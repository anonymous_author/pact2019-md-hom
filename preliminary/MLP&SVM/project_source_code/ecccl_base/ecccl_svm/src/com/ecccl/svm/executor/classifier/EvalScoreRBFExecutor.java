package com.ecccl.svm.executor.classifier;

import com.ecccl.svm.util.SVMBenchmark;
import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params.ParamsL1R1;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * This class manages the OpenCL kernel execution of computing
 * the score of an instance.
 *
 * @author
 */
public class EvalScoreRBFExecutor {
    private static EvalScoreRBFExecutor instance = null;

    private static ParamsL1R1 params;
    private static Kernel kernel;
    private static Kernel kernel2;

    // input/output
    private FloatBuffer scoresBuffer;
    private FloatBuffer gammaBuffer;

    // input sizes
    private IntBuffer rangeCountBuffer;
    private IntBuffer inputSvCountBuffer;
    private IntBuffer K2_R1Buffer;

    private int batchSize;

    private interface ARGS {
        int SIZE = 8;
        int L1 = 0, R1 = 1;
        int GAMMA = 2, MATRIX_RESULT = 3, ALPHA = 4, LABEL = 5;
        int RES_G = 6, RESULT = 7;
    }

    private interface K2_ARGS {
        int SIZE = 5;
        int L1 = 0, R1 = 1;
        int INT_RES = 2, RES_G = 3, RESULT = 4;
    }

    public static EvalScoreRBFExecutor getInstance(int batchSize) {
        if (instance == null) {
            ParamsL1R1 params = ParamsL1R1.getParamsRbfScore();
            instance = new EvalScoreRBFExecutor(params, batchSize, true);
        }
        return instance;
    }

    public static EvalScoreRBFExecutor getInstance(ParamsL1R1 params, int batchSize) {
        if (instance == null) {
            instance = new EvalScoreRBFExecutor(params,batchSize, false);
        }
        return instance;
    }

    private EvalScoreRBFExecutor(ParamsL1R1 _params, int batchSize, boolean useCache) {
        params = _params;
        this.batchSize = batchSize;

        long start = System.nanoTime();
        buildKernel(useCache);
        long end = System.nanoTime();
        SVMBenchmark.buildEvalScore += end - start;

        createBuffer();
        initArgs();
    }

    private void buildKernel(boolean useCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_reduce_rbf_eval",
                params.getOptions(), useCache);

        kernel = new Kernel(program, "svm_reduce_rbf_eval_1");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0, params.LOCAL_SIZE_DIM_1);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0, params.GLOBAL_SIZE_DIM_1);
        kernel.setWorkDim(params.WORK_DIM);

        if(params.NUM_WG_R_1 > 1) buildKernel2(useCache);
    }

    private void buildKernel2(boolean useCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_reduce_rbf_eval_2",
                params.getOptions(), useCache);

        kernel2 = new Kernel(program, "svm_reduce_rbf_eval_2");
        kernel2.setLocalSize(params.K2_LOCAL_SIZE_DIM_0, params.K2_LOCAL_SIZE_DIM_1);
        kernel2.setGlobalSize(params.K2_GLOBAL_SIZE_DIM_0, params.K2_GLOBAL_SIZE_DIM_1);
        kernel2.setWorkDim(params.WORK_DIM);
    }

    private void createBuffer() {
        gammaBuffer = BufferUtils.createFloatBuffer(1);
        rangeCountBuffer = BufferUtils.createIntBuffer(1);
        inputSvCountBuffer = BufferUtils.createIntBuffer(1);
        K2_R1Buffer = BufferUtils.createIntBuffer(1);
        scoresBuffer = BufferUtils.createFloatBuffer(batchSize);
    }

    public void updateArgs(int numSv, float gamma, long result, long alpha, long label) {

        // set dimensions of kernel
        rangeCountBuffer.put(batchSize).flip();
        inputSvCountBuffer.put(numSv).flip();
        if(params.NUM_WG_R_1 > 1) K2_R1Buffer.put(params.K2_G_CB_SIZE_R_1(numSv)).flip();

        // set gamma value
        gammaBuffer.put(gamma).flip();

        // set input of kernel
        kernel.setArg(ARGS.MATRIX_RESULT, result, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.ALPHA, alpha, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.LABEL, label, Kernel.TypeFlag.SHARED);

        // set output of kernel/kernel2
        if(params.NUM_WG_R_1 > 1) {
            kernel2.setArgs();
        }
        kernel.setArgs();
    }

    private void initArgs() {
        // set static input of kernel
        kernel.initArgs(ARGS.SIZE);
        kernel.setInputArg(ARGS.L1, rangeCountBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(ARGS.R1, inputSvCountBuffer, Kernel.TypeFlag.WRITE_SCALAR);

        kernel.setInputArg(ARGS.GAMMA, gammaBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputOutputArg(ARGS.RES_G, 1, Kernel.TypeFlag.CACHE);

        // set static input of kernel2
        if(params.NUM_WG_R_1 > 1) {
            kernel2.initArgs(K2_ARGS.SIZE);
            kernel2.setScalarArg(K2_ARGS.L1, rangeCountBuffer);
            kernel2.setScalarArg(K2_ARGS.R1, K2_R1Buffer);
            kernel2.setInputOutputArg(K2_ARGS.RES_G, 1, Kernel.TypeFlag.CACHE);

            long k1Result = kernel.setIOArg(ARGS.RESULT, batchSize * params.NUM_WG_R_1 * 4, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.INT_RES, k1Result, Kernel.TypeFlag.CACHE);

            kernel2.setOutputArg(K2_ARGS.RESULT, scoresBuffer, Kernel.TypeFlag.READ);
        } else {
            kernel.setOutputArg(ARGS.RESULT, scoresBuffer, Kernel.TypeFlag.READ);
        }
    }

    public float[] execute(int count, float bias) {
        if(PlatformUtil.PROFILING_ENABLED){
            SVMBenchmark.runEvalScore += kernel.executeKernelProfile();

            if(params.NUM_WG_R_1 > 1) {
                kernel2.setScalarArgs();
                SVMBenchmark.runEvalScore += kernel2.executeKernelProfile();
                SVMBenchmark.runEvalScore += kernel2.readArgsProfile();
            } else {
                SVMBenchmark.runEvalScore += kernel.readArgsProfile();
            }
        } else {
            kernel.executeKernel();
            if(params.NUM_WG_R_1 > 1) {
                kernel2.setScalarArgs();
                kernel2.executeKernel();

                kernel2.readArgs();
            } else {
                kernel.readArgs();
            }
        }

        float[] scores = new float[count];
        for(int i = 0; i < count; i++) {
            scores[i] = (scoresBuffer.get(i) - bias) >= 0 ? 1.0f : 0f;
        }

        return scores;
    }

    public long profile() {
        long runtime = 0;

        runtime += kernel.executeKernelProfile();
        if(params.NUM_WG_R_1 > 1) {
            kernel2.setScalarArgs();
            runtime += kernel2.executeKernelProfile();
        }

        return runtime;
    }

    public static void releaseKernel() {
        if (kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
        if (kernel2 != null) {
            kernel2.releaseArgs();
            kernel2.releaseKernel();
        }
    }
}
