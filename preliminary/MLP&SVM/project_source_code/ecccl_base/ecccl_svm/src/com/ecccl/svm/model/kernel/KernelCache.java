package com.ecccl.svm.model.kernel;

import model.ChainData;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * This class caches the calculated kernel values on the CPU in a {@code LinkedHashMap}.
 *
 * @author
 */
public class KernelCache {
    private int size;
    private LinkedHashMap<Long, Float> cache;
    private LocalKernel kernel;
    private int numInst;
    private ChainData data;

    /**
     * @param data input dataset
     * @param kernel kernel function
     * @param sizeMB size of cache in MB
     */
    public KernelCache(ChainData data, LocalKernel kernel, int sizeMB) {
        this.data = data;
        this.size = sizeMB * 1000 * 1000 / 4;
        this.cache = new LinkedHashMap<>();
        this.numInst = data.length();
        this.kernel = kernel;
    }

    /**
     * Returns the value in the kernel matrix and updates the use of it in
     * the cache.
     *
     * @param i1 x-index in kernel matrix
     * @param i2 y-index in kernel matrix
     * @return value in kernel matrix
     */
    //
    public synchronized float get(int i1, int i2){
        return put(i1, i2);
    }

    /**
     * Three cases:
     * 1. If cache contains key, then update it
     * 2. If cache is full, then remove first in cache and put new key with value of removed element
     * 3. Otherwise compute kernel value and put it into the cache
     *
     * @param i1 x-index in kernel matrix
     * @param i2 y-index in kernel matrix
     * @return value which is put in the cache
     */
    public synchronized float put(int i1, int i2) {
        if(cache.containsKey((long) i1 * numInst + i2)){
            float val = cache.get((long) i1 * numInst + i2);
            cache.remove((long) i1 * numInst + i2);
            cache.put((long) i1 * numInst + i2, val);
            return val;
        } else if (cache.size() == size) {
            Iterator<Long> iter = cache.keySet().iterator();
            iter.next();
            iter.remove();
        }
        float res = kernel.computeKernel(data.getAttributes(i1), data.getAttributes(i2),
                data.getLabel(i1), data.getLabel(i2),
                data.getInitNumAttributes(), data.getLabelAsAttrCount());

        cache.put((long) i1 * numInst + i2, res);
        return res;
    }

    public synchronized int size() {
        return cache.size();
    }
}