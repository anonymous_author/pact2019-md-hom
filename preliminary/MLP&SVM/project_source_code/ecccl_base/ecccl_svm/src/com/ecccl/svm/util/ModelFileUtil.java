package com.ecccl.svm.util;

import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.model.kernel.KernelVersion;
import com.ecccl.svm.model.kernel.LinearKernel;
import com.ecccl.svm.model.kernel.LocalKernel;
import com.ecccl.svm.model.kernel.RBFKernel;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ModelFileUtil {

    public static void saveMetaECC(int eccCount, int numLabels, String path) {
        File outFile = new File(path + "ecc");
        if(outFile.getParentFile() != null)
            outFile.getParentFile().mkdirs();

        try {
            PrintWriter writer = new PrintWriter(path + "ecc", "UTF-8");
            writer.println(eccCount);
            writer.println(numLabels);
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static int[] readMetaECC(String path) throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader(path))) {
            int[] metadata = new int[2];
            metadata[0] = Integer.parseInt(br.readLine());
            metadata[1] = Integer.parseInt(br.readLine());
            return metadata;
        } catch (Exception e) {
            System.err.println("Could not read ecc meta data");
            throw e;
        }
    }

    public static void storeModels(List<ModelSVM> models, int chainCount, String modelFolderPath) {
        System.out.println("Storing models in: " + modelFolderPath);
        int numLabels = models.size() / chainCount;
        saveMetaECC(chainCount, numLabels, modelFolderPath);

        for (int chainId = 0; chainId < chainCount; chainId++) {
            for (int labelId = 0; labelId < numLabels; labelId++) {
               saveModelAsFile(models.get(chainId * numLabels + labelId), modelFolderPath + labelId + "-" + chainId);
            }
        }
    }

    private static void saveModelAsFile(ModelSVM model, String path) {
        File outFile = new File(path);
        if(outFile.getParentFile() != null)
            outFile.getParentFile().mkdirs();

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path), 8192 * 8);
            StringBuilder sb = new StringBuilder();
            sb.append(model.getNumSupportVectors()).append("\n");
            sb.append(model.getNumAttributes()).append("\n");
            sb.append(model.getLabelIndex()).append("\n");

            sb.append(model.getB()).append("\n");

            LocalKernel kernel = model.getKernel();
            if(kernel.getKernelVersion() == KernelVersion.LINEAR){
                sb.append(0).append("\n");
            } else if(kernel.getKernelVersion() == KernelVersion.RBF){
                sb.append(1).append(" ").append(((RBFKernel) kernel).getGamma()).append("\n");
            }

            float[] supportVectors = model.getSupportVectors();
            for(int svId = 0; svId < model.getNumSupportVectors(); svId++) {
                sb.append(model.getAlpha(svId)).append(";");
                sb.append(model.getLabel(svId)).append(";");
                for(int j = 0; j < model.getNumAttributes(); j++) {
                    if(supportVectors[svId * model.getNumAttributes() + j] != 0) {
                        sb.append(j).append(":").append(supportVectors[svId * model.getNumAttributes() + j]).append(" ");
                    }
                }
                sb.append("\n");
            }
            writer.append(sb);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<ModelSVM> restoreModels(String modelFolderPath) throws IOException {
        System.out.println("Restoring models from: " + modelFolderPath);

        List<ModelSVM> models = new ArrayList<>();
        int[] metadata = readMetaECC(modelFolderPath + "ecc");

        int chainCount = metadata[0];
        int numLabels = metadata[1];
        for (int chainId = 0; chainId < chainCount; chainId++) {
            for (int labelId = 0; labelId < numLabels; labelId++) {
                models.add(readModelFromFile(modelFolderPath + labelId + "-" + chainId));
            }
        }

        return models;
    }

    public static ModelSVM readModelFromFile(String path) {
        try(BufferedReader br = new BufferedReader(new FileReader(path))) {
            String[] fields;

            // read meta data from model
            int numInstances = Integer.parseInt(br.readLine());
            int numAttributes = Integer.parseInt(br.readLine());
            int labelIndex = Integer.parseInt(br.readLine());

            // read bias b
            float b = Float.parseFloat(br.readLine());

            // read kernel
            LocalKernel kernel;
            String line = br.readLine();
            fields = line.split(" ");
            if(Integer.parseInt(fields[0]) == 0) {
                kernel = new LinearKernel();
            } else {
                kernel = new RBFKernel(Float.parseFloat(fields[1]));
            }

            // read support vectors (attributes and label) and alphas
            float[] sv = new float[numInstances * numAttributes];
            float[] label = new float[numInstances];
            float[] alphas = new float[numInstances];

            int i = 0;
            line = br.readLine();
            while(line != null && !line.equals("")){
                fields = line.split(";");

                alphas[i] = Float.parseFloat(fields[0]);
                label[i] = Float.parseFloat(fields[1]);
                String[] attributes;
                if(fields.length == 3) attributes = fields[2].trim().split(" ");
                else attributes = new String[0];

                for (String attrStr : attributes) {
                    String[] attribute = attrStr.split(":");
                    sv[i * numAttributes + Integer.parseInt(attribute[0])] = Float.parseFloat(attribute[1]);
                }
                i++;

                line = br.readLine();
            }

            return new ModelSVM(b, kernel, sv, label, alphas, numAttributes, numInstances, labelIndex);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.print("Can't read model file.");
        }
        return null;
    }
}
