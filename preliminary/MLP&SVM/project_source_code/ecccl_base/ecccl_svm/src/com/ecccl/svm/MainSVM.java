package com.ecccl.svm;

import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.model.kernel.LocalKernel;
import com.ecccl.svm.model.kernel.RBFKernel;
import com.ecccl.svm.util.SVMBenchmark;
import evaluator.Evaluator;
import model.InputData;
import model.LabelResults;
import opencl.PlatformUtil;
import util.Config;
import util.WekaLoader;

import java.util.List;

public class MainSVM {
    public static long PROFILING_TIME = 0;

    public static void main(String[] args) throws Exception {
        PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

        InputData train = WekaLoader.load("data/ecccl/eurlex-sm-fold1-train-norm-1L", 1);
        InputData test = WekaLoader.load("data/ecccl/eurlex-sm-fold1-test-norm-1L", 1);

        // normalize if data is numeric and every attribute not in the same range
        //NormalizeUtil.normalizeInput(train, test, NormalizeUtil.NormType.FEATURE_SCALING_0_1);

        // Parameter
        float C = 1;
        float tol = 0.001f;
        LocalKernel kernel  = new RBFKernel(0.01f);
        int sizeCacheMB = 3800;

        int seed = 0;
        int chainCount = 1;
        int subsetPercentage = 100;
        float thresholdECC = 0.5f;

        // Training
        EccSVM eccSVM = new EccSVM.Builder()
                .setC(C)
                .setTol(tol)
                .setKernel(kernel)
                .setSizeCacheMB(sizeCacheMB)
                .setSeed(seed)
                .setChainCount(chainCount)
                .setSubsetPercentage(subsetPercentage)
                .build();


        long startTrain = System.currentTimeMillis();
        List<ModelSVM> models = eccSVM.train(train);
        long endTrain = System.currentTimeMillis();

        //ModelFileUtil.storeModels(models, chainCount,"./models/");

        //List<ModelSVM> models = ModelFileUtil.restoreModels("./models/");

        //Thread.sleep(10000);

        // Testing
        EccSVMTester eccSVMTester = new EccSVMTester(chainCount, thresholdECC);

        long startEval = System.currentTimeMillis();
        LabelResults results = eccSVMTester.classifyECC(test, models);
        long endEval = System.currentTimeMillis();

        Evaluator.evaluate(test, results, "./outputs/");

        System.out.println("\nTraining: " + (endTrain - startTrain) + " ms");
        System.out.println("Training (ohne Bauen): " + (endTrain - startTrain - SVMBenchmark.getBuildTimeTraining()) + " ms");
        System.out.println("\nClassifying: " + (endEval - startEval) + " ms");
        System.out.println("Classifying (ohne Bauen): " + (endEval - startEval - SVMBenchmark.getBuildTimeTesting()) + " ms");

        PlatformUtil.requestShutdown();
    }
}
