package com.ecccl.svm.model.kernel;

/**
 * This class represents the linear kernel: u*v.
 *
 * @author
 */
public class LinearKernel extends LocalKernel {
    public LinearKernel() {
        super(KernelVersion.LINEAR);
    }

    @Override
    public float computeKernel(float[] attr1, float[] attr2, float[] label1, float[] label2,
                               int initNumAttr, int numAttrAsLabels) {
        float result = 0;
        for (int i = 0; i < initNumAttr; ++i) {
            result += attr1[i] * attr2[i];
        }
        for (int i = 0; i < numAttrAsLabels; ++i) {
            result += label1[i] * label2[i];
        }
        return result;
    }
}
