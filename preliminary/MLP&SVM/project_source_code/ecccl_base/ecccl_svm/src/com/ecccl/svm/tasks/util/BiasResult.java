package com.ecccl.svm.tasks.util;

public class BiasResult {
    private float b_low;
    private int i_low;
    private float b_up;
    private int i_up;

    public BiasResult(float b_low, int i_low, float b_up, int i_up) {
        this.b_low = b_low;
        this.i_low = i_low;
        this.b_up = b_up;
        this.i_up = i_up;
    }

    public float b_low() {
        return b_low;
    }

    public int i_low() {
        return i_low;
    }

    public float b_up() {
        return b_up;
    }

    public int i_up() {
        return i_up;
    }
}
