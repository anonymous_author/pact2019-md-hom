package com.ecccl.svm.tasks.util;

import java.util.Iterator;
import java.util.LinkedHashMap;

public class CacheLRU {
    private long size;
    private LinkedHashMap<Integer, Integer> cacheIndices;
    private int counter;

    public CacheLRU(long size) {
        this.size = size;
        this.cacheIndices = new LinkedHashMap<>();
        this.counter = 0;
    }

    public void reset() {
        this.cacheIndices = new LinkedHashMap<>();
        this.counter = 0;
    }

    // if key is in map then update with put, otherwise ignore with -1
    public synchronized Integer get(Integer key){
        Integer val = cacheIndices.get(key);
        if(val == null) val = -1;
        else val = put(key);
        return val;
    }

    /**
     * a. If cache contains key, then update it
     * b. If cache is full, then remove first in cache and put new key with value of removed element
     * c. Otherwise put key with current counter and increment counter
     */
    public synchronized Integer put(Integer key) {
        Integer val;
        if(cacheIndices.containsKey(key)){
            val = cacheIndices.get(key);
            cacheIndices.remove(key);
            cacheIndices.put(key, val);
            return val;
        } else if (cacheIndices.size() == size) {
            Iterator<Integer> iter = cacheIndices.keySet().iterator();
            val = cacheIndices.get(iter.next());
            iter.remove();
            cacheIndices.put(key, val);
            return val;
        } else {
            cacheIndices.put(key, counter);
            counter++;
            return counter - 1;
        }
    }

    public synchronized boolean contains(Integer key){

        return cacheIndices.containsKey(key);
    }

    private synchronized int size() {
        return cacheIndices.size();
    }
}