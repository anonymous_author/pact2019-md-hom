package com.ecccl.svm.executor.classifier;

import com.ecccl.svm.util.SVMBenchmark;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params.ParamsL1L2R1;
import org.lwjgl.BufferUtils;

import java.nio.IntBuffer;

/**
 * This class manages the OpenCL kernel execution of computing
 * multiple row kernel evaluations.
 *
 * @author
 */
public class EvalKernelRBFExecutor {
    private static EvalKernelRBFExecutor instance = null;
    private static ParamsL1L2R1 params;
    private static Kernel kernel;
    private static Kernel kernel2;

    private IntBuffer indexBuffer;
    private IntBuffer rangeCountBuffer;
    private IntBuffer inputSvCountBuffer;
    private IntBuffer attributeCountBuffer;
    private IntBuffer K2_R1Buffer;

    private long k1ResultPtr;

    private interface K1_ARGS {
        int SIZE = 8;
        int L1 = 0, L2 = 1, R1 = 2;
        int INDEX = 3, TEST_DATA = 4, INPUT_SV = 5;
        int RES_G = 6, RESULT = 7;
    }

    private interface K2_ARGS {
        int SIZE = 5;
        int L1 = 0, L2 = 1, R1 = 2;
        int INT_RES = 3; //RES_G = 4,
        int RESULT = 4;
    }

    public static EvalKernelRBFExecutor getInstance() {
        if (instance == null) {
            ParamsL1L2R1 params = ParamsL1L2R1.getParamsRbf();
            instance = new EvalKernelRBFExecutor(params, true);
        }
        return instance;
    }

    public static EvalKernelRBFExecutor getInstance(ParamsL1L2R1 params) {
        if (instance == null) {
            instance = new EvalKernelRBFExecutor(params,false);
        }
        return instance;
    }

    private EvalKernelRBFExecutor(ParamsL1L2R1 _params, boolean useCache) {
        params = _params;

        long start = System.nanoTime();
        buildKernel(useCache);
        long end = System.nanoTime();
        SVMBenchmark.buildEvalKernel += end - start;

        createBuffer();
        initArgs();
    }

    private void buildKernel(boolean useCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_rbf_kernel_batch", params.getOptions(), useCache);

        kernel = new Kernel(program, "svm_compute_rbf_1");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0, params.LOCAL_SIZE_DIM_1, params.LOCAL_SIZE_DIM_2);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0, params.GLOBAL_SIZE_DIM_1, params.GLOBAL_SIZE_DIM_2);
        kernel.setWorkDim(params.WORK_DIM);

        if(params.NUM_WG_R_1 > 1) buildKernel2(useCache);
    }

    private void buildKernel2(boolean useCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_rbf_kernel_batch_2", params.getOptions(), useCache);

        kernel2 = new Kernel(program, "svm_compute_rbf_2");
        kernel2.setLocalSize(params.K2_LOCAL_SIZE_DIM_0, params.K2_LOCAL_SIZE_DIM_1, params.K2_LOCAL_SIZE_DIM_2);
        kernel2.setGlobalSize(params.K2_GLOBAL_SIZE_DIM_0, params.K2_GLOBAL_SIZE_DIM_1, params.K2_GLOBAL_SIZE_DIM_2);
        kernel2.setWorkDim(params.WORK_DIM);
    }

    private void createBuffer() {
        indexBuffer = BufferUtils.createIntBuffer(1);
        rangeCountBuffer = BufferUtils.createIntBuffer(1);
        inputSvCountBuffer = BufferUtils.createIntBuffer(1);
        attributeCountBuffer = BufferUtils.createIntBuffer(1);
        K2_R1Buffer = BufferUtils.createIntBuffer(1);
    }

    public void updateArgs(int numSv, int numAttributes, int range, long testData, long inputSV, long result) {

        // update dimensions of kernel
        rangeCountBuffer.put(range).flip();
        inputSvCountBuffer.put(numSv).flip();
        attributeCountBuffer.put(numAttributes).flip();
        if(params.NUM_WG_R_1 > 1) K2_R1Buffer.put(params.K2_G_CB_SIZE_R_1(numAttributes)).flip();

        // update input data of kernel
        kernel.setArg(K1_ARGS.TEST_DATA, testData, Kernel.TypeFlag.SHARED);
        kernel.setArg(K1_ARGS.INPUT_SV, inputSV, Kernel.TypeFlag.SHARED);

        // update result of kernel/kernel2 depending on NUM_WG_R_1
        if(params.NUM_WG_R_1 == 1) {
            kernel.setArg(K1_ARGS.RESULT, result, Kernel.TypeFlag.SHARED);
            kernel.setArgs();
        } else {
            if(k1ResultPtr != 0L) OCLUtil.releaseMem(k1ResultPtr);

            k1ResultPtr = kernel.setIOArg(K1_ARGS.RESULT,numSv * range * 4 * params.NUM_WG_R_1, Kernel.TypeFlag.SHARED);
            kernel2.setArg(K2_ARGS.INT_RES, k1ResultPtr, Kernel.TypeFlag.CACHE);
            kernel2.setArg(K2_ARGS.RESULT, result, Kernel.TypeFlag.SHARED);

            kernel.setArgs();
            kernel2.setArgs();
        }
    }

    private void initArgs() {
        kernel.initArgs(K1_ARGS.SIZE);

        // set dimensions of kernel
        kernel.setInputArg(K1_ARGS.L1, rangeCountBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(K1_ARGS.L2, inputSvCountBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(K1_ARGS.R1, attributeCountBuffer, Kernel.TypeFlag.WRITE_SCALAR);

        // set input data of kernel
        kernel.setInputArg(K1_ARGS.INDEX, indexBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputOutputArg(K1_ARGS.RES_G, 1, Kernel.TypeFlag.CACHE);

        // set dimensions of kernel2
        if(params.NUM_WG_R_1 > 1) {
            kernel2.initArgs(K2_ARGS.SIZE);
            kernel2.setScalarArg(K2_ARGS.L1, rangeCountBuffer);
            kernel2.setScalarArg(K2_ARGS.L2, inputSvCountBuffer);
            kernel2.setScalarArg(K2_ARGS.R1, K2_R1Buffer);
        }
    }

    public void execute(int index) {
        indexBuffer.put(index).flip();

        kernel.setScalarArgs();
        if(PlatformUtil.PROFILING_ENABLED){
            SVMBenchmark.runEvalKernel += kernel.executeKernelProfile();
            if(params.NUM_WG_R_1 > 1) {
                kernel2.setScalarArgs();
                SVMBenchmark.runEvalKernel += kernel2.executeKernelProfile();
            }
        } else {
            kernel.executeKernel();
            if(params.NUM_WG_R_1 > 1) {
                kernel2.setScalarArgs();
                kernel2.executeKernel();
            }
        }
    }

    public long profile(int index) {
        indexBuffer.put(index).flip();

        long runtime = 0;

        kernel.setScalarArgs();
        runtime += kernel.executeKernelProfile();

        if(params.NUM_WG_R_1 > 1) {
            kernel2.setScalarArgs();
            runtime += kernel2.executeKernelProfile();
        }

        return runtime;
    }

    public static void releaseKernel() {
        if (kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
        if (kernel2 != null) {
            kernel2.releaseArgs();
            kernel2.releaseKernel();
        }
    }
}
