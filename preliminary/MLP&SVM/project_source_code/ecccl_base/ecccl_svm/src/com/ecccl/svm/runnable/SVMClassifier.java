package com.ecccl.svm.runnable;

import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.EccSVMTester;
import com.ecccl.svm.util.ModelFileUtil;
import com.ecccl.svm.util.SVMBenchmark;
import evaluator.Evaluator;
import model.LabelResults;
import opencl.PlatformUtil;
import org.apache.commons.cli.*;
import com.ecccl.svm.util.CliSVMUtil;
import util.Config;
import model.InputData;
import util.WekaLoader;

import java.io.IOException;
import java.util.List;

public class SVMClassifier {
    private static final String JAR_NAME = "mdSVM-classify.jar";

    private static final String DEFAULT_MODEL_PATH = "./models/";
    private static final String DEFAULT_OUTPUT_PATH = "./outputs/";

    public static void main(String[] args) throws IOException {

        // Input and Output options
        Options optionsIO = new Options();
        optionsIO.addOption(Option.builder("test").hasArg().desc("test file (required").required().build());
        optionsIO.addOption(Option.builder("l").longOpt("label").hasArg().desc("number of labels (required)").required().build());
        optionsIO.addOption(Option.builder("m").longOpt("models").hasArg().desc("model path (default ./models/)").build());
        optionsIO.addOption(Option.builder("o").longOpt("output").hasArg().desc("output path (default ./outputs/)").build());
        optionsIO.addOption(Option.builder("q").longOpt("quiet").desc("quiet mode").build());

        // Platform options
        Options optionsPlatform = CliSVMUtil.getPlatformOptions();

        // Model Parameter
        Options optionsParameter = new Options();
        optionsParameter.addOption(Option.builder("tecc").longOpt("threshold")
                .desc("threshold for ECC (default: " + CliSVMUtil.DEFAULT_THRESHOLD_ECC + ")").build());

        // collect all options
        Options options = new Options();
        for(Option option : optionsIO.getOptions()) options.addOption(option);
        for(Option option : optionsPlatform.getOptions()) options.addOption(option);
        for(Option option : optionsParameter.getOptions()) options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            // parse platform
            CliSVMUtil.parsePlatformOptions(cmd);
            PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

            // parse i/o
            String input = cmd.getOptionValue("test");
            int labelCount = Integer.parseInt(cmd.getOptionValue("l"));
            String modelPath = cmd.hasOption("m") ? cmd.getOptionValue("m") : DEFAULT_MODEL_PATH;
            String outputPath = cmd.hasOption("o") ? cmd.getOptionValue("o") : DEFAULT_OUTPUT_PATH;

            // quiet mode
            boolean quietMode = cmd.hasOption("q");
            if(quietMode) Config.PRINT_LEVEL = 0;

            // parse model parameter
            float thresholdECC = CliSVMUtil.parseFloat("tecc", cmd, CliSVMUtil.DEFAULT_THRESHOLD_ECC);

            // load data
            InputData test = WekaLoader.load(input, labelCount);

            // restore model files
            List<ModelSVM> models = ModelFileUtil.restoreModels(modelPath);
            if(models == null || models.size() == 0) throw new IllegalArgumentException("Failed to restore model files.");

            EccSVMTester eccSVMTester = new EccSVMTester(models.size() / labelCount, thresholdECC);

            // classify
            long startEval = System.currentTimeMillis();
            LabelResults results = eccSVMTester.classifyECC(test, models);
            long endEval = System.currentTimeMillis();

            // evaluate
            Evaluator.evaluate(test, results, outputPath);

            System.out.println("\nClassifying: " + (endEval - startEval) + " ms");
            System.out.println("Classifying (without building kernels): " + (endEval - startEval - SVMBenchmark.getBuildTimeTesting()) + " ms");

            System.out.println("\nClassification of labels finished.");

        } catch (ParseException e) {

            // show error and command line options
            System.err.println("Error: " + e.getMessage());
            CliSVMUtil.formatHelp(optionsIO, optionsPlatform, optionsParameter,
                    CliSVMUtil.ORDER_INPUT_CLASSIFIER, CliSVMUtil.ORDER_CLASSIFIER,
                    "java -jar " + JAR_NAME + " [options] -test test_file -l label_count");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PlatformUtil.requestShutdown();
        }
    }
}
