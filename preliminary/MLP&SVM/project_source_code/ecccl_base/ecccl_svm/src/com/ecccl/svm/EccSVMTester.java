package com.ecccl.svm;

import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.tasks.LinearClassification;
import com.ecccl.svm.tasks.RBFClassification;
import com.ecccl.svm.util.SVMBenchmark;
import model.ChainResults;
import model.InputData;
import model.LabelResults;
import opencl.PlatformUtil;
import util.Config;

import java.util.List;

public class EccSVMTester {
    private int chainCount;
    private float thresholdECC;

    public EccSVMTester(int chainCount, float thresholdECC) {
        this.chainCount = chainCount;
        this.thresholdECC = thresholdECC;
    }

    public LabelResults classifyECC(InputData testData, List<ModelSVM> models) {
        LabelResults ensembleResults = new LabelResults(testData.getNumInstances(), testData.getNumLabels());

        if(Config.PRINT_LEVEL > 0) System.out.print("\nClassifying: ");

        for (int eccNr = 0; eccNr < chainCount; eccNr++) {
            ChainResults chainResults = classifyChain(testData, models, eccNr);

            for (int i = 0; i < testData.getNumLabels(); i++) {
                for (int j = 0; j < testData.getNumInstances(); j++) {
                    ensembleResults.addResult(i, j, chainResults.get(i, j));
                }
            }
        }

        ensembleResults.threshold(chainCount, thresholdECC);

        if(Config.PRINT_LEVEL > 0) System.out.println();

        if(PlatformUtil.PROFILING_ENABLED) {
            System.out.println("Kernel Time RBF: " + SVMBenchmark.runEvalKernel / 1000000 + " ms");
            System.out.println("Kernel Time RBF Score: " + SVMBenchmark.runEvalScore / 1000000 + " ms");
        }

        return ensembleResults;
    }

    private ChainResults classifyChain(InputData testData, List<ModelSVM> models, int eccNr) {
        ChainResults chainResults = new ChainResults(testData.length(), testData.getNumLabels());

        for(int i = 0; i < testData.getNumLabels(); i++) {
            ModelSVM model = models.get(eccNr * testData.getNumLabels() + i);
            chainResults.setLabelOrder(i, model.getLabelIndex());

            // classify instances for current label of model with results of other labels
            float[] results;
            switch(model.getKernel().getKernelVersion()) {
                case LINEAR:
                    LinearClassification classification = new LinearClassification(testData, chainResults, model);
                    results = classification.getOutputs();
                    break;
                default:
                    RBFClassification rbfClassification = new RBFClassification(testData, chainResults, model);
                    results = rbfClassification.getOutputs();
                    rbfClassification.release();
                    break;
            }

            chainResults.putWithOrder(i, results);
            if(Config.PRINT_LEVEL > 0) System.out.print(".");
        }

        return chainResults;
    }
}
