package com.ecccl.svm.runnable;

import com.ecccl.svm.EccSVM;
import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.util.ModelFileUtil;
import com.ecccl.svm.util.SVMBenchmark;
import org.apache.commons.cli.*;
import model.InputData;
import opencl.PlatformUtil;
import com.ecccl.svm.util.CliSVMUtil;
import util.CliUtil;
import util.Config;
import util.WekaLoader;

import java.util.List;

public class SVMTraining {
    private static final String JAR_NAME = "mdSVM-train.jar";

    private static final String DEFAULT_MODEL_PATH = "./models/";

    public static void main(String[] args) {
        PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

        // Input and Output options
        Options optionsIO = new Options();
        optionsIO.addOption(Option.builder("train").hasArg().desc("training file (required)").required().build());
        optionsIO.addOption(Option.builder("l").longOpt("label").hasArg().desc("number of labels (required)").required().build());
        optionsIO.addOption(Option.builder("o").longOpt("output").hasArg().desc("model path (default " + DEFAULT_MODEL_PATH + ")").build());
        optionsIO.addOption(Option.builder("q").longOpt("quiet").desc("quiet mode").build());

        // Platform options
        Options optionsPlatform = CliSVMUtil.getPlatformOptions();

        // SVM parameter options
        Options optionsParameter = CliSVMUtil.getSVMOptions();

        // collect all options
        Options options = new Options();
        for(Option option : optionsIO.getOptions()) options.addOption(option);
        for(Option option : optionsPlatform.getOptions()) options.addOption(option);
        for(Option option : optionsParameter.getOptions()) options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            // parse platform
            CliSVMUtil.parsePlatformOptions(cmd);
            PlatformUtil.requestInit(Config.PLATFORM_ID, Config.DEVICE_ID, Config.DEVICE_TYPE);

            // parse i/o
            String input = cmd.getOptionValue("train");
            int labelCount = Integer.parseInt(cmd.getOptionValue("l"));
            String modelPath = cmd.hasOption("o") ? cmd.getOptionValue("o") : DEFAULT_MODEL_PATH;

            // quiet mode
            if(cmd.hasOption("q")) Config.PRINT_LEVEL = 0;

            // load data
            InputData train = WekaLoader.load(input, labelCount);

            // create classifier from options
            int numCC = CliSVMUtil.parseInt("n", cmd, CliSVMUtil.DEFAULT_NUM_CC);
            EccSVM eccSVM = CliSVMUtil.getSVMFromOptions(cmd);

            // train
            long startTrain = System.currentTimeMillis();
            List<ModelSVM> models = eccSVM.train(train);
            long endTrain = System.currentTimeMillis();

            // store model files
            ModelFileUtil.storeModels(models, numCC, modelPath);

            // output runtime
            System.out.println("\nTraining: " + (endTrain - startTrain) + " ms");
            System.out.println("Training (without building kernels): " + (endTrain - startTrain - SVMBenchmark.getRunTimeTraining()) + " ms");

            System.out.println("\nModels are stored in: " + modelPath);
            System.out.println("Training of labels finished.");

        } catch (ParseException e) {

            // show error and command line options
            System.err.println("Error: " + e.getMessage());
            CliUtil.formatHelp(optionsIO, optionsPlatform, optionsParameter,
                    CliSVMUtil.ORDER_INPUT_TRAINING, CliSVMUtil.ORDER_TRAINING,
                    "java -jar " + JAR_NAME + " [options] -train training_file -l label_count");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PlatformUtil.requestShutdown();
        }
    }
}
