package com.ecccl.svm.model.kernel;

/**
 * This class represents the radial basis function (RBF) kernel: exp(-gamma*|u-v|^2).
 *
 * @author
 */
public class RBFKernel extends LocalKernel {
    private float gamma;

    public RBFKernel() {
        super(KernelVersion.RBF);
        this.gamma = 0.01f;
    }

    public RBFKernel(float gamma) {
        super(KernelVersion.RBF);
        this.gamma = gamma;
    }

    public float getGamma(){
        return gamma;
    }

    @Override
    public float computeKernel(float[] attr1, float[] attr2, float[] label1, float[] label2,
                               int initNumAttr, int numAttrAsLabels) {
        float result = 0;
        for (int i = 0; i < initNumAttr; ++i) {
            result += (attr1[i] - attr2[i]) * (attr1[i] - attr2[i]);
        }
        for (int i = 0; i < numAttrAsLabels; ++i) {
            result += (label1[i] - label2[i]) * (label1[i] - label2[i]);
        }

        result = (float) Math.exp(-gamma * result);
        return result;
    }
}
