package com.ecccl.svm.executor.training;

import com.ecccl.svm.util.SVMBenchmark;
import org.lwjgl.BufferUtils;
import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params.ParamsL1;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * This class manages the OpenCL kernel execution of updating
 * the error values.
 *
 * @author
 */
public class ErrorUpdateExecutor {
    private static ErrorUpdateExecutor instance = null;
    private static Kernel kernel;
    private static ParamsL1 params;

    private IntBuffer cacheUpBuffer;
    private IntBuffer cacheLowBuffer;
    private FloatBuffer coefLowBuffer;
    private FloatBuffer coefUpBuffer;

    private IntBuffer iUpBuffer;
    private IntBuffer iLowBuffer;
    private ByteBuffer listUpBuffer;
    private ByteBuffer listLowBuffer;

    private interface ARGS {
        int SIZE = 11;
        int CACHE_UP = 0, CACHE_LOW = 1, COEF_UP = 2, COEF_LOW = 3;
        int I_UP = 4, I_LOW = 5, LIST_UP = 6, LIST_LOW = 7;
        int CACHE = 8, LIST = 9, ERROR = 10;
    }

    public static ErrorUpdateExecutor getInstance(int numInstances, long cache, long error, long list) {
        if (instance == null) {
            ParamsL1 params = new ParamsL1(ParamsL1.TUNING_FILE_ERROR_UPDATE);
            instance = new ErrorUpdateExecutor(params, numInstances, cache, error, list, true);
        }
        instance.updateArgs(cache, error, list);
        return instance;
    }

    public static ErrorUpdateExecutor getInstance(ParamsL1 params, int numInstances, long cache, long error, long list) {
        if (instance == null) {
            instance = new ErrorUpdateExecutor(params, numInstances, cache, error, list, false);
        }
        instance.updateArgs(cache, error, list);
        return instance;
    }

    private ErrorUpdateExecutor(ParamsL1 _params, int numInstances, long cache, long error, long list, boolean useCache) {
        params = _params;

        long start = System.nanoTime();
        buildKernel(numInstances, useCache);
        long end = System.nanoTime();
        SVMBenchmark.buildError += end - start;

        createBuffer();
        initArgs(cache, error, list);
    }

    private void buildKernel(int numInstances, boolean useBuildCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_error_update_kernel",
                params.getOption(numInstances), useBuildCache);

        kernel = new Kernel(program, "compute_error_update_1");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0);
        kernel.setWorkDim(params.WORK_DIM);
    }

    private void createBuffer() {
        // cache rows to be computed
        cacheUpBuffer = BufferUtils.createIntBuffer(1);
        cacheLowBuffer = BufferUtils.createIntBuffer(1);

        // coefficients of error update
        coefUpBuffer = BufferUtils.createFloatBuffer(1);
        coefLowBuffer = BufferUtils.createFloatBuffer(1);

        iUpBuffer = BufferUtils.createIntBuffer(1);
        iLowBuffer = BufferUtils.createIntBuffer(1);
        listUpBuffer = BufferUtils.createByteBuffer(1);
        listLowBuffer = BufferUtils.createByteBuffer(1);
    }

    private void updateArgs(long cache, long error, long list) {
        kernel.setArg(ARGS.CACHE, cache, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.LIST, list, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.ERROR, error, Kernel.TypeFlag.SHARED);
        kernel.setArgs();
    }

    private void initArgs(long cache, long error, long list){
        kernel.initArgs(ARGS.SIZE);
        kernel.setInputArg(ARGS.CACHE_UP, cacheUpBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(ARGS.CACHE_LOW, cacheLowBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(ARGS.COEF_UP, coefUpBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(ARGS.COEF_LOW, coefLowBuffer, Kernel.TypeFlag.WRITE_SCALAR);

        kernel.setInputArg(ARGS.I_UP, iUpBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(ARGS.I_LOW, iLowBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(ARGS.LIST_UP, listUpBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(ARGS.LIST_LOW, listLowBuffer, Kernel.TypeFlag.WRITE_SCALAR);

        kernel.setArg(ARGS.CACHE, cache, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.LIST, list, Kernel.TypeFlag.SHARED);
        kernel.setArg(ARGS.ERROR, error, Kernel.TypeFlag.SHARED);
        kernel.setArgs();
    }

    public void execute(int cache_i_up, int cache_i_low, float coef_up, float coef_low, int iUp, int iLow, byte listUp, byte listLow){
        // set index of rows which are needed by computation
        cacheUpBuffer.put(cache_i_up).flip();
        cacheLowBuffer.put(cache_i_low).flip();

        // set coefficients of computation
        coefUpBuffer.put(coef_up).flip();
        coefLowBuffer.put(coef_low).flip();

        // values for updating list indices
        iUpBuffer.put(iUp).flip();
        iLowBuffer.put(iLow).flip();
        listUpBuffer.put(listUp).flip();
        listLowBuffer.put(listLow).flip();

        kernel.setScalarArgs();

        // execute program
        if(PlatformUtil.PROFILING_ENABLED) {
            SVMBenchmark.runError += kernel.executeKernelProfile();
        } else {
            kernel.executeKernel();
        }
    }

    public long profile(int cache_i_up, int cache_i_low, float coef_up, float coef_low,  int iUp, int iLow, byte listUp, byte listLow){
        cacheUpBuffer.put(cache_i_up).flip();
        cacheLowBuffer.put(cache_i_low).flip();
        coefUpBuffer.put(coef_up).flip();
        coefLowBuffer.put(coef_low).flip();

        // set coefficients of computation
        coefUpBuffer.put(coef_up).flip();
        coefLowBuffer.put(coef_low).flip();

        //System.out.println(iUp);
        iUpBuffer.put(iUp).flip();
        iLowBuffer.put(iLow).flip();
        listUpBuffer.put(listUp).flip();
        listLowBuffer.put(listLow).flip();

        kernel.setScalarArgs();
        return kernel.executeKernelProfile();
    }

    public static void releaseKernel() {
        if(kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
    }
}
