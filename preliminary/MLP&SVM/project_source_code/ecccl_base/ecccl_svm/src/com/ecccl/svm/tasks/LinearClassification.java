package com.ecccl.svm.tasks;

import model.ChainResults;
import model.InputData;
import com.ecccl.svm.model.ModelSVM;

public class LinearClassification {
    private double[] weights;
    private float b;
    private ChainResults chainResults;

    private InputData input;
    private float[] supportVectors;
    private int numAttributesSv;

    public LinearClassification(InputData input, ChainResults chainResults, ModelSVM model) {
        this.chainResults = chainResults;
        this.input = input;
        this.supportVectors = model.getSupportVectors();
        this.numAttributesSv = model.getNumAttributes();
        this.b = model.getB();

        computeWeights(model, supportVectors, model.getAlphas(), model.getLabel());
    }

    public float[] getOutputs() {
        float[] results = new float[input.getNumInstances()];

        for (int instanceId = 0; instanceId < input.getNumInstances(); ++instanceId) {
            float result = scoring(instanceId);
            results[instanceId] = result >= 0 ? 1.0f : 0f;
        }

        return results;
    }

    private float scoring(int instanceIndex) {
        int numAttributesSV = numAttributesSv;
        int numAttributesInput = input.getNumAttributes();
        float[] instance = input.getInstance(instanceIndex);

        float result = 0;
        for (int i = 0; i < numAttributesSV; ++i) {
            if(i > numAttributesInput - 1){
                // todo: correct?
                result += weights[i] * chainResults.getWithOrder(i - numAttributesInput, instanceIndex);
            } else {
                result += weights[i] * instance[i];
            }
        }
        return result - b;
    }

    private void computeWeights(ModelSVM model, float[] supportVectors, float[] alphas, float[] label) {
        weights = new double[model.getNumAttributes()];
        for (int attr = 0; attr < model.getNumAttributes(); ++attr) {
            for (int i = 0; i < model.getNumSupportVectors(); ++i) {
                double x = supportVectors[i * model.getNumAttributes() + attr];
                double y = label[i];
                double a = alphas[i];
                weights[attr] += (a * y * x);
            }
        }
    }
}
