package com.ecccl.svm.tasks;

import com.ecccl.svm.executor.training.*;
import model.ChainData;
import com.ecccl.svm.model.kernel.KernelCache;
import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.steps.WorkingSetSelection;
import com.ecccl.svm.model.kernel.LocalKernel;
import com.ecccl.svm.steps.ErrorUpdate;
import com.ecccl.svm.steps.KernelEvaluator;
import com.ecccl.svm.tasks.util.BiasResult;
import util.Config;

import static org.lwjgl.opencl.CLUtil.checkCLError;

public class Training {
    private int iterations = 0;

    private ChainData input;
    private float[] alphas;
    private double eps;
    private double tol;
    private float C;
    private LocalKernel kernel;

    private float b_up, b_low;
    private int i_up, i_low;

    private KernelCache localKernelCache;

    // steps
    private KernelEvaluator kernelEvaluator;
    private ErrorUpdate errorUpdate;
    private WorkingSetSelection workingSetSelection;

    /**
     * @param numInstances number of instances
     * @param C cost of SVM
     * @param eps tolerance when choosing index on the boundaries (0, C)
     * @param tol tolerance on termination criteria
     * @param sizeCacheMB maximum size of cache on GPU device
     * @param kernel kernel used in SVM
     */
    public Training(int numInstances, float C, double eps, double tol, int sizeCacheMB, LocalKernel kernel) {
        this.C = C;
        this.kernel = kernel;
        this.eps = eps;
        this.tol = tol;

        // initialize steps
        kernelEvaluator = new KernelEvaluator(kernel, numInstances, sizeCacheMB);
        errorUpdate = new ErrorUpdate(numInstances, C, eps, kernelEvaluator.getCache());
        workingSetSelection = new WorkingSetSelection(numInstances, errorUpdate.getList(), errorUpdate.getError());
    }

    public void updateData(ChainData data) {
        this.input = data;
        this.iterations = 0;
        this.localKernelCache = new KernelCache(input, kernel, Config.SVM_SIZE_MB_LOCAL_CACHE);
        this.alphas = new float[input.length()];

        // update data in steps
        kernelEvaluator.updateData(data);
        errorUpdate.updateData(data);

        initializeTraining();
    }

    public ModelSVM train() {

        // Special case: all labels same value
        if(b_low == b_up && b_low == -1){
            b_low = Float.MAX_VALUE;
            b_up = Float.MAX_VALUE;
        } else if(b_low == b_up && b_low == 1) {
            b_low = -Float.MAX_VALUE;
            b_up = -Float.MAX_VALUE;
        } else {
            do {
                takeStep();
                iterations++;
                if(iterations % 10000 == 0) System.out.print(".");
            } while (b_up < b_low - 2 * tol);
        }

        return ModelSVM.createModel(alphas, input, (b_low + b_up) / 2, kernel);
    }

    public void release() {
        kernelEvaluator.release();
    }

    public void finish() {
        kernelEvaluator.finish();
        errorUpdate.finish();
    }

    private void takeStep() {
        float alpha_up = alphas[i_up];
        float alpha_low = alphas[i_low];
        float y_up = input.getCurrentLabel(i_up);
        float y_low = input.getCurrentLabel(i_low);
        float s = y_up * y_low;

        float k11 = localKernelCache.get(i_up, i_up);
        float k12 = localKernelCache.get(i_up, i_low);
        float k22 = localKernelCache.get(i_low, i_low);
        float eta = 2 * k12 - k11 - k22;

        float H, L;
        if(y_low != y_up) {
            H = Math.min(C, alpha_low - alpha_up + C);
            L = Math.max(0, alpha_low - alpha_up);
        } else {
            L = Math.max(0, alpha_low + alpha_up - C);
            H = Math.min(C, alpha_up + alpha_low);
        }

        // prevent dividing by 0
        if(eta == 0) eta -= eps;

        float alow = alpha_low - y_low * (b_up - b_low) / eta;
        if(alow < L) alow = L;
        if(alow > H) alow = H;

        float aup = alpha_up + s * (alpha_low - alow);
        if (aup < tol) aup = 0;
        else if (aup > (C - tol)) aup = C;

        // set new alphas to array
        alphas[i_up] = aup;
        alphas[i_low] = alow;

        // execute training step on device (updating list and error and computing new bias)
        BiasResult result = executeStep(i_up, i_low, alpha_up, alpha_low, aup, alow, y_up, y_low);

        b_low = result.b_low();
        i_low = result.i_low();
        b_up = result.b_up();
        i_up = result.i_up();
    }

    private BiasResult executeStep(int iUp, int iLow, float alphaUp, float alphaLow, float alphaUpNew, float alphaLowNew, float yUp, float yLow) {

        // compute kernel rows if they are not already in the kernel cache
        int cacheUp = kernelEvaluator.checkKernelRow(iUp, iLow);
        int cacheLow = kernelEvaluator.checkKernelRow(iLow, iUp);

        // update error values for every instance and update indices list
        errorUpdate.updateError(cacheUp, cacheLow, iUp, iLow, alphaUp, alphaLow, alphaUpNew, alphaLowNew, yUp, yLow);

        // calculate new bias values and corresponding id's of the next update step
        return workingSetSelection.run();
    }

    private void initializeTraining() {
        boolean found = false;
        for (int i = 0; i < input.length(); i++) {
            if(input.getCurrentLabel(i) == 1) {
                b_low = 1;
                i_low = i;
                found = true;
                break;
            }
        }
        if(!found) {
            b_low = -1;
            i_low = 0;
        }
        found = false;
        for (int i = 0; i < input.length(); i++) {
            if(input.getCurrentLabel(i) == -1) {
                b_up = -1;
                i_up = i;
                found = true;
                break;
            }
        }
        if(!found) {
            b_up = 1;
            if(i_low == 0) i_up = 1;
            else i_up = 0;
        }
    }

    /**
     * Releases the OpenCL programs of the executors.
     */
    public static void releaseKernel() {
        ErrorUpdateExecutor.releaseKernel();
        RBFKernelExecutor.releaseKernel();
        LinearKernelExecutor.releaseKernel();
        UpdateBiasExecutor.releaseKernel();
    }
}