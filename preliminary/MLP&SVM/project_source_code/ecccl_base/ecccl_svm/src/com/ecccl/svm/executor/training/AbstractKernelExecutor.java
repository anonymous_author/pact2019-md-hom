package com.ecccl.svm.executor.training;

/**
 * This abstract class represents a executor of a row kernel evaluation
 * in the kernel matrix.
 *
 * @author
 */
public abstract class AbstractKernelExecutor {
    public abstract void execute(int i, int cacheIndex);
}
