package com.ecccl.svm.util;

public class SVMBenchmark {
    // Training
    public static long buildError = 0;
    public static long buildBias = 0;
    public static long buildKernel = 0;
    public static long buildKernelMap = 0;

    public static long runError = 0;
    public static long runBias = 0;
    public static long readBias = 0;
    public static long runKernel = 0;
    public static long runKernelMap = 0;

    // Testing
    public static long buildEvalKernel = 0;
    public static long buildEvalScore = 0;

    public static long runEvalKernel = 0;
    public static long runEvalScore = 0;

    public static void resetTraining() {
        buildError = 0;
        buildBias = 0;
        buildKernel = 0;
        buildKernelMap = 0;

        runError = 0;
        runBias = 0;
        readBias = 0;
        runKernel = 0;
        runKernelMap = 0;
    }

    public static void resetTesting() {
        buildEvalKernel = 0;
        buildEvalScore = 0;

        runEvalKernel = 0;
        runEvalScore = 0;
    }

    public static void reset() {
        resetTraining();
        resetTesting();
    }

    public static long getBuildTimeTraining() {
        return (buildError + buildBias + buildKernel + buildKernelMap) / 1000000;
    }

    public static long getRunTimeTraining() {
        return (runError + runBias + readBias + runKernel + runKernelMap) / 1000000;
    }

    public static long getBuildTimeTesting() {
        return (buildEvalKernel + buildEvalScore) / 1000000;
    }

    public static long getRunTimeTesting() {
        return (runEvalKernel + runEvalScore) / 1000000;
    }
}
