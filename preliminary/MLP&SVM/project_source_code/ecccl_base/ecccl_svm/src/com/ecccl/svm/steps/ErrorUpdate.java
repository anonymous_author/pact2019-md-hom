package com.ecccl.svm.steps;

import model.ChainData;
import com.ecccl.svm.executor.training.ErrorUpdateExecutor;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opencl.CL10;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public class ErrorUpdate {
    private ErrorUpdateExecutor errorUpdateExecutor;

    private long list;
    private long error;

    // tolerance when choosing index on the boundaries (0, C)
    private double eps;

    private float C;

    public ErrorUpdate(int numInstances, float C, double eps, long cache) {
        this.eps = eps;
        this.C = C;

        // create buffer for error and list
        error = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances * 4);
        list = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances);

        // initialize ocl executor
        errorUpdateExecutor = ErrorUpdateExecutor.getInstance(numInstances, cache, error, list);
    }

    public void updateData(ChainData data) {
        // initialize error and list indices values + load to device
        loadErrorAndListToDevice(data);
    }

    public long getError() {
        return error;
    }

    public long getList() {
        return list;
    }

    /**
     * Updates the error values for every instance in the data set with
     * the changes made by the update of the la grange multiplier.
     * Additionally updates the indices in the list for iUp and iLow.
     *
     * @param cacheUp index of the updated alpha up
     * @param cacheLow index of the updated alpha low
     */
    public void updateError(int cacheUp, int cacheLow, int iUp, int iLow, float alphaUp, float alphaLow,
                            float alphaUpNew, float alphaLowNew, float yUp, float yLow) {
        byte listUp = getIndex(alphaUpNew, yUp);
        byte listLow = getIndex(alphaLowNew, yLow);

        float coefUp = yUp * (alphaUpNew - alphaUp);
        float coefLow = yLow * (alphaLowNew - alphaLow);
        errorUpdateExecutor.execute(cacheUp, cacheLow, coefUp, coefLow, iUp, iLow, listUp, listLow);
    }

    private byte getIndex(float alpha, float y){
        if(eps < alpha && alpha < (C - eps)) return 0;
        if(y == 1 && alpha <= eps) return 1;
        if(y == -1 && alpha >= (C - eps)) return 1;
        if(y == 1 && alpha >= (C - eps)) return 2;
        if(y == -1 && alpha <= eps) return 2;
        return 0;
    }

    public void finish() {
        CL10.clReleaseMemObject(list);
        CL10.clReleaseMemObject(error);
    }

    private void loadErrorAndListToDevice(ChainData data){
        FloatBuffer errorBuffer = BufferUtils.createFloatBuffer(data.length());
        ByteBuffer listBuffer = BufferUtils.createByteBuffer(data.length());

        for(int i = 0; i < data.length(); i++) {
            errorBuffer.put(-data.getCurrentLabel(i));

            if (data.getCurrentLabel(i) == -1) {
                listBuffer.put((byte) 2);
            } else {
                listBuffer.put((byte) 1);
            }
        }

        errorBuffer.flip();
        listBuffer.flip();

        OCLUtil.write(errorBuffer, error, CL10.CL_TRUE);
        OCLUtil.write(listBuffer, list, CL10.CL_TRUE);
    }
}
