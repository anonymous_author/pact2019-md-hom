package com.ecccl.svm.executor.training;

import com.ecccl.svm.util.SVMBenchmark;
import org.lwjgl.BufferUtils;
import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params.ParamsL1R1;

import java.nio.IntBuffer;

/**
 * This class manages the OpenCL kernel execution of computing
 * a row of kernel evaluations for the linear kernel.
 *
 * @author
 */
public class LinearKernelExecutor extends AbstractKernelExecutor {
    private static LinearKernelExecutor instance = null;

    // kernels
    private static Kernel kernel;
    private static Kernel kernel2;
    private static ParamsL1R1 params;

    // buffer for kernels
    private IntBuffer iBuffer;
    private IntBuffer cacheIndexBuffer;

    private IntBuffer numInstancesBuffer;
    private IntBuffer numAttributesBuffer;
    private IntBuffer K2_R1_SIZE_Buffer;

    public interface K1_ARGS {
        int SIZE = 6;
        int INDEX = 0, CACHE_INDEX = 1, INPUT = 2, RESULT = 3;
        int L1 = 4, R1 = 5;
    }

    public interface K2_ARGS {
        int SIZE = 5;
        int K1_RES = 0, CACHE_INDEX = 1, RESULT = 2;
        int L1 = 3, R1 = 4;
    }

    public static LinearKernelExecutor getInstance(int numInstances, int numAttributes, long input, long cache) {
        if (instance == null) {
            params = new ParamsL1R1(ParamsL1R1.TUNING_FILE_LINEAR);
            instance = new LinearKernelExecutor(numInstances, numAttributes, input, cache, true);
        }
        instance.updateArgs(numInstances, numAttributes, input, cache);
        return instance;
    }

    public static LinearKernelExecutor getInstance(ParamsL1R1 _params, int numInstances, int numAttributes, long input, long cache) {
        if (instance == null) {
            params = _params;
            instance = new LinearKernelExecutor(numInstances, numAttributes, input, cache, false);
        }
        instance.updateArgs(numInstances, numAttributes, input, cache);
        return instance;
    }

    private LinearKernelExecutor(int numInstances, int numAttributes, long input, long cache, boolean useCache){
        long start = System.nanoTime();
        buildKernel(useCache);
        long end = System.nanoTime();
        SVMBenchmark.buildKernel += end - start;

        createBuffer();
        initArgs(input, cache, numInstances, numAttributes);
    }

    private void buildKernel(boolean useCache) {
        String options = params.getOptions();
        if (params.NUM_WG_R_1 == 1) options += " -D SINGLE_KERNEL";
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_linear_kernel", options, useCache);

        kernel = new Kernel(program, "svm_compute_linear_1");
        kernel.setLocalSize(params.LOCAL_SIZE_DIM_0, params.LOCAL_SIZE_DIM_1);
        kernel.setGlobalSize(params.GLOBAL_SIZE_DIM_0, params.GLOBAL_SIZE_DIM_1);
        kernel.setWorkDim(params.WORK_DIM);

        if (params.NUM_WG_R_1 > 1) buildKernel2(useCache);
    }

    private void buildKernel2(boolean useCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_linear_kernel_2",
                params.getOptions(), useCache);

        kernel2 = new Kernel(program, "svm_compute_linear_2");
        kernel2.setLocalSize(params.K2_LOCAL_SIZE_DIM_0, params.K2_LOCAL_SIZE_DIM_1);
        kernel2.setGlobalSize(params.K2_GLOBAL_SIZE_DIM_0, params.K2_GLOBAL_SIZE_DIM_1);
        kernel2.setWorkDim(params.WORK_DIM);
    }

    private void createBuffer() {
        // create buffer for index of row to be computed && compute line on device
        iBuffer = BufferUtils.createIntBuffer(1);
        cacheIndexBuffer = BufferUtils.createIntBuffer(1);

        numInstancesBuffer = BufferUtils.createIntBuffer(1);
        numAttributesBuffer = BufferUtils.createIntBuffer(1);
        K2_R1_SIZE_Buffer = BufferUtils.createIntBuffer(1);
    }

    private void initArgs(long input, long cache, int numInstances, int numAttributes){
        // long resGSize = mainParams.getResGSizeKernelOne(numInstances, numAttributes, 4);

        numInstancesBuffer.put(numInstances).flip();
        numAttributesBuffer.put(numAttributes).flip();
        K2_R1_SIZE_Buffer.put(params.K2_G_CB_SIZE_R_1(numAttributes)).flip();

        // set argument for first kernel
        kernel.initArgs(K1_ARGS.SIZE);
        kernel.setInputArg(K1_ARGS.INDEX, iBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(K1_ARGS.CACHE_INDEX, cacheIndexBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        //kernel.setInputOutputArg(K1_ARGS.RES_G, 1, Kernel.TypeFlag.CACHE);
        kernel.setArg(K1_ARGS.INPUT, input, Kernel.TypeFlag.SHARED);
        kernel.setInputArg(K1_ARGS.L1, numInstancesBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(K1_ARGS.R1, numAttributesBuffer, Kernel.TypeFlag.WRITE_SCALAR);

        // set arguments for second kernel
        if(params.NUM_WG_R_1 > 1) {
            long k1_result = kernel.setIOArg(K1_ARGS.RESULT, numInstances * params.NUM_WG_R_1 * 4, Kernel.TypeFlag.SHARED);

            kernel2.initArgs(K2_ARGS.SIZE);
            kernel2.setArg(K2_ARGS.K1_RES, k1_result, Kernel.TypeFlag.SHARED);
            kernel2.setInputArg(K2_ARGS.CACHE_INDEX, cacheIndexBuffer, Kernel.TypeFlag.WRITE_SCALAR);
            //kernel2.setInputOutputArg(K2_ARGS.RES_G, 1, Kernel.TypeFlag.CACHE);
            kernel2.setArg(K2_ARGS.RESULT, cache, Kernel.TypeFlag.SHARED);
            kernel2.setInputArg(K2_ARGS.L1, numInstancesBuffer, Kernel.TypeFlag.WRITE_SCALAR);
            kernel2.setInputArg(K2_ARGS.R1, K2_R1_SIZE_Buffer, Kernel.TypeFlag.WRITE_SCALAR);

            kernel.setArgs();
            kernel2.setArgs();
        } else {
            kernel.setArg(K1_ARGS.RESULT, cache, Kernel.TypeFlag.SHARED);
            kernel.setArgs();
        }
    }

    private void updateArgs(int numInstances, int numAttributes, long input, long cache) {
        numInstancesBuffer.put(numInstances).flip();
        numAttributesBuffer.put(numAttributes).flip();
        K2_R1_SIZE_Buffer.put(params.K2_G_CB_SIZE_R_1(numAttributes)).flip();

        kernel.setArg(RBFKernelExecutor.K1_ARGS.INPUT, input, Kernel.TypeFlag.SHARED);

        if(params.NUM_WG_R_1 > 1) {
            kernel2.setArg(RBFKernelExecutor.K2_ARGS.RESULT, cache, Kernel.TypeFlag.SHARED);
            kernel.setArgs();
            kernel2.setArgs();
        } else {
            kernel.setArg(RBFKernelExecutor.K1_ARGS.RESULT, cache, Kernel.TypeFlag.SHARED);
            kernel.setArgs();
        }
    }

    public void execute(int i, int cacheIndex){
        // set number of row to be computed
        iBuffer.put(i).flip();
        cacheIndexBuffer.put(cacheIndex).flip();

        // execute kernel
        if(PlatformUtil.PROFILING_ENABLED){
            kernel.setScalarArgs();
            SVMBenchmark.runKernel += kernel.executeKernelProfile();

            if(params.NUM_WG_R_1 > 1) {
                kernel2.setScalarArgs();
                SVMBenchmark.runKernel  += kernel2.executeKernelProfile();
            }
        } else {
            kernel.setScalarArgs();
            kernel.executeKernel();

            if(params.NUM_WG_R_1 > 1) {
                kernel2.setScalarArgs();
                kernel2.executeKernel();
            }
        }
    }

    public long profile(int i, int cacheIndex){
        iBuffer.put(i).flip();
        cacheIndexBuffer.put(cacheIndex).flip();

        long runtime = 0;
        kernel.setScalarArgs();
        runtime += kernel.executeKernelProfile();

        if(params.NUM_WG_R_1 > 1) {
            kernel2.setScalarArgs();
            runtime += kernel2.executeKernelProfile();
        }

        return runtime;
    }

    public static void releaseKernel() {
        if (kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
        if (kernel2 != null) {
            kernel2.releaseArgs();
            kernel2.releaseKernel();
        }
    }
}
