//#pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void svm_classify_rbf(
                            // constant values
                            __constant float* p_gamma,
                            __constant float* input_data,
                            __constant int* p_num_labels,
                            __constant int* p_num_attributes,
                            __constant int* p_num_instances,

                            // changing values
                            __constant int* p_i,
                            __constant int* p_j,

                            // output
                            __global float* cache_data
							)
{
    const int n = get_global_id(0);
    int num_instances = *p_num_instances;
    if(n >= num_instances) return;

    int i = *p_i;
    int j = *p_j;
    float gamma = *p_gamma;
    int num_attributes = *p_num_attributes;
    int num_labels = *p_num_labels;
    int num_values = num_attributes + num_labels;

    if(i >= 0){
        cache_data[n] = 0;
        for (int k = 0; k < num_attributes; ++k) {
            cache_data[n] = (input_data[i * num_values + k] - input_data[n * num_values + k]) *
                (input_data[i * num_values + k] - input_data[n * num_values + k]) + cache_data[n];
        }
        cache_data[n] = exp(-gamma * cache_data[n]);
    }

    if(j >= 0){
        cache_data[num_instances + n] = 0;
        for (int k = 0; k < num_attributes; ++k) {
            cache_data[num_instances + n] = (input_data[j * num_values + k] - input_data[n * num_values + k]) *
                (input_data[j * num_values + k] - input_data[n * num_values + k]) + cache_data[num_instances + n];
        }
        cache_data[num_instances + n] = exp(-gamma * cache_data[num_instances + n]);
    }
}

__kernel void svm_classify_linear(
                            // constant values
                            __constant float* p_gamma,
                            __constant float* input_data,
                            __constant int* p_num_labels,
                            __constant int* p_num_attributes,
                            __constant int* p_num_instances,

                            // changing values
                            __constant int* p_i,
                            __constant int* p_j,

                            // output
                            __global float* cache_data
							)
{
    const int n = get_global_id(0);
    int num_instances = *p_num_instances;
    if(n >= num_instances) return;

    int i = *p_i;
    int j = *p_j;
    int num_attributes = *p_num_attributes;
    int num_labels = *p_num_labels;
    int num_values = num_attributes + num_labels;

    if(i >= 0){
        cache_data[n] = 0;
        for (int k = 0; k < num_attributes; ++k) {
            cache_data[n] = cache_data[n] + input_data[i * num_values + k] * input_data[n * num_values + k];
        }
    }

    if(j >= 0){
        cache_data[num_instances + n] = 0;
        for (int k = 0; k < num_attributes; ++k) {
            cache_data[num_instances + n] = cache_data[num_instances + n] + input_data[j * num_values + k] * input_data[n * num_values + k];
        }
    }
}