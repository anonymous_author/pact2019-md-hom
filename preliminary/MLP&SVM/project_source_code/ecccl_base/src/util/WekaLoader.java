package util;

import model.InputData;
import mulan.data.InvalidDataFormatException;
import mulan.data.MultiLabelInstances;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

public class WekaLoader {
    public static InputData load(String arff, int numLabels) throws Exception {
        String arffFile = arff;
        String ext = getFileExtension(arff);
        if(!ext.equals("arff")) arffFile += ".arff";

        if(numLabels < 1) {
            System.err.println("Number of labels must be at least 1");
            throw new IllegalArgumentException("Number of labels must be at least 1");
        }

        if(numLabels == 1) {
            Instances data;
            try {
                ConverterUtils.DataSource source = new ConverterUtils.DataSource(arffFile);
                data = source.getDataSet();
            } catch (Exception e) {
                System.err.println("Error reading file: " + arff);
                throw e;
            }
            InputData input = new InputData(data, data.numAttributes() - 1, 1);
            if(Config.PRINT_LEVEL > 0) System.out.println("Loading data: " + arff);
            return input;
        } else {
            MultiLabelInstances mli;
            try {
                mli = new MultiLabelInstances(arffFile, numLabels);
                if(Config.PRINT_LEVEL > 0) System.out.println("Loading data: " + arff);
            } catch (InvalidDataFormatException e) {
                System.err.println("Error reading file: " + arff);
                throw e;
            }
            return new InputData(mli.getDataSet(), mli.getFeatureAttributes().size(), mli.getNumLabels());
        }
    }

    private static InputData load(String arff, String xml) throws InvalidDataFormatException {
        if(Config.PRINT_LEVEL > 0) System.out.println("Loading data: " + arff);
        String arffFile = arff + ".arff";
        String xmlFile = xml + ".xml";

        MultiLabelInstances mli;
        try {
            long start = System.currentTimeMillis();
            mli = new MultiLabelInstances(arffFile, xmlFile);
            long end = System.currentTimeMillis();
            if(Config.PRINT_LEVEL > 0) System.out.println("Loading data: " + (end-start) + " ms");
        } catch (InvalidDataFormatException e) {
            System.err.println("Error reading file: " + arff);
            throw e;
        }
        return new InputData(mli.getDataSet(), mli.getFeatureAttributes().size(), mli.getNumLabels());
    }

    private static String getFileExtension(String file) {
        int i = file.lastIndexOf('.');
        if (i >= 0) {
            return file.substring(i+1);
        } else {
            return "";
        }
    }
}
