package util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * JSONUtil is used to read JSON Data from a file or string.
 *
 * @author
 */
public class JSONUtil {
    public static JSONObject readArgs(String args) throws ParseException {
        JSONParser parser = new JSONParser();
        return (JSONObject) parser.parse(args);
    }

    public static JSONObject readFileArgs(String file) {
        String filePath = new File("").getAbsolutePath();

        JSONParser parser = new JSONParser();
        JSONObject tps = null;
        try {
            tps = (JSONObject) parser.parse(new FileReader(filePath + file));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return tps;
    }
}
