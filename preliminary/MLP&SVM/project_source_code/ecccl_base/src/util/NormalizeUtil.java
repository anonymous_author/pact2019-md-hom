package util;

import model.InputData;

import java.util.Arrays;

public class NormalizeUtil {
    private final static float EPSILON = Float.MIN_VALUE;

    /**
     * Types of normalization methods:
     *      Standard Score (Z-Score): (X - mean) / standard_deviation
     *      Feature Scaling in range [0, 1]: X' = (X - X_min) / (X_max - X_min)
     *      Feature Scaling in range [-1, 1]: X' = -1 + (X - X_min) * (1 - (-1)) / (X_max - X_min)
     */
    public enum NormType{
        STANDARD_SCORE(0),
        STANDARD_SCORE_DOUBLE(1),
        FEATURE_SCALING_0_1(2),
        FEATURE_SCALING_MINUS_1_1(3);
        
        private final int flag;

        NormType(int flag) {
            this.flag = flag;
        }

        public int get() {
            return this.flag;
        }
    }

    public static void normalizeInput(InputData train, InputData test, NormType type) {
        if(type == NormType.STANDARD_SCORE){
            normalizeStandard(train, test, 1);
        } else if(type == NormType.STANDARD_SCORE_DOUBLE){
            normalizeStandard(train, test, 2);
        } else if(type == NormType.FEATURE_SCALING_0_1){
            normalizeFeature(train, test, 0, 1);
        } else if(type == NormType.FEATURE_SCALING_MINUS_1_1){
            normalizeFeature(train, test, -1, 1);
        }
    }

    public static void normalizeInput(InputData input, NormType type) {
        if(type == NormType.STANDARD_SCORE){
            normalizeStandard(input, 1);
        } else if(type == NormType.STANDARD_SCORE_DOUBLE){
            normalizeStandard(input, 2);
        } else if(type == NormType.FEATURE_SCALING_0_1){
            normalizeFeature(input, 0, 1);
        } else if(type == NormType.FEATURE_SCALING_MINUS_1_1){
            normalizeFeature(input,-1, 1);
        }
    }

    private static void normalizeFeature(InputData train, InputData test, int a, int b) {
        int numAttr = train.getNumAttributes();
        float[] min = new float[numAttr];
        float[] max = new float[numAttr];
        Arrays.fill(min, Float.POSITIVE_INFINITY);
        Arrays.fill(max, Float.NEGATIVE_INFINITY);

        // get max and min values and normalize
        for (int attr = 0; attr < numAttr; ++attr){
            for (float[] instance : train.getInstances()){
                max[attr] = Math.max(max[attr], instance[attr]);
                min[attr] = Math.min(min[attr], instance[attr]);
            }
            for (float[] instance : test.getInstances()){
                max[attr] = Math.max(max[attr], instance[attr]);
                min[attr] = Math.min(min[attr], instance[attr]);
            }
            float diff = (max[attr] - min[attr]);
            if(diff == 0) diff += Float.MIN_VALUE;
            for (int i = 0; i < train.length(); ++i)
                train.setAttribute(i, attr, a + (train.getAttribute(i, attr) - min[attr]) * (b - a) / diff);
            for (int i = 0; i < test.length(); ++i)
                test.setAttribute(i, attr, a + (test.getAttribute(i, attr) - min[attr]) * (b - a) / diff);
        }
    }

    private static void normalizeFeature(InputData input, int a, int b) {
        int numAttr = input.getNumAttributes();
        float[] min = new float[numAttr];
        float[] max = new float[numAttr];
        Arrays.fill(min, Float.POSITIVE_INFINITY);
        Arrays.fill(max, Float.NEGATIVE_INFINITY);

        // get max and min values and normalize
        for (int attr = 0; attr < numAttr; ++attr){
            for (float[] instance : input.getInstances()){
                max[attr] = Math.max(max[attr], instance[attr]);
                min[attr] = Math.min(min[attr], instance[attr]);
            }
            float diff = (max[attr] - min[attr]);
            if(diff == 0) diff += Float.MIN_VALUE;
            for (int i = 0; i < input.length(); ++i)
                input.setAttribute(i, attr, a + (input.getAttribute(i, attr) - min[attr]) * (b - a) / diff);
        }
    }

    private static void normalizeStandard(InputData train, InputData test, int a) {
        int numInstTrain = train.length();
        int numInstTest = test.length();
        int numAttr = train.getNumAttributes();
        float[] means = new float[numAttr];
        float[] stdDev = new float[numAttr];

        // calculate mean for each attribute
        for (int attr = 0; attr < numAttr; ++attr){
            for (float[] instance : train.getInstances()) means[attr] += instance[attr];
            for (float[] instance : test.getInstances()) means[attr] += instance[attr];
            means[attr] = means[attr] / (numInstTrain + numInstTest);
        }

        // calculate standard deviation for each attribute
        for (int attr = 0; attr < numAttr; ++attr){
            for (float[] instance : train.getInstances())
                stdDev[attr] += (instance[attr] - means[attr]) * (instance[attr] - means[attr]);
            for (float[] instance : test.getInstances())
                stdDev[attr] += (instance[attr] - means[attr]) * (instance[attr] - means[attr]);
            stdDev[attr] = (float) Math.sqrt(stdDev[attr] / (numInstTrain + numInstTest));
            // prevent dividing with 0
            stdDev[attr] += EPSILON;
        }

        // normalize attributes
        for (int attr = 0; attr < numAttr; ++attr) {
            for (int i = 0; i < numInstTrain; ++i)
                train.setAttribute(i, attr, (train.getAttribute(i, attr) - means[attr]) / (a * stdDev[attr]));
            for (int i = 0; i < numInstTest; ++i)
                test.setAttribute(i, attr, (test.getAttribute(i, attr) - means[attr]) / (a * stdDev[attr]));
        }
    }

    private static void normalizeStandard(InputData input, int a){
        int numInst = input.length();
        int numAttr = input.getNumAttributes();
        float[] means = new float[input.getNumAttributes()];
        float[] stdDev = new float[input.getNumAttributes()];

        // calculate mean for each attribute
        for (int attr = 0; attr < numAttr; ++attr){
            for (float[] instance : input.getInstances()) means[attr] += instance[attr];
            means[attr] = means[attr] / numInst;
        }

        // calculate standard deviation for each attribute
        for (int attr = 0; attr < numAttr; ++attr){
            for (float[] instance : input.getInstances())
                stdDev[attr] += (instance[attr] - means[attr]) * (instance[attr] - means[attr]);
            stdDev[attr] = (float) Math.sqrt(stdDev[attr] / numInst);
            // prevent dividing with 0
            stdDev[attr] += EPSILON;
        }

        // normalize attributes
        for (int attr = 0; attr < numAttr; ++attr) {
            for (int i = 0; i < numInst; ++i)
                input.setAttribute(i, attr, (input.getAttribute(i, attr) - means[attr]) / (a * stdDev[attr]));
        }
    }
}
