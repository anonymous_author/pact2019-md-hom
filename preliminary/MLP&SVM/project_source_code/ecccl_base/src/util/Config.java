package util;

import opencl.PlatformUtil;

public abstract class Config {
    public static int PRINT_LEVEL = 2;
    public static int SVM_SIZE_MB_LOCAL_CACHE = 512;
    public static int PLATFORM_ID = 0;
    public static int DEVICE_ID = 0;
    public static PlatformUtil.DeviceType DEVICE_TYPE = PlatformUtil.DeviceType.GPU;
}
