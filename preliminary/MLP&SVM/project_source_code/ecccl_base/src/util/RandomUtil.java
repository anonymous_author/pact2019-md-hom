package util;

import java.util.Arrays;
import java.util.Random;

public class RandomUtil {
    public static int[] getShuffledRange(int size, Random rand) {
        int[] array = new int[size];
        for(int i = 0; i < size; i++) {
            array[i] = i;
        }

        // Shuffle array
        for (int i=size; i>1; i--) {
            int r = rand.nextInt(i);
            int t = array[i - 1];
            array[i - 1] = array[r];
            array[r] = t;
        }

        return array;
    }
}
