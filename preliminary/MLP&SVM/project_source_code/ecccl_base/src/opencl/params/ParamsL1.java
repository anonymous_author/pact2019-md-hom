package opencl.params;

import org.json.simple.JSONObject;
import util.JSONUtil;

public class ParamsL1 {
    public static final String TUNING_FILE_RBF_MAP = "/tuning_results/svm/rbf_map_kernel_tp.json";
    public static final String TUNING_FILE_ERROR_UPDATE = "/tuning_results/svm/error_update_kernel_tp.json";
    public static final String TUNING_FILE_DELTA_INIT = "/tuning_results/mlp_old/delta_init_tp.json";

    public int CACHE_L_CB;
    public int CACHE_P_CB;

    public int G_CB_RES_DEST_LEVEL;
    public int L_CB_RES_DEST_LEVEL;
    public int P_CB_RES_DEST_LEVEL;

    public int L_CB_SIZE_L_1;
    public int P_CB_SIZE_L_1;

    public int NUM_WG_L_1;
    public int NUM_WI_L_1;
    public int OCL_DIM_L_1;

    public int LOCAL_SIZE_DIM_0;
    public int GLOBAL_SIZE_DIM_0;

    // Work dimension
    public int WORK_DIM = 1;

    public ParamsL1(String path) {
        JSONObject tps = JSONUtil.readFileArgs(path);
        setVars(tps);
    }

    public ParamsL1(JSONObject tps) {
        setVars(tps);
    }

    private void setVars(JSONObject tps) {
        CACHE_L_CB = ((Long) tps.get("CACHE_L_CB")).intValue();
        CACHE_P_CB = ((Long) tps.get("CACHE_P_CB")).intValue();
        G_CB_RES_DEST_LEVEL = ((Long) tps.get("G_CB_RES_DEST_LEVEL")).intValue();
        L_CB_RES_DEST_LEVEL = ((Long) tps.get("L_CB_RES_DEST_LEVEL")).intValue();
        P_CB_RES_DEST_LEVEL = ((Long) tps.get("P_CB_RES_DEST_LEVEL")).intValue();
        L_CB_SIZE_L_1 = ((Long) tps.get("L_CB_SIZE_L_1")).intValue();
        P_CB_SIZE_L_1 = ((Long) tps.get("P_CB_SIZE_L_1")).intValue();
        NUM_WG_L_1 = ((Long) tps.get("NUM_WG_L_1")).intValue();
        NUM_WI_L_1 = ((Long) tps.get("NUM_WI_L_1")).intValue();
        OCL_DIM_L_1 = ((Long) tps.get("OCL_DIM_L_1")).intValue();

        LOCAL_SIZE_DIM_0 = NUM_WI_L_1;
        GLOBAL_SIZE_DIM_0 = NUM_WG_L_1 * NUM_WI_L_1;
    }

    public String getOptions() {
        return "-D CACHE_L_CB=" + CACHE_L_CB +
                " -D CACHE_P_CB=" + CACHE_P_CB +
                " -D G_CB_RES_DEST_LEVEL=" + G_CB_RES_DEST_LEVEL +
                " -D L_CB_RES_DEST_LEVEL=" + L_CB_RES_DEST_LEVEL +
                " -D P_CB_RES_DEST_LEVEL=" + P_CB_RES_DEST_LEVEL +
                " -D L_CB_SIZE_L_1=" + L_CB_SIZE_L_1 +
                " -D P_CB_SIZE_L_1=" + P_CB_SIZE_L_1 +
                " -D NUM_WG_L_1=" + NUM_WG_L_1 +
                " -D NUM_WI_L_1=" + NUM_WI_L_1 +
                " -D OCL_DIM_L_1=" + OCL_DIM_L_1;
    }

    public String getOption(int sizeL1) {
        return "-D CACHE_L_CB=" + CACHE_L_CB +
                " -D CACHE_P_CB=" + CACHE_P_CB +
                " -D G_CB_RES_DEST_LEVEL=" + G_CB_RES_DEST_LEVEL +
                " -D L_CB_RES_DEST_LEVEL=" + L_CB_RES_DEST_LEVEL +
                " -D P_CB_RES_DEST_LEVEL=" + P_CB_RES_DEST_LEVEL +
                " -D G_CB_SIZE_L_1=" + sizeL1 +
                " -D L_CB_SIZE_L_1=" + L_CB_SIZE_L_1 +
                " -D P_CB_SIZE_L_1=" + P_CB_SIZE_L_1 +
                " -D NUM_WG_L_1=" + NUM_WG_L_1 +
                " -D NUM_WI_L_1=" + NUM_WI_L_1 +
                " -D OCL_DIM_L_1=" + OCL_DIM_L_1;
    }

    public String getOptions(int numInstances, float gamma){
        return "-D GAMMA=" + gamma + " " + getOption(numInstances);
    }
}
