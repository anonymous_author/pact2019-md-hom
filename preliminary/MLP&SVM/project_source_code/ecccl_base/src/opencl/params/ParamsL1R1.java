package opencl.params;

import opencl.ParamsUtil;
import org.json.simple.JSONObject;
import util.JSONUtil;

public class ParamsL1R1 {
    public static final String TUNING_FILE_RBF = "/tuning_results/svm/rbf_main_kernel_tp.json";
    public static final String TUNING_FILE_LINEAR = "/tuning_results/svm/linear_kernel_tp.json";
    public static final String TUNING_FILE_RBF_SCORE = "/tuning_results/svm/eval_rbf_score_tp.json";

    private static ParamsL1R1 PARAMS_RBF;
    private static ParamsL1R1 PARAMS_RBF_SCORE;

    public static ParamsL1R1 getParamsRbf() {
        if(PARAMS_RBF == null) {
            PARAMS_RBF = new ParamsL1R1(TUNING_FILE_RBF);
        }
        return PARAMS_RBF;
    }

    public static ParamsL1R1 getParamsRbfScore() {
        if(PARAMS_RBF_SCORE == null) {
            PARAMS_RBF_SCORE = new ParamsL1R1(TUNING_FILE_RBF_SCORE);
        }
        return PARAMS_RBF_SCORE;
    }

    public int CACHE_L_CB;
    public int CACHE_P_CB;

    public int G_CB_RES_DEST_LEVEL;
    public int L_CB_RES_DEST_LEVEL;
    public int P_CB_RES_DEST_LEVEL;

    // L1 Dimension
    public int L_CB_SIZE_L_1;
    public int P_CB_SIZE_L_1;

    public int NUM_WG_L_1;
    public int NUM_WI_L_1;
    public int OCL_DIM_L_1;

    // R1 Dimension
    public int L_CB_SIZE_R_1;
    public int P_CB_SIZE_R_1;

    public int NUM_WG_R_1;
    public int NUM_WI_R_1;
    public int OCL_DIM_R_1;

    public int LOCAL_SIZE_DIM_0;
    public int GLOBAL_SIZE_DIM_0;

    public int LOCAL_SIZE_DIM_1;
    public int GLOBAL_SIZE_DIM_1;

    public int K2_LOCAL_SIZE_DIM_0;
    public int K2_GLOBAL_SIZE_DIM_0;

    public int K2_LOCAL_SIZE_DIM_1;
    public int K2_GLOBAL_SIZE_DIM_1;

    // Work dimension
    public int WORK_DIM = 2;

    public ParamsL1R1(String path) {
        JSONObject tps = JSONUtil.readFileArgs(path);
        setVars(tps);
    }

    public ParamsL1R1(JSONObject tps) {
        setVars(tps);
    }

    private void setVars(JSONObject tps) {
        CACHE_L_CB = ((Long) tps.get("CACHE_L_CB")).intValue();
        CACHE_P_CB = ((Long) tps.get("CACHE_P_CB")).intValue();
        G_CB_RES_DEST_LEVEL = ((Long) tps.get("G_CB_RES_DEST_LEVEL")).intValue();
        L_CB_RES_DEST_LEVEL = ((Long) tps.get("L_CB_RES_DEST_LEVEL")).intValue();
        P_CB_RES_DEST_LEVEL = ((Long) tps.get("P_CB_RES_DEST_LEVEL")).intValue();
        L_CB_SIZE_L_1 = ((Long) tps.get("L_CB_SIZE_L_1")).intValue();
        P_CB_SIZE_L_1 = ((Long) tps.get("P_CB_SIZE_L_1")).intValue();
        NUM_WG_L_1 = ((Long) tps.get("NUM_WG_L_1")).intValue();
        NUM_WI_L_1 = ((Long) tps.get("NUM_WI_L_1")).intValue();
        L_CB_SIZE_R_1 = ((Long) tps.get("L_CB_SIZE_R_1")).intValue();
        P_CB_SIZE_R_1 = ((Long) tps.get("P_CB_SIZE_R_1")).intValue();
        NUM_WG_R_1 = ((Long) tps.get("NUM_WG_R_1")).intValue();
        NUM_WI_R_1 = ((Long) tps.get("NUM_WI_R_1")).intValue();
        OCL_DIM_L_1 = ((Long) tps.get("OCL_DIM_L_1")).intValue();
        OCL_DIM_R_1 = ((Long) tps.get("OCL_DIM_R_1")).intValue();

        if(OCL_DIM_L_1 == 0) {
            LOCAL_SIZE_DIM_0 = K2_LOCAL_SIZE_DIM_0 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_0 = K2_GLOBAL_SIZE_DIM_0 = NUM_WG_L_1 * NUM_WI_L_1;
        } else if (OCL_DIM_L_1 == 1) {
            LOCAL_SIZE_DIM_1 = K2_LOCAL_SIZE_DIM_1 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_1 = K2_GLOBAL_SIZE_DIM_1 = NUM_WG_L_1 * NUM_WI_L_1;
        }

        if(OCL_DIM_R_1 == 0) {
            LOCAL_SIZE_DIM_0 = NUM_WI_R_1;
            GLOBAL_SIZE_DIM_0 = NUM_WG_R_1 * NUM_WI_R_1;
            K2_LOCAL_SIZE_DIM_0 = getLocalSizeSecondKernelStatic();
            K2_GLOBAL_SIZE_DIM_0 = getLocalSizeSecondKernelStatic();
        } else if (OCL_DIM_R_1 == 1) {
            LOCAL_SIZE_DIM_1 = NUM_WI_R_1;
            GLOBAL_SIZE_DIM_1 = NUM_WG_R_1 * NUM_WI_R_1;
            K2_LOCAL_SIZE_DIM_1 = getLocalSizeSecondKernelStatic();
            K2_GLOBAL_SIZE_DIM_1 = getLocalSizeSecondKernelStatic();
        }
    }

    public String getOptions(int SIZE_L_1, int SIZE_R_1){
        return "-D CACHE_L_CB=" + CACHE_L_CB +
                " -D CACHE_P_CB=" + CACHE_P_CB +
                " -D G_CB_RES_DEST_LEVEL=" + G_CB_RES_DEST_LEVEL +
                " -D L_CB_RES_DEST_LEVEL=" + L_CB_RES_DEST_LEVEL +
                " -D P_CB_RES_DEST_LEVEL=" + P_CB_RES_DEST_LEVEL +
                " -D G_CB_SIZE_L_1=" + SIZE_L_1 +
                " -D L_CB_SIZE_L_1=" + L_CB_SIZE_L_1 +
                " -D P_CB_SIZE_L_1=" + P_CB_SIZE_L_1 +
                " -D NUM_WG_L_1=" + NUM_WG_L_1 +
                " -D NUM_WI_L_1=" + NUM_WI_L_1 +
                " -D OCL_DIM_L_1=" + OCL_DIM_L_1 +
                " -D G_CB_SIZE_R_1=" + SIZE_R_1 +
                " -D L_CB_SIZE_R_1=" + L_CB_SIZE_R_1 +
                " -D P_CB_SIZE_R_1=" + P_CB_SIZE_R_1 +
                " -D NUM_WG_R_1=" + NUM_WG_R_1 +
                " -D NUM_WI_R_1=" + NUM_WI_R_1 +
                " -D OCL_DIM_R_1=" + OCL_DIM_R_1;
    }

    public String getOptions(){
        return "-D CACHE_L_CB=" + CACHE_L_CB +
                " -D CACHE_P_CB=" + CACHE_P_CB +
                " -D G_CB_RES_DEST_LEVEL=" + G_CB_RES_DEST_LEVEL +
                " -D L_CB_RES_DEST_LEVEL=" + L_CB_RES_DEST_LEVEL +
                " -D P_CB_RES_DEST_LEVEL=" + P_CB_RES_DEST_LEVEL +
                " -D L_CB_SIZE_L_1=" + L_CB_SIZE_L_1 +
                " -D P_CB_SIZE_L_1=" + P_CB_SIZE_L_1 +
                " -D NUM_WG_L_1=" + NUM_WG_L_1 +
                " -D NUM_WI_L_1=" + NUM_WI_L_1 +
                " -D OCL_DIM_L_1=" + OCL_DIM_L_1 +
                " -D L_CB_SIZE_R_1=" + L_CB_SIZE_R_1 +
                " -D P_CB_SIZE_R_1=" + P_CB_SIZE_R_1 +
                " -D NUM_WG_R_1=" + NUM_WG_R_1 +
                " -D NUM_WI_R_1=" + NUM_WI_R_1 +
                " -D OCL_DIM_R_1=" + OCL_DIM_R_1;
    }

    public String getOptions(int SIZE_L_1, int SIZE_R_1, float gamma){
        return "-D GAMMA=" + gamma + " " + getOptions(SIZE_L_1, SIZE_R_1);
    }

    public String getOptions(float gamma){
        return "-D GAMMA=" + gamma + " " + getOptions();
    }

    public int getLocalSizeSecondKernel(int G_CB_SIZE_R) {
        int min = Math.min(L_CB_SIZE_R_1, (Math.min(NUM_WG_R_1, ceil(G_CB_SIZE_R, L_CB_SIZE_R_1))));
        return Math.min(NUM_WI_R_1, ceil(min , Math.min(P_CB_SIZE_R_1, min)));
    }

    public int K2_G_CB_SIZE_R_1(int G_CB_SIZE_R_1) {
        return Math.min(NUM_WG_R_1, ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1));
    }

    public int getLocalSizeSecondKernelStatic() {
        return Math.min(NUM_WI_R_1, ceil(L_CB_SIZE_R_1, P_CB_SIZE_R_1));
    }

    public long getResGSizeKernelOne(int G_CB_SIZE_L1, int G_CB_SIZE_R1, int elemSize) {
        return ParamsUtil.getResGSizeKernelOne(G_CB_SIZE_L1, G_CB_SIZE_R1, elemSize,
                G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL,
                NUM_WG_R_1, NUM_WI_R_1);
    }

    private int ceil(int x, int y){
        return (x + y - 1) / y;
    }
}
