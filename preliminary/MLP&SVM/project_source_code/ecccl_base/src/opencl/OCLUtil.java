package opencl;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.opencl.CL10;

import java.nio.*;

import static org.lwjgl.opencl.CLUtil.checkCLError;

public class OCLUtil {
    public enum MemType {
        USE(CL10.CL_MEM_USE_HOST_PTR),
        COPY(CL10.CL_MEM_COPY_HOST_PTR),
        ALLOC(CL10.CL_MEM_ALLOC_HOST_PTR);

        private final int type;

        MemType(int type) { this.type = type; }

        public int get() {
            return this.type;
        }
    }

    public static long createBuffer(Kernel.RWFlag flag, MemType memType, long size) {
        IntBuffer errorCode = BufferUtils.createIntBuffer(1);

        long ptr = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | memType.get(), size, errorCode);
        checkCLError(errorCode);

        return ptr;
    }

    public static long createBuffer(Kernel.RWFlag flag, long size) {
        IntBuffer errorCode = BufferUtils.createIntBuffer(1);

        long ptr = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | MemType.ALLOC.get(), size, errorCode);
        checkCLError(errorCode);

        return ptr;
    }

    public static long createBuffer(Kernel.RWFlag flag, MemType memType, FloatBuffer buffer) {
        IntBuffer errorCode = BufferUtils.createIntBuffer(1);
        long ptr = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | memType.get(), buffer, errorCode);
        checkCLError(errorCode);
        return ptr;
    }

    public static long createBuffer(Kernel.RWFlag flag, MemType memType, IntBuffer buffer) {
        IntBuffer errorCode = BufferUtils.createIntBuffer(1);
        long ptr = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | memType.get(), buffer, errorCode);
        checkCLError(errorCode);
        return ptr;
    }

    public static long createBuffer(Kernel.RWFlag flag, MemType memType, ByteBuffer buffer) {
        IntBuffer errorCode = BufferUtils.createIntBuffer(1);
        long ptr = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | memType.get(), buffer, errorCode);
        checkCLError(errorCode);
        return ptr;
    }

    public static long createZeroFloatBuffer(Kernel.RWFlag flag, int capacity) {
        FloatBuffer oldDBuffer = BufferUtils.createFloatBuffer(capacity);
        oldDBuffer.put(new float[capacity]).flip();
        return createBuffer(flag, MemType.COPY, oldDBuffer);
    }

    public static FloatBuffer read(FloatBuffer buffer, long ptr, int blocking) {
        int ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), ptr, blocking, 0, buffer, null, null);
        checkCLError(ret);

        return buffer;
    }

    public static FloatBuffer write(FloatBuffer buffer, long ptr, int blocking) {
        int ret = CL10.clEnqueueWriteBuffer(PlatformUtil.getCommandQueue(), ptr, blocking, 0, buffer, null, null);
        checkCLError(ret);
        return buffer;
    }

    public static IntBuffer write(IntBuffer buffer, long ptr, int blocking) {
        int ret = CL10.clEnqueueWriteBuffer(PlatformUtil.getCommandQueue(), ptr, blocking, 0, buffer, null, null);
        checkCLError(ret);
        return buffer;
    }

    public static void write(ByteBuffer buffer, long ptr, int blocking) {
        int ret = CL10.clEnqueueWriteBuffer(PlatformUtil.getCommandQueue(), ptr, blocking, 0, buffer, null, null);
        checkCLError(ret);
    }

    public static void copy(int size, long src, long dest) {
        int ret = CL10.clEnqueueCopyBuffer(PlatformUtil.getCommandQueue(), src, dest, 0, 0, size, 0, null, null);
        checkCLError(ret);
    }

    public static long writeProfile(ByteBuffer buffer, long ptr, int blocking) {
        int ret;
        PointerBuffer event = BufferUtils.createPointerBuffer(1);
        ByteBuffer timeStart = BufferUtils.createByteBuffer(8);
        ByteBuffer timeEnd = BufferUtils.createByteBuffer(8);

        ret = CL10.clEnqueueWriteBuffer(PlatformUtil.getCommandQueue(), ptr,  blocking, 0, buffer, null, event);
        checkCLError(ret);

        CL10.clWaitForEvents(event);

        CL10.clGetEventProfilingInfo(event.get(0), CL10.CL_PROFILING_COMMAND_START, timeStart, null);
        CL10.clGetEventProfilingInfo(event.get(0), CL10.CL_PROFILING_COMMAND_END, timeEnd, null);
        long start = timeStart.getLong(0);
        long end = timeEnd.getLong(0);
        return (end - start);
    }

    public static long readProfile(FloatBuffer buffer, long ptr, int blocking) {
        PointerBuffer event = BufferUtils.createPointerBuffer(1);
        ByteBuffer timeStart = BufferUtils.createByteBuffer(8);
        ByteBuffer timeEnd = BufferUtils.createByteBuffer(8);

        int ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), ptr, blocking, 0, buffer, null, event);
        checkCLError(ret);

        checkCLError(CL10.clWaitForEvents(event));
        checkCLError(CL10.clGetEventProfilingInfo(event.get(0), CL10.CL_PROFILING_COMMAND_START, timeStart, null));
        checkCLError(CL10.clGetEventProfilingInfo(event.get(0), CL10.CL_PROFILING_COMMAND_END, timeEnd, null));

        long start = timeStart.getLong(0);
        long end = timeEnd.getLong(0);
        timeEnd.rewind();
        timeStart.rewind();

        return end - start;
    }

    public static float[][] bufferToArr(FloatBuffer buffer, int height, int width) {
        float[][] weightsArr = new float[height][width];
        for (int k = 0; k < height; k++) {
            buffer.get(weightsArr[k]);
        }
        return weightsArr;
    }

    public static void debugPrint(long ptr, int size1, int size2) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(size1 * size2);
        int ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), ptr, PlatformUtil.BLOCKING_OP_READ, 0, buffer, null, null);
        checkCLError(ret);

        for(int k = 0; k < size1; k++) {
            for(int j = 0; j < size2; j++) {
                System.out.print(String.format("%f ", buffer.get()) + " ");
            }
            System.out.println();
        }
    }

    public static void debugPrintWeights(long weight, int layer, int[] layerSizes) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(layerSizes[layer] * layerSizes[layer + 1]);
        int ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), weight, PlatformUtil.BLOCKING_OP_READ, 0, buffer, null, null);
        checkCLError(ret);

        for(int k = 0; k < layerSizes[layer]; k++) {
            for(int j = 0; j < layerSizes[layer + 1]; j++) {
                System.out.print(String.format("%f ", buffer.get()) + " ");
            }
            System.out.println();
        }
    }

    public static synchronized void releaseMem(long ptr) {
        CL10.clReleaseMemObject(ptr);
    }

    public static synchronized void releaseMem(long[] ptr) {
        for(long pointer : ptr) CL10.clReleaseMemObject(pointer);
    }
}
