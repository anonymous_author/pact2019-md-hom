package opencl;

import java.nio.Buffer;

public class OCLBuffer {
    private Buffer buffer;
    private long pointer;
    private int rows;

    public OCLBuffer(Buffer buffer, long pointer) {
        this.buffer = buffer;
        this.pointer = pointer;
    }

    public OCLBuffer(Buffer buffer, long pointer, int rows) {
        this.buffer = buffer;
        this.pointer = pointer;
        this.rows = rows;
    }

    public int rows() {
        return rows;
    }

    public long pointer() {
        return pointer;
    }

    public void setPointer(long pointer) {
        this.pointer = pointer;
    }

    public Buffer buffer() {
        return buffer;
    }

    public void setBuffer(Buffer buffer) {
            this.buffer = buffer;
        }
}