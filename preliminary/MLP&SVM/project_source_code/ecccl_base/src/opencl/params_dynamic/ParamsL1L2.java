package opencl.params_dynamic;

import org.json.simple.JSONObject;
import util.JSONUtil;

public class ParamsL1L2 {
    public static final String TUNING_FILE_WEIGHT_BIAS = "/tuning_results/eccmlp/weight_bias_tp.json";

    private int CACHE_L_CB;
    private int CACHE_P_CB;

    private int G_CB_RES_DEST_LEVEL;
    private int L_CB_RES_DEST_LEVEL;
    private int P_CB_RES_DEST_LEVEL;

    // L1 Dimension
    private int L_CB_SIZE_L_1;
    private int P_CB_SIZE_L_1;

    private int NUM_WG_L_1;
    private int NUM_WI_L_1;
    private int OCL_DIM_L_1;

    // L2 Dimension
    private int L_CB_SIZE_L_2;
    private int P_CB_SIZE_L_2;

    private int NUM_WG_L_2;
    private int NUM_WI_L_2;
    private int OCL_DIM_L_2;

    public int LOCAL_SIZE_DIM_0;
    public int GLOBAL_SIZE_DIM_0;

    public int LOCAL_SIZE_DIM_1;
    public int GLOBAL_SIZE_DIM_1;

    public int WORK_DIM = 2;

    public ParamsL1L2(String path) {
        JSONObject tps = JSONUtil.readFileArgs(path);
        setVars(tps);
    }

    public ParamsL1L2(JSONObject tps) {
        setVars(tps);
    }

    private void setVars(JSONObject tps) {
        CACHE_L_CB = ((Long) tps.get("CACHE_L_CB")).intValue();
        CACHE_P_CB = ((Long) tps.get("CACHE_P_CB")).intValue();
        G_CB_RES_DEST_LEVEL = ((Long) tps.get("G_CB_RES_DEST_LEVEL")).intValue();
        L_CB_RES_DEST_LEVEL = ((Long) tps.get("L_CB_RES_DEST_LEVEL")).intValue();
        P_CB_RES_DEST_LEVEL = ((Long) tps.get("P_CB_RES_DEST_LEVEL")).intValue();

        L_CB_SIZE_L_1 = ((Long) tps.get("L_CB_SIZE_L_1")).intValue();
        P_CB_SIZE_L_1 = ((Long) tps.get("P_CB_SIZE_L_1")).intValue();
        NUM_WG_L_1 = ((Long) tps.get("NUM_WG_L_1")).intValue();
        NUM_WI_L_1 = ((Long) tps.get("NUM_WI_L_1")).intValue();
        OCL_DIM_L_1 = ((Long) tps.get("OCL_DIM_L_1")).intValue();

        L_CB_SIZE_L_2 = ((Long) tps.get("L_CB_SIZE_L_2")).intValue();
        P_CB_SIZE_L_2 = ((Long) tps.get("P_CB_SIZE_L_2")).intValue();
        NUM_WG_L_2 = ((Long) tps.get("NUM_WG_L_2")).intValue();
        NUM_WI_L_2 = ((Long) tps.get("NUM_WI_L_2")).intValue();
        OCL_DIM_L_2 = ((Long) tps.get("OCL_DIM_L_2")).intValue();

        if(OCL_DIM_L_1 == 0) {
            LOCAL_SIZE_DIM_0 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_0 = NUM_WG_L_1 * NUM_WI_L_1;
        } else if (OCL_DIM_L_1 == 1) {
            LOCAL_SIZE_DIM_1 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_1 = NUM_WG_L_1 * NUM_WI_L_1;
        }

        if(OCL_DIM_L_2 == 0) {
            LOCAL_SIZE_DIM_0 = NUM_WI_L_2;
            GLOBAL_SIZE_DIM_0 = NUM_WG_L_2 * NUM_WI_L_2;
        } else if (OCL_DIM_L_2 == 1) {
            LOCAL_SIZE_DIM_1 = NUM_WI_L_2;
            GLOBAL_SIZE_DIM_1 = NUM_WG_L_2 * NUM_WI_L_2;
        }
    }

    public String getOptions()
    {
        return "-D CACHE_L_CB=" + this.CACHE_L_CB + " -D CACHE_P_CB=" + this.CACHE_P_CB + " -D G_CB_RES_DEST_LEVEL=" + this.G_CB_RES_DEST_LEVEL + " -D L_CB_RES_DEST_LEVEL=" + this.L_CB_RES_DEST_LEVEL + " -D P_CB_RES_DEST_LEVEL=" + this.P_CB_RES_DEST_LEVEL + " -D L_CB_SIZE_L_1=" + this.L_CB_SIZE_L_1 + " -D P_CB_SIZE_L_1=" + this.P_CB_SIZE_L_1 + " -D NUM_WG_L_1=" + this.NUM_WG_L_1 + " -D NUM_WI_L_1=" + this.NUM_WI_L_1 + " -D OCL_DIM_L_1=" + this.OCL_DIM_L_1 + " -D L_CB_SIZE_L_2=" + this.L_CB_SIZE_L_2 + " -D P_CB_SIZE_L_2=" + this.P_CB_SIZE_L_2 + " -D NUM_WG_L_2=" + this.NUM_WG_L_2 + " -D NUM_WI_L_2=" + this.NUM_WI_L_2 + " -D OCL_DIM_L_2=" + this.OCL_DIM_L_2;
    }

    public String getOptions(int L1, int L2)
    {
        return "-D CACHE_L_CB=" + this.CACHE_L_CB + " -D CACHE_P_CB=" + this.CACHE_P_CB + " -D G_CB_RES_DEST_LEVEL=" + this.G_CB_RES_DEST_LEVEL + " -D L_CB_RES_DEST_LEVEL=" + this.L_CB_RES_DEST_LEVEL + " -D P_CB_RES_DEST_LEVEL=" + this.P_CB_RES_DEST_LEVEL + " -D G_CB_SIZE_L_1=" + L1 + " -D L_CB_SIZE_L_1=" + this.L_CB_SIZE_L_1 + " -D P_CB_SIZE_L_1=" + this.P_CB_SIZE_L_1 + " -D NUM_WG_L_1=" + this.NUM_WG_L_1 + " -D NUM_WI_L_1=" + this.NUM_WI_L_1 + " -D OCL_DIM_L_1=" + this.OCL_DIM_L_1 + " -D G_CB_SIZE_L_2=" + L2 + " -D L_CB_SIZE_L_2=" + this.L_CB_SIZE_L_2 + " -D P_CB_SIZE_L_2=" + this.P_CB_SIZE_L_2 + " -D NUM_WG_L_2=" + this.NUM_WG_L_2 + " -D NUM_WI_L_2=" + this.NUM_WI_L_2 + " -D OCL_DIM_L_2=" + this.OCL_DIM_L_2;
    }
}
