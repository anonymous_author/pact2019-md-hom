// helper macros
#define CONCAT_IN_DESCENDING_OCL_ORDER_0(i) i
#define CONCAT_IN_DESCENDING_OCL_ORDER(i) CONCAT_IN_DESCENDING_OCL_ORDER_0(i)

#define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, i_size) i_id
#define FLAT_INDEX_IN_DESCENDING_OCL_ORDER(i_id, i_size) FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, i_size)

// md_hom type definition
#define TYPE_T float
#define TYPE_TS float

#define PRIVATE 0
#define LOCAL   1
#define GLOBAL  2

// =============== macro definitions per dimension ============================
// -------------------- L_1 --------------------

// cache block sizes
#define K1_G_CB_SIZE_L_1 G_CB_SIZE_L_1
#define K1_L_CB_SIZE_L_1 L_CB_SIZE_L_1
#define K1_P_CB_SIZE_L_1 P_CB_SIZE_L_1

// functional unit ids
#define K1_G_FU_ID_L_1 i_wg_l_1
#define K1_L_FU_ID_L_1 i_wi_l_1
#define K1_P_FU_ID_L_1 0

// number of functional units
#define K1_G_NUM_FU_L_1 NUM_WG_L_1
#define K1_L_NUM_FU_L_1 NUM_WI_L_1
#define K1_P_NUM_FU_L_1 1

// number of cache blocks per functional unit
#define K1_G_NUM_CB_L_1 1 // == (M_1 / K1_G_CB_SIZE_L_1 / 1)
#define K1_L_NUM_CB_L_1 (K1_G_CB_SIZE_L_1 / K1_L_CB_SIZE_L_1 / K1_G_NUM_FU_L_1)
#define K1_P_NUM_CB_L_1 (K1_L_CB_SIZE_L_1 / K1_P_CB_SIZE_L_1 / K1_L_NUM_FU_L_1)

// number of extra cache blocks
#define K1_G_NUM_EXTRA_CB_L_1 0 // == (M_1 / K1_G_CB_SIZE_L_1 % 1)
#define K1_L_NUM_EXTRA_CB_L_1 (K1_G_CB_SIZE_L_1 / K1_L_CB_SIZE_L_1 % K1_G_NUM_FU_L_1)
#define K1_P_NUM_EXTRA_CB_L_1 (K1_L_CB_SIZE_L_1 / K1_P_CB_SIZE_L_1 % K1_L_NUM_FU_L_1)

// number of extra elements
#define K1_G_NUM_EXTRA_ELEMS_L_1 0 // == (M_1 % K1_G_CB_SIZE_L_1)
#define K1_L_NUM_EXTRA_ELEMS_L_1 (K1_G_CB_SIZE_L_1 % K1_L_CB_SIZE_L_1)
#define K1_P_NUM_EXTRA_ELEMS_L_1 (K1_L_CB_SIZE_L_1 % K1_P_CB_SIZE_L_1)

// size of incomplete cache blocks
#define K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 (K1_L_CB_SIZE_L_1 % K1_P_CB_SIZE_L_1)
#define K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 (K1_G_CB_SIZE_L_1 % K1_L_CB_SIZE_L_1 % K1_P_CB_SIZE_L_1)
#define K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 (K1_G_CB_SIZE_L_1 % K1_L_CB_SIZE_L_1)

// number of cache blocks in incomplete parent cache block per functional unit
#define K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 / K1_P_CB_SIZE_L_1 / K1_L_NUM_FU_L_1)

// number of extra cache blocks in incomplete parent cache block
#define K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 ((K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 / K1_P_CB_SIZE_L_1) % K1_L_NUM_FU_L_1)

// number of extra elements in incomplete parent cache block
#define K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 % K1_P_CB_SIZE_L_1)

// cache block offsets
#define K1_G_CB_OFFSET_L_1 0 // == (K1_G_CB_SIZE_L_1 * (0 + i_g_cb_L_1 * 1))
#define K1_L_CB_OFFSET_L_1 (K1_L_CB_SIZE_L_1 * (K1_G_FU_ID_L_1 + i_l_cb_l_1 * K1_G_NUM_FU_L_1))
#define K1_P_CB_OFFSET_L_1 (K1_P_CB_SIZE_L_1 * (K1_L_FU_ID_L_1 + i_p_cb_l_1 * K1_L_NUM_FU_L_1))


// -------------------- combined over dimensions --------------------

// flat WI ids
#define K1_G_FLAT_WI_ID (FLAT_INDEX_IN_DESCENDING_OCL_ORDER(get_global_id(OCL_DIM_L_1), get_global_size(OCL_DIM_L_1)))
#define K1_L_FLAT_WI_ID (FLAT_INDEX_IN_DESCENDING_OCL_ORDER(get_local_id(OCL_DIM_L_1), get_local_size(OCL_DIM_L_1)))
#define K1_P_FLAT_WI_ID (0)

// flat number of WIs
#define K1_G_FLAT_NUM_WI (K1_G_NUM_FU_L_1 * K1_L_NUM_FU_L_1)
#define K1_L_FLAT_NUM_WI (K1_L_NUM_FU_L_1)
#define K1_P_FLAT_NUM_WI (1)
// =============== end of macro definitions per dimension =====================

// =============== macro definitions per buffer ===============================
// -------------------- buffer CACHE_DATA_I --------------------

// buffer abstraction
#define BUFFER_CACHE_DATA_I_INDEX_0(i) i
#define BUFFER_CACHE_DATA_I_G_SIZE_0 K1_G_CB_SIZE_L_1
#define BUFFER_CACHE_DATA_I_L_SIZE_0 K1_L_CB_SIZE_L_1
#define BUFFER_CACHE_DATA_I_P_SIZE_0 K1_P_CB_SIZE_L_1
#define K1_G_BUFFER_CACHE_DATA_I(i) cache_data_i[(i)]
#define K1_L_BUFFER_CACHE_DATA_I(i) cb_l_cache_data_i[(BUFFER_CACHE_DATA_I_INDEX_0(i))]
#define K1_P_BUFFER_CACHE_DATA_I(i) cb_p_cache_data_i[(BUFFER_CACHE_DATA_I_INDEX_0(i))]

// partitioning and cache usage
#define K1_G_MEM_CACHE_DATA_I(i) K1_G_BUFFER_CACHE_DATA_I(i)
#if CACHE_L_CB != 0
#define K1_L_MEM_CACHE_DATA_I(i) K1_L_BUFFER_CACHE_DATA_I(i)
#else
#define K1_L_MEM_CACHE_DATA_I(i) K1_G_MEM_CACHE_DATA_I(K1_L_CB_OFFSET_L_1 + (i))
#endif
#if CACHE_P_CB != 0
#define K1_P_MEM_CACHE_DATA_I(i) K1_P_BUFFER_CACHE_DATA_I(i)
#else
#define K1_P_MEM_CACHE_DATA_I(i) K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + (i))
#endif

// cache block sizes
#define K1_G_CB_SIZE_CACHE_DATA_I (K1_G_CB_SIZE_L_1)
#define K1_L_CB_SIZE_CACHE_DATA_I (K1_L_CB_SIZE_L_1)
#define K1_P_CB_SIZE_CACHE_DATA_I (K1_P_CB_SIZE_L_1)


// -------------------- buffer CACHE_DATA_J --------------------

// buffer abstraction
#define BUFFER_CACHE_DATA_J_INDEX_0(i) i
#define BUFFER_CACHE_DATA_J_G_SIZE_0 K1_G_CB_SIZE_L_1
#define BUFFER_CACHE_DATA_J_L_SIZE_0 K1_L_CB_SIZE_L_1
#define BUFFER_CACHE_DATA_J_P_SIZE_0 K1_P_CB_SIZE_L_1
#define K1_G_BUFFER_CACHE_DATA_J(i) cache_data_j[(i)]
#define K1_L_BUFFER_CACHE_DATA_J(i) cb_l_cache_data_j[(BUFFER_CACHE_DATA_J_INDEX_0(i))]
#define K1_P_BUFFER_CACHE_DATA_J(i) cb_p_cache_data_j[(BUFFER_CACHE_DATA_J_INDEX_0(i))]

// partitioning and cache usage
#define K1_G_MEM_CACHE_DATA_J(i) K1_G_BUFFER_CACHE_DATA_J(i)
#if CACHE_L_CB != 0
#define K1_L_MEM_CACHE_DATA_J(i) K1_L_BUFFER_CACHE_DATA_J(i)
#else
#define K1_L_MEM_CACHE_DATA_J(i) K1_G_MEM_CACHE_DATA_J(K1_L_CB_OFFSET_L_1 + (i))
#endif
#if CACHE_P_CB != 0
#define K1_P_MEM_CACHE_DATA_J(i) K1_P_BUFFER_CACHE_DATA_J(i)
#else
#define K1_P_MEM_CACHE_DATA_J(i) K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + (i))
#endif

// cache block sizes
#define K1_G_CB_SIZE_CACHE_DATA_J (K1_G_CB_SIZE_L_1)
#define K1_L_CB_SIZE_CACHE_DATA_J (K1_L_CB_SIZE_L_1)
#define K1_P_CB_SIZE_CACHE_DATA_J (K1_P_CB_SIZE_L_1)


// -------------------- buffer ERROR --------------------

// buffer abstraction
#define BUFFER_ERROR_INDEX_0(i) i
#define BUFFER_ERROR_G_SIZE_0 K1_G_CB_SIZE_L_1
#define BUFFER_ERROR_L_SIZE_0 K1_L_CB_SIZE_L_1
#define BUFFER_ERROR_P_SIZE_0 K1_P_CB_SIZE_L_1
#define K1_G_BUFFER_ERROR(i) error[(i)]
#define K1_L_BUFFER_ERROR(i) cb_l_error[(BUFFER_ERROR_INDEX_0(i))]
#define K1_P_BUFFER_ERROR(i) cb_p_error[(BUFFER_ERROR_INDEX_0(i))]

// partitioning and cache usage
#define K1_G_MEM_ERROR(i) K1_G_BUFFER_ERROR(i)
#if CACHE_L_CB != 0
#define K1_L_MEM_ERROR(i) K1_L_BUFFER_ERROR(i)
#else
#define K1_L_MEM_ERROR(i) K1_G_MEM_ERROR(K1_L_CB_OFFSET_L_1 + (i))
#endif
#if CACHE_P_CB != 0
#define K1_P_MEM_ERROR(i) K1_P_BUFFER_ERROR(i)
#else
#define K1_P_MEM_ERROR(i) K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + (i))
#endif

// cache block sizes
#define K1_G_CB_SIZE_ERROR (K1_G_CB_SIZE_L_1)
#define K1_L_CB_SIZE_ERROR (K1_L_CB_SIZE_L_1)
#define K1_P_CB_SIZE_ERROR (K1_P_CB_SIZE_L_1)


// -------------------- result buffer --------------------

// check which levels are used
#if G_CB_RES_DEST_LEVEL == PRIVATE || L_CB_RES_DEST_LEVEL == PRIVATE || P_CB_RES_DEST_LEVEL == PRIVATE
#define K1_P_LEVEL_HAS_RESULTS
#endif
#if G_CB_RES_DEST_LEVEL == LOCAL || L_CB_RES_DEST_LEVEL == LOCAL || P_CB_RES_DEST_LEVEL == LOCAL
#define K1_L_LEVEL_HAS_RESULTS
#endif
#if G_CB_RES_DEST_LEVEL == GLOBAL || L_CB_RES_DEST_LEVEL == GLOBAL || P_CB_RES_DEST_LEVEL == GLOBAL
#define K1_G_LEVEL_HAS_RESULTS
#endif

// ------ PRIVATE ------
#ifdef K1_P_LEVEL_HAS_RESULTS
// construct prefix for res_p
#if G_CB_RES_DEST_LEVEL == PRIVATE
#define K1_RES_P_BUFFER_G_PREFIX_L_1() [i_l_cb_l_1]
#define K1_RES_P_BUFFER_DEF_G_PREFIX_L_1() [K1_L_NUM_CB_L_1 + ((K1_L_NUM_EXTRA_CB_L_1 + K1_L_NUM_EXTRA_ELEMS_L_1) > 0)]
#else
#define K1_RES_P_BUFFER_G_PREFIX_L_1()
#define K1_RES_P_BUFFER_DEF_G_PREFIX_L_1()
#endif
#if L_CB_RES_DEST_LEVEL == PRIVATE
#define K1_RES_P_BUFFER_L_PREFIX_L_1() [i_p_cb_l_1]
#define K1_RES_P_BUFFER_DEF_L_PREFIX_L_1() [K1_P_NUM_CB_L_1 + ((K1_P_NUM_EXTRA_CB_L_1 + K1_P_NUM_EXTRA_ELEMS_L_1) > 0)]
#else
#define K1_RES_P_BUFFER_L_PREFIX_L_1()
#define K1_RES_P_BUFFER_DEF_L_PREFIX_L_1()
#endif
// buffer abstraction for res_p
#define K1_RES_P_BUFFER_NAME() res_p
#define K1_RES_P_BUFFER_DEF K1_RES_P_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_P_BUFFER_DEF_G_PREFIX_L_1()K1_RES_P_BUFFER_DEF_L_PREFIX_L_1()[K1_P_CB_SIZE_L_1])
#define K1_RES_P_BUFFER(i) K1_RES_P_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_P_BUFFER_G_PREFIX_L_1()K1_RES_P_BUFFER_L_PREFIX_L_1()[(i)])
#endif

// ------ LOCAL ------
#ifdef K1_L_LEVEL_HAS_RESULTS
// construct prefix for res_l
#if G_CB_RES_DEST_LEVEL == LOCAL
#define K1_RES_L_BUFFER_G_PREFIX_L_1() [i_l_cb_l_1]
#define K1_RES_L_BUFFER_DEF_G_PREFIX_L_1() [K1_L_NUM_CB_L_1 + ((K1_L_NUM_EXTRA_CB_L_1 + K1_L_NUM_EXTRA_ELEMS_L_1) > 0)]
#else
#define K1_RES_L_BUFFER_G_PREFIX_L_1()
#define K1_RES_L_BUFFER_DEF_G_PREFIX_L_1()
#endif
// buffer abstraction for res_l
#define K1_RES_L_BUFFER_NAME() res_l
#define K1_RES_L_BUFFER_DEF K1_RES_L_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_L_BUFFER_DEF_G_PREFIX_L_1()[K1_L_CB_SIZE_L_1])
#define K1_RES_L_BUFFER(i) K1_RES_L_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_L_BUFFER_G_PREFIX_L_1()[K1_P_CB_OFFSET_L_1 + (i)])
#endif

// ------ GLOBAL ------
#ifdef K1_G_LEVEL_HAS_RESULTS
// buffer abstraction for res_g
#define K1_RES_G_BUFFER_NAME() error
#define K1_RES_G_BUFFER(i) K1_RES_G_BUFFER_NAME()[(K1_L_CB_OFFSET_L_1 + K1_P_CB_OFFSET_L_1 + (i))]
#endif

// determine memory destination for results
#if   P_CB_RES_DEST_LEVEL == PRIVATE
#define K1_P_CB_RES_DEST(i) K1_RES_P_BUFFER(i)
#elif P_CB_RES_DEST_LEVEL == LOCAL
#define K1_P_CB_RES_DEST(i) K1_RES_L_BUFFER(i)
#elif P_CB_RES_DEST_LEVEL == GLOBAL
#define K1_P_CB_RES_DEST(i) K1_RES_G_BUFFER(i)
#endif

#if   L_CB_RES_DEST_LEVEL == PRIVATE
#define K1_L_CB_RES_DEST(i) K1_RES_P_BUFFER(i)
#elif L_CB_RES_DEST_LEVEL == LOCAL
#define K1_L_CB_RES_DEST(i) K1_RES_L_BUFFER(i)
#elif L_CB_RES_DEST_LEVEL == GLOBAL
#define K1_L_CB_RES_DEST(i) K1_RES_G_BUFFER(i)
#endif

#if   G_CB_RES_DEST_LEVEL == PRIVATE
#define K1_G_CB_RES_DEST(i) K1_RES_P_BUFFER(i)
#elif G_CB_RES_DEST_LEVEL == LOCAL
#define K1_G_CB_RES_DEST(i) K1_RES_L_BUFFER(i)
#elif G_CB_RES_DEST_LEVEL == GLOBAL
#define K1_G_CB_RES_DEST(i) K1_RES_G_BUFFER(i)
#endif


// buffer abstraction for kernel_res buffer
#define K1_KERNEL_RES_BUFFER(i) error[(i)]

#define K1_G_KERNEL_RES(i) K1_KERNEL_RES_BUFFER(K1_G_CB_OFFSET_L_1 + (i))
#define K1_L_KERNEL_RES(i) K1_G_KERNEL_RES(K1_L_CB_OFFSET_L_1 + (i))
#define K1_P_KERNEL_RES(i) K1_L_KERNEL_RES(K1_P_CB_OFFSET_L_1 + (i))
// =============== end of macro definitions per buffer ========================

// =============== scalar function ============================================
inline TYPE_TS f(const TYPE_T cache_data_i_val, const TYPE_T cache_data_j_val, const TYPE_T coef_i_val, const TYPE_T coef_j_val, const TYPE_T error_val) {
  return error_val + coef_i_val * cache_data_i_val + coef_j_val * cache_data_j_val;
}
// =============== end of scalar function =====================================

// =============== kernel 1 ===================================================
__kernel void compute_error_update_1(const uint index_i, const uint index_j,
                                     const TYPE_T coef_i, const TYPE_T coef_j,
                                     const uint iUp, const uint iLow,
                                     const char listUp, const char listLow,
                                     __global TYPE_T const * const restrict cache_data,
                                     __global char * const restrict list,
                                     __global TYPE_T * const restrict error) {
  // set pointers
  __global TYPE_T const * const cache_data_i = cache_data + index_i * G_CB_SIZE_L_1;
  __global TYPE_T const * const cache_data_j = cache_data + index_j * G_CB_SIZE_L_1;

  __global TYPE_TS * const restrict res_g;

  // update list index for updating bias
  if(get_group_id(OCL_DIM_L_1) == 0 && get_local_id(OCL_DIM_L_1) == 0) {
    list[iUp] = listUp;
    list[iLow] = listLow;
  }

  // map md_hom dimensions to OpenCL dimensions
  const size_t i_wg_l_1 = get_group_id(OCL_DIM_L_1);
  const size_t i_wi_l_1 = get_local_id(OCL_DIM_L_1);

  // declare variables for caching inputs
  #if CACHE_L_CB != 0
  __local TYPE_T cb_l_cache_data_i[BUFFER_CACHE_DATA_I_L_SIZE_0];
  __local TYPE_T cb_l_cache_data_j[BUFFER_CACHE_DATA_J_L_SIZE_0];
  __local TYPE_T cb_l_error[BUFFER_ERROR_L_SIZE_0];
  #endif
  #if CACHE_P_CB != 0
  __private TYPE_T cb_p_cache_data_i[BUFFER_CACHE_DATA_I_P_SIZE_0];
  __private TYPE_T cb_p_cache_data_j[BUFFER_CACHE_DATA_J_P_SIZE_0];
  __private TYPE_T cb_p_error[BUFFER_ERROR_P_SIZE_0];
  #endif

  // declare variables for result memory
  // ------ LOCAL ------
  #ifdef K1_L_LEVEL_HAS_RESULTS
  __local TYPE_TS K1_RES_L_BUFFER_DEF;
  #endif
  // ------ PRIVATE ------
  #ifdef K1_P_LEVEL_HAS_RESULTS
  __private TYPE_TS K1_RES_P_BUFFER_DEF;
  #endif

  #if K1_L_NUM_CB_L_1 > 0
  for (size_t i_l_cb_l_1 = 0; i_l_cb_l_1 < K1_L_NUM_CB_L_1; ++i_l_cb_l_1) {
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if K1_L_CB_SIZE_CACHE_DATA_I / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_CACHE_DATA_I / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_I(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_I(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_CACHE_DATA_I % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_CACHE_DATA_I % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_CACHE_DATA_I / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_I(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_I(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_CACHE_DATA_J / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_CACHE_DATA_J / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_J(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_J(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_CACHE_DATA_J % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_CACHE_DATA_J % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_CACHE_DATA_J / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_J(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_J(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_ERROR(i_l_elem_l_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_ERROR(i_l_elem_l_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------

    #if K1_P_NUM_CB_L_1 > 0
    for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    } // end of "i_p_cb_l_1"-loop
    #endif
    // post process whole extra cache blocks in dimension L_1
    #if K1_P_NUM_EXTRA_CB_L_1 > 0
    if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    } // end of post processing whole extra cache blocks in dimension L_1
    #endif
    // post process single extra incomplete cache block in dimension L_1
    #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
    if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    }// end of post process single extra incomplete cache block in dimension L_1
    #endif

    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif

    // move results upwards in memory hierarchy if not already done by reduction
    #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
    #if K1_P_NUM_CB_L_1 > 0
    for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    } // end of "i_p_cb_l_1"-loop
    #endif
    // post process whole extra cache blocks in dimension L_1
    #if K1_P_NUM_EXTRA_CB_L_1 > 0
    if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    } // end of post processing whole extra cache blocks in dimension L_1
    #endif
    // post process single extra incomplete cache block in dimension L_1
    #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
    if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    }// end of post process single extra incomplete cache block in dimension L_1
    #endif
    #endif
  } // end of "i_l_cb_l_1"-loop
  #endif
  // post process whole extra cache blocks in dimension L_1
  #if K1_L_NUM_EXTRA_CB_L_1 > 0
  if (K1_G_FU_ID_L_1 < K1_L_NUM_EXTRA_CB_L_1) {
    const size_t i_l_cb_l_1 = K1_L_NUM_CB_L_1;
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if K1_L_CB_SIZE_CACHE_DATA_I / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_CACHE_DATA_I / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_I(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_I(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_CACHE_DATA_I % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_CACHE_DATA_I % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_CACHE_DATA_I / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_I(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_I(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_CACHE_DATA_J / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_CACHE_DATA_J / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_J(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_J(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_CACHE_DATA_J % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_CACHE_DATA_J % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_CACHE_DATA_J / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_J(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_J(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_ERROR(i_l_elem_l_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_ERROR(i_l_elem_l_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------

    #if K1_P_NUM_CB_L_1 > 0
    for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    } // end of "i_p_cb_l_1"-loop
    #endif
    // post process whole extra cache blocks in dimension L_1
    #if K1_P_NUM_EXTRA_CB_L_1 > 0
    if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    } // end of post processing whole extra cache blocks in dimension L_1
    #endif
    // post process single extra incomplete cache block in dimension L_1
    #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
    if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    }// end of post process single extra incomplete cache block in dimension L_1
    #endif

    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif

    // move results upwards in memory hierarchy if not already done by reduction
    #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
    #if K1_P_NUM_CB_L_1 > 0
    for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    } // end of "i_p_cb_l_1"-loop
    #endif
    // post process whole extra cache blocks in dimension L_1
    #if K1_P_NUM_EXTRA_CB_L_1 > 0
    if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    } // end of post processing whole extra cache blocks in dimension L_1
    #endif
    // post process single extra incomplete cache block in dimension L_1
    #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
    if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    }// end of post process single extra incomplete cache block in dimension L_1
    #endif
    #endif
  } // end of post processing whole extra cache blocks in dimension L_1
  #endif
  // post process single extra incomplete cache block in dimension L_1
  #if K1_L_NUM_EXTRA_ELEMS_L_1 > 0
  if (K1_G_FU_ID_L_1 == K1_L_NUM_EXTRA_CB_L_1 % K1_G_NUM_FU_L_1) {
    const size_t i_l_cb_l_1 = K1_L_NUM_CB_L_1;
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if (K1_L_CB_SIZE_CACHE_DATA_I / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < (K1_L_CB_SIZE_CACHE_DATA_I / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_I(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_I(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if (K1_L_CB_SIZE_CACHE_DATA_I / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_CACHE_DATA_I / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_CACHE_DATA_I / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_I(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_I(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if (K1_L_CB_SIZE_CACHE_DATA_J / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < (K1_L_CB_SIZE_CACHE_DATA_J / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_J(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_J(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if (K1_L_CB_SIZE_CACHE_DATA_J / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_CACHE_DATA_J / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_CACHE_DATA_J / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_CACHE_DATA_J(i_l_elem_l_1) = K1_G_MEM_CACHE_DATA_J(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_ERROR(i_l_elem_l_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    #if (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_l_1 = index;
      K1_L_MEM_ERROR(i_l_elem_l_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1);
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------

    #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
    for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1; ++i_p_cb_l_1) {
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    } // end of "i_p_cb_l_1"-loop
    #endif
    // post process whole extra cache blocks in dimension L_1
    #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
    if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_I % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_CACHE_DATA_J % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    } // end of post processing whole extra cache blocks in dimension L_1
    #endif
    // post process single extra incomplete cache block in dimension L_1
    #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
    if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 % K1_L_NUM_FU_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_CACHE_DATA_I / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_I(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_CACHE_DATA_J / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1) = K1_L_MEM_CACHE_DATA_J(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_L_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_l_1 = index;
        K1_P_MEM_ERROR(i_p_elem_l_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------

      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
        // process one mda element
        K1_P_CB_RES_DEST(i_p_elem_l_1) = f(
            K1_P_MEM_CACHE_DATA_I(i_p_elem_l_1),
            K1_P_MEM_CACHE_DATA_J(i_p_elem_l_1),
            coef_i,
            coef_j,
            K1_P_MEM_ERROR(i_p_elem_l_1)
        );

      }

      // move results upwards in memory hierarchy if not already done by reduction
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
        K1_L_CB_RES_DEST(i_p_elem_l_1) = K1_P_CB_RES_DEST(i_p_elem_l_1);
      }
      #endif
    }// end of post process single extra incomplete cache block in dimension L_1
    #endif

    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif

    // move results upwards in memory hierarchy if not already done by reduction
    #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
    #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
    for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1; ++i_p_cb_l_1) {
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    } // end of "i_p_cb_l_1"-loop
    #endif
    // post process whole extra cache blocks in dimension L_1
    #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
    if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    } // end of post processing whole extra cache blocks in dimension L_1
    #endif
    // post process single extra incomplete cache block in dimension L_1
    #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
    if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 % K1_L_NUM_FU_L_1) {
      const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
      for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
        K1_G_CB_RES_DEST(i_p_elem_l_1) = K1_L_CB_RES_DEST(i_p_elem_l_1);
      }
    }// end of post process single extra incomplete cache block in dimension L_1
    #endif
    #endif
  }// end of post process single extra incomplete cache block in dimension L_1
  #endif

//        barrier(CLK_GLOBAL_MEM_FENCE);
//               if(get_local_id(OCL_DIM_L_1) == 0 && get_group_id(OCL_DIM_L_1) == 0){
//                 for(int i = 1; i < 100; i++)
//                     printf("%f\n", error[i]);
//                 printf("\n");
//               }
}
// =============== end of kernel 1 ============================================