package opencl;

public class ParamsUtil {
    public static long getResGSizeKernelOne(int G_CB_SIZE_L1, int G_CB_SIZE_R1, int elemSize,
                                     int G_CB_RES_DEST_LEVEL, int L_CB_RES_DEST_LEVEL, int P_CB_RES_DEST_LEVEL,
                                     int NUM_WG_R_1, int NUM_WI_R_1) {
        if(L_CB_RES_DEST_LEVEL < 2 && P_CB_RES_DEST_LEVEL < 2) return 1;
        long size = G_CB_SIZE_L1 * G_CB_SIZE_R1;
        if(G_CB_RES_DEST_LEVEL == 2) size *= NUM_WG_R_1;
        if(L_CB_RES_DEST_LEVEL == 2) size *= NUM_WI_R_1;

        return size * elemSize;
    }

    public static long getResGSizeKernelOne(int G_CB_SIZE_L1, int G_CB_SIZE_L2, int G_CB_SIZE_R1, int elemSize,
                                            int G_CB_RES_DEST_LEVEL, int L_CB_RES_DEST_LEVEL, int P_CB_RES_DEST_LEVEL,
                                            int NUM_WG_R_1, int NUM_WI_R_1) {
        if(L_CB_RES_DEST_LEVEL < 2 && P_CB_RES_DEST_LEVEL < 2) return 1;
        long size = G_CB_SIZE_L1 * G_CB_SIZE_L2 * G_CB_SIZE_R1;
        if(G_CB_RES_DEST_LEVEL == 2) size *= NUM_WG_R_1;
        if(L_CB_RES_DEST_LEVEL == 2) size *= NUM_WI_R_1;

        return size * elemSize;
    }

    public static long getResGSizeKernelOne(int G_CB_SIZE_R1, int elemSize,
                                            int G_CB_RES_DEST_LEVEL, int L_CB_RES_DEST_LEVEL, int P_CB_RES_DEST_LEVEL,
                                            int NUM_WG_R_1, int NUM_WI_R_1) {
        if(L_CB_RES_DEST_LEVEL < 2 && P_CB_RES_DEST_LEVEL < 2) return 1;
        long size = G_CB_SIZE_R1;
        if(G_CB_RES_DEST_LEVEL == 2) size *= NUM_WG_R_1;
        if(L_CB_RES_DEST_LEVEL == 2) size *= NUM_WI_R_1;

        return size * elemSize;
    }

    public static int getLocalSizeSecondKernel(int P_CB_SIZE_R_1, int L_CB_SIZE_R_1, int G_CB_SIZE_R_1, int NUM_WG_R_1, int NUM_WI_R_1) {
        int min = (int) Math.min(L_CB_SIZE_R_1, (Math.min(NUM_WG_R_1, Math.ceil((float) G_CB_SIZE_R_1 / L_CB_SIZE_R_1))));
        return Math.min(NUM_WI_R_1, ceil(min, Math.min(P_CB_SIZE_R_1, min)));
    }

    private static int ceil(int x, int y){
        return (x + y - 1) / y;
    }
}
