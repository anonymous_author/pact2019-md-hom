package opencl;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.opencl.CL10;

import java.nio.*;
import java.util.LinkedList;
import java.util.List;

import static org.lwjgl.opencl.CLUtil.checkCLError;

/**
 * This class represents a kernel in OpenCL.
 *
 * @author
 */
public class Kernel {
    private IntBuffer errorCode;

    private final long clKernel;
    private final long program;
    private final String name;
    private List<KernelArg> args;

    private PointerBuffer localSize, globalSize;
    private int workDim;

    // Profiling
    private ByteBuffer timeStart, timeEnd;
    private PointerBuffer event;

    /**
     * @param program pointer to the program of the kernel
     * @param name name of the kernel function
     */
    public Kernel(long program, String name) {
        this.program = program;
        this.name = name;
        this.args = new LinkedList<>();
        this.globalSize = null;
        this.localSize = null;

        this.errorCode = BufferUtils.createIntBuffer(1);
        this.clKernel = CL10.clCreateKernel(this.program, this.name, this.errorCode);
        checkCLError(this.errorCode);

        if(PlatformUtil.PROFILING_ENABLED) {
            timeStart = BufferUtils.createByteBuffer(8);
            timeEnd = BufferUtils.createByteBuffer(8);
            event = BufferUtils.createPointerBuffer(1);
        }
    }

    public void initArgs(int size) {
        this.args = new LinkedList<>();
        for(int i = 0; i < size; i ++) args.add(new KernelArg());
    }

    public void resetArgs() {
        this.args = new LinkedList<>();
    }

    public void setInputArg(int index, Buffer buffer, TypeFlag type) {
        args.set(index, new KernelArg(buffer, index, RWFlag.READ_ONLY, type));
    }

    public void setScalarArg(int index, Buffer buffer) {
        args.set(index, new KernelArg(buffer, index, TypeFlag.WRITE_SCALAR));
    }

    public void setArg(int index, long pointer, TypeFlag type) {
        args.set(index, new KernelArg(index, RWFlag.READ_WRITE, type, pointer));
    }

    public void setArg(int index, long pointer, RWFlag rwtype, TypeFlag type) {
        args.set(index, new KernelArg(index, rwtype, type, pointer));
    }

    public void setInputOutputArg(int index, long size, TypeFlag type) {
        args.set(index, new KernelArg(size, index, RWFlag.READ_WRITE, type));
    }

    public long setIOArg(int index, long size, TypeFlag type) {
        KernelArg arg = new KernelArg(size, index, RWFlag.READ_WRITE, type);
        args.set(index, arg);
        return arg.getPointer();
    }

    public long setOutputArg(int index, Buffer buffer, TypeFlag type) {
        KernelArg arg = new KernelArg(buffer, index, RWFlag.WRITE_ONLY, type);
        args.set(index, arg);
        return arg.getPointer();
    }


    public void setGlobalSize(long size) {
        globalSize = BufferUtils.createPointerBuffer(1);
        globalSize.put(size);
        globalSize.flip();
    }

    public void setGlobalSize(long size_dim_0, long size_dim_1) {
        globalSize = BufferUtils.createPointerBuffer(2);
        globalSize.put(size_dim_0);
        globalSize.put(size_dim_1);
        globalSize.flip();
    }

    public void setGlobalSize(long size_dim_0, long size_dim_1, long size_dim_2) {
        globalSize = BufferUtils.createPointerBuffer(3);
        globalSize.put(size_dim_0);
        globalSize.put(size_dim_1);
        globalSize.put(size_dim_2);
        globalSize.flip();
    }

    public void setLocalSize(long size) {
        localSize = BufferUtils.createPointerBuffer(1);
        localSize.put(size);
        localSize.flip();
    }

    public void setLocalSize(long sizeDim0, long sizeDim1) {
        localSize = BufferUtils.createPointerBuffer(2);
        localSize.put(sizeDim0);
        localSize.put(sizeDim1);
        localSize.flip();
    }

    public void setLocalSize(long sizeDim0, long sizeDim1, long sizeDim2) {
        localSize = BufferUtils.createPointerBuffer(3);
        localSize.put(sizeDim0);
        localSize.put(sizeDim1);
        localSize.put(sizeDim2);
        localSize.flip();
    }

    public void setWorkDim(int workDim) {
        this.workDim = workDim;
    }

    public void executeKernel(){
        int ret = CL10.clEnqueueNDRangeKernel(PlatformUtil.getCommandQueue(), clKernel, workDim, null,
                globalSize, localSize, null, null);
        checkCLError(ret);
    }

    public long executeKernelProfile(){
        int ret = CL10.clEnqueueNDRangeKernel(PlatformUtil.getCommandQueue(), clKernel, workDim, null,
                globalSize, localSize, null, event);
        checkCLError(ret);

        ret = CL10.clWaitForEvents(event);
        checkCLError(ret);
        checkCLError(CL10.clGetEventProfilingInfo(event.get(0), CL10.CL_PROFILING_COMMAND_START, timeStart, null));
        checkCLError(CL10.clGetEventProfilingInfo(event.get(0), CL10.CL_PROFILING_COMMAND_END, timeEnd, null));

        long start = timeStart.getLong(0);
        long end = timeEnd.getLong(0);
        timeEnd.rewind();
        timeStart.rewind();

        OCLUtil.releaseMem(event.get(0));

        return (end - start);
    }

    public void setArgs(){
        int ret;
        for (KernelArg arg : args) {
            if (arg.getType() != TypeFlag.WRITE_SCALAR) {
                ret = CL10.clSetKernelArg1p(clKernel, arg.getIndex(), arg.getPointer());
                checkCLError(ret);
            } else {
                setScalarArg(arg);
            }
        }
    }

    public void setPointerArgs() {
        int ret;
        for (KernelArg arg : args) {
            if (arg.getType() != TypeFlag.WRITE_SCALAR) {
                ret = CL10.clSetKernelArg1p(clKernel, arg.getIndex(), arg.getPointer());
                checkCLError(ret);
            }
        }
    }

    public void setScalarArgs() {
        for (KernelArg arg : args) {
            if (arg.getType() == TypeFlag.WRITE_SCALAR) setScalarArg(arg);
        }
    }

    private void setScalarArg(KernelArg arg) {
        int ret = 0;
        if (arg.getBuffer() instanceof FloatBuffer)
            ret = CL10.clSetKernelArg(clKernel, arg.getIndex(), ((FloatBuffer) arg.getBuffer()));
        else if (arg.getBuffer() instanceof IntBuffer)
            ret = CL10.clSetKernelArg(clKernel, arg.getIndex(), ((IntBuffer) arg.getBuffer()));
        else if (arg.getBuffer() instanceof ByteBuffer)
            ret = CL10.clSetKernelArg(clKernel, arg.getIndex(), ((ByteBuffer) arg.getBuffer()));
        checkCLError(ret);
    }

    public void readArgs(){
        for (KernelArg arg : args) {
            if ((arg.getType() == TypeFlag.READ)) {
                enqueueRead(arg);
            }
        }
    }

    public long readArgsProfile(){
        long time = 0;
        for (KernelArg arg : args) {
            if ((arg.getType() == TypeFlag.READ)) {
                time += enqueueReadProfile(arg);
            }
        }
        return time;
    }

    private void enqueueRead(KernelArg arg) {
        int ret = 0;
        if (arg.getBuffer() instanceof ByteBuffer) {
            ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_READ, 0, ((ByteBuffer) arg.getBuffer()), null, null);
        } else if (arg.getBuffer() instanceof IntBuffer)
            ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_READ, 0, ((IntBuffer) arg.getBuffer()), null, null);
        else if (arg.getBuffer() instanceof FloatBuffer) {
            ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_READ, 0, ((FloatBuffer) arg.getBuffer()), null, null);
        }
        checkCLError(ret);
    }

    private long enqueueReadProfile(KernelArg arg) {
        int ret = 0;
        if (arg.getBuffer() instanceof FloatBuffer)
            ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_READ, 0, ((FloatBuffer) arg.getBuffer()), null, event);
        else if (arg.getBuffer() instanceof IntBuffer)
            ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_READ, 0, ((IntBuffer) arg.getBuffer()), null, event);
        else if (arg.getBuffer() instanceof ByteBuffer)
            ret = CL10.clEnqueueReadBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_READ, 0, ((ByteBuffer) arg.getBuffer()), null, event);
        checkCLError(ret);

        checkCLError(CL10.clWaitForEvents(event));
        checkCLError(CL10.clGetEventProfilingInfo(event.get(0), CL10.CL_PROFILING_COMMAND_START, timeStart, null));
        checkCLError(CL10.clGetEventProfilingInfo(event.get(0), CL10.CL_PROFILING_COMMAND_END, timeEnd, null));

        long start = timeStart.getLong(0);
        long end = timeEnd.getLong(0);
        timeEnd.rewind();
        timeStart.rewind();

        OCLUtil.releaseMem(event.get(0));

        return (end - start);
    }

    public void writeArgs(){
        for (KernelArg arg : args) {
            if (arg.getType() == TypeFlag.WRITE) enqueueWrite(arg);
        }
    }

    private void enqueueWrite(KernelArg arg) {
        int ret = 0;
        if (arg.getBuffer() instanceof FloatBuffer)
            ret = CL10.clEnqueueWriteBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_WRITE, 0, ((FloatBuffer) arg.getBuffer()), null, null);
        else if (arg.getBuffer() instanceof IntBuffer)
            ret = CL10.clEnqueueWriteBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_WRITE, 0, ((IntBuffer) arg.getBuffer()), null, null);
        else if (arg.getBuffer() instanceof ByteBuffer)
            ret = CL10.clEnqueueWriteBuffer(PlatformUtil.getCommandQueue(), arg.getPointer(), PlatformUtil.BLOCKING_OP_WRITE, 0, ((ByteBuffer) arg.getBuffer()), null, null);
        checkCLError(ret);
    }

    public void releaseArgs() {
        for (KernelArg arg : args) {
            if(arg.type != TypeFlag.SHARED && arg.getPointer() != 0L) CL10.clReleaseMemObject(arg.getPointer());
        }
    }

    public void releaseArg(int index) {
        CL10.clReleaseMemObject(args.get(index).pointer);
    }

    public void releaseKernel() {
        CL10.clReleaseKernel(clKernel);
        CL10.clReleaseProgram(program);
    }

    private class KernelArg {
        private Buffer buffer;
        private int index;
        private long pointer;
        private RWFlag flag;
        private TypeFlag type;

        KernelArg() {}

        KernelArg(Buffer buffer, int index, RWFlag flag, TypeFlag type) {
            init(buffer, index, flag, type);
            if (buffer instanceof FloatBuffer)
                this.pointer = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | PlatformUtil.memMode(), (FloatBuffer) buffer, Kernel.this.errorCode);
            else if (buffer instanceof IntBuffer)
                this.pointer = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | PlatformUtil.memMode(), (IntBuffer) buffer, Kernel.this.errorCode);
            else if (buffer instanceof ByteBuffer)
                this.pointer = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | PlatformUtil.memMode(), (ByteBuffer) buffer, Kernel.this.errorCode);
            else if (buffer instanceof ShortBuffer)
                this.pointer = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | PlatformUtil.memMode(), (ShortBuffer) buffer, Kernel.this.errorCode);
            checkCLError(Kernel.this.errorCode);
        }

        KernelArg(Buffer buffer, int index, TypeFlag type) {
            init(buffer, index, RWFlag.READ_ONLY, type);
        }

        KernelArg(long size, int index, RWFlag flag, TypeFlag type) {
            init(null, index, flag, type);
            this.pointer = CL10.clCreateBuffer(PlatformUtil.getContext(), flag.get() | OCLUtil.MemType.ALLOC.get(), size, errorCode);
            checkCLError(errorCode);
        }

        KernelArg(int index, RWFlag flag, TypeFlag type, long pointer) {
            init(buffer, index, flag, type);
            this.pointer = pointer;
        }

        private void init(Buffer buffer, int index, RWFlag flag, TypeFlag type) {
            this.buffer = buffer;
            this.index = index;
            this.flag = flag;
            this.type = type;
        }

        public Buffer getBuffer() {
            return buffer;
        }

        public int getIndex() {
            return index;
        }

        public long getPointer() {
            return pointer;
        }

        public RWFlag getFlag() {
            return flag;
        }

        public TypeFlag getType() {
            return type;
        }
    }

    public enum TypeFlag {
        WRITE(0),
        WRITE_SCALAR(1),
        READ(2),
        CACHE(3),
        SHARED(4);

        private final int flag;

        TypeFlag(int flag) {
            this.flag = flag;
        }

        public int get() {
            return this.flag;
        }
    }

    public enum RWFlag {
        READ_ONLY(CL10.CL_MEM_READ_ONLY),
        WRITE_ONLY(CL10.CL_MEM_WRITE_ONLY),
        READ_WRITE(CL10.CL_MEM_READ_WRITE);

        private final int flag;

        RWFlag(int flag) {
            this.flag = flag;
        }

        public int get() {
            return this.flag;
        }
    }
}
