package opencl;

import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.opencl.*;
import org.lwjgl.system.MemoryUtil;
import util.Config;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opencl.CLUtil.checkCLError;

public class PlatformUtil {
    public enum DeviceType {
        CPU(CL10.CL_DEVICE_TYPE_CPU),
        GPU(CL10.CL_DEVICE_TYPE_GPU),
        ACC(CL10.CL_DEVICE_TYPE_ACCELERATOR);

        private final int type;

        DeviceType(int type) { this.type = type; }

        public int get() {
            return this.type;
        }
    }

    public static String NATIVE_PATH = "native";

    private static boolean DEBUG = false;
    private static boolean DISPLAY_BUILD_LOG = false;
    public static boolean PROFILING_ENABLED = false;

    private static boolean initialized = false;
    private static IntBuffer errorCode;

    public static OCLUtil.MemType MEM_TYPE = OCLUtil.MemType.USE;
    public static DeviceType DEVICE_TYPE = Config.DEVICE_TYPE;

    public static int BLOCKING_OP_READ = CL10.CL_TRUE;
    public static int BLOCKING_OP_WRITE = CL10.CL_FALSE;

    private static CLPlatform platform;
    private static CLDevice device;
    private static long context;
    private static long commandQueue;

    private static int memMode = MEM_TYPE.get();

    private static CLProgramCallback cbProgram;
    private static CLContextCallback cbContext;

    private PlatformUtil() {
    }

    public static long getContext() {
        return context;
    }

    public static long getCommandQueue() {
        return commandQueue;
    }

    public static synchronized boolean isInitialized() {
        return initialized;
    }

    public static int memMode() {
        return memMode;
    }

    public static synchronized void requestInit(int platformId, int deviceId, DeviceType type) {
        init(type.get(), platformId, deviceId);
    }

    public static synchronized void requestShutdown() {
        if(isInitialized()) {
            if (DEBUG) System.out.println("Shutting down CL subsystem");

            CL10.clReleaseCommandQueue(commandQueue);
            CL12.clReleaseDevice(device.address());
            CL10.clReleaseContext(context);

            if (cbContext != null) cbContext.release();
            if (cbProgram != null) cbProgram.release();

            initialized = false;
        }
    }

    private static synchronized void init(int deviceType, int platformId, int deviceId) {
        if (!initialized) {
            String nativePath = "";
            String path = PlatformUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            try {
                String decodedPath = URLDecoder.decode(path, "UTF-8");
                String jarName = new java.io.File(PlatformUtil.class.getProtectionDomain()
                        .getCodeSource()
                        .getLocation()
                        .getPath())
                        .getName();
                nativePath = decodedPath.replace(jarName, NATIVE_PATH);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(PlatformUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (DEBUG) System.out.println("Loading native LWJGL files from: " + nativePath);
            System.setProperty("org.lwjgl.librarypath", nativePath);

            errorCode = BufferUtils.createIntBuffer(1);

            // get the platform
            platform = CLPlatform.getPlatforms().get(platformId);
            if (DEBUG) PlatformUtil.printPlatformInfo(platform);

            // get the gpu
            device = platform.getDevices(deviceType).get(deviceId);
            if (DEBUG) PlatformUtil.printDeviceInfo(device);

            //create context
            PointerBuffer ctxProps = BufferUtils.createPointerBuffer(3);
            ctxProps.put(CL10.CL_CONTEXT_PLATFORM).put(platform).put(0).flip();
            context = CL10.clCreateContext(ctxProps, device.address(), cbContext = new CLContextCallback() {
                @Override
                public void invoke(long errinfo, long private_info, long cb, long user_data) {
                    System.err.println("[LWJGL] cl_create_context_callback");
                    System.err.println("\tInfo: " + MemoryUtil.memDecodeUTF8(errinfo));
                }
            }, 0, errorCode);
            checkCLError(errorCode);

            //create queue
            if(PROFILING_ENABLED)
                commandQueue = CL10.clCreateCommandQueue(context, device.address(), CL10.CL_QUEUE_PROFILING_ENABLE, errorCode);
            else
                commandQueue = CL10.clCreateCommandQueue(context, device.address(), 0, errorCode);
            checkCLError(errorCode);

            initialized = true;
        }
    }

    public static synchronized long buildProgram(String path, String options, boolean useCache) {
        if(useCache) {
            return PlatformUtil.buildWithCache(path, options);
        } else {
            return PlatformUtil.buildProgram(path, options);
        }
    }

    public static synchronized long buildProgram(String path, String options) {
        long program = PlatformUtil.createProgram(readKernelSource(path));
        PlatformUtil.buildProgram(program, options);
        return program;
    }

    // TODO: change to path hashing for more performance at end
    private static synchronized long buildWithCache(String path, String options) {
        String hash = getHash(options + path);
        File file = new File("./cache/" + hash);
        long program = 0L;

        if(file.isFile()) {
            program = PlatformUtil.buildProgramFromBinary(options, "./cache/" + hash);
        }
        if(program == 0L) {
            String src = readKernelSource(path);
            program = PlatformUtil.createProgram(src);
            PlatformUtil.buildProgram(program, options);
            PlatformUtil.storeProgramBinary(program, "./cache/" + hash);
        }

        return program;
    }

    private static String getHash(String str) {
        try {
            return DatatypeConverter.printHexBinary(
                    MessageDigest.getInstance("MD5").digest(str.getBytes("UTF-8")));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            return null;
        }
    }

    private static synchronized long createProgram(String source) {
        long program = CL10.clCreateProgramWithSource(context, source, errorCode);
        checkCLError(errorCode);
        return program;
    }

    private static synchronized long buildProgramFromBinary(String options, String binary) {
        ByteBuffer binaryBuffer;
        long binarySize;
        try {
            RandomAccessFile aFile = new RandomAccessFile(binary,"r");
            FileChannel inChannel = aFile.getChannel();
            binarySize = inChannel.size();
            binaryBuffer = ByteBuffer.allocateDirect((int) binarySize);

            inChannel.read(binaryBuffer);
            binaryBuffer.rewind();

            inChannel.close();
            aFile.close();
        } catch (IOException e) {
            e.printStackTrace();
            return 0L;
        }

        PointerBuffer deviceList = PointerBuffer.allocateDirect(1);
        deviceList.put(device.address()).rewind();

        ByteBuffer[] binaries = new ByteBuffer[1];
        binaries[0] = binaryBuffer;

        IntBuffer errBuf = BufferUtils.createIntBuffer(1);
        long program = CL10.clCreateProgramWithBinary(context, deviceList, binaries, null, errBuf);
        try {
            checkCLError(errBuf);
        } catch (Exception e) {
            return 0L;
        }

        if(program == 0L) {
            return 0L;
        } else {
            PlatformUtil.buildProgram(program, options);
        }

        return program;
    }

    private static synchronized void buildProgram(long program, String options) {
        //CountDownLatch latch = new CountDownLatch(1);

        options = options + " -cl-fast-relaxed-math";
        int ret = CL10.clBuildProgram(program, device.address(), options, cbProgram = new CLProgramCallback() {
            @Override
            public void invoke(long program, long user_data) {
                if (DISPLAY_BUILD_LOG) {
                    System.err.printf(
                            "The cl_program [0x%X] was built %s\n",
                            program,
                            Info.clGetProgramBuildInfoInt(program, device.address(), CL10.CL_PROGRAM_BUILD_STATUS) == CL10.CL_SUCCESS ? "successfully" : "unsuccessfully"
                    );
                    String log = Info.clGetProgramBuildInfoStringASCII(program, device.address(), CL10.CL_PROGRAM_BUILD_LOG);
                    if (!log.isEmpty()) {
                        System.err.printf("BUILD LOG:\n----\n%s\n-----\n", log);
                    }
                }
                //latch.countDown();
            }
        }, 0);
        checkCLError(ret);

//        try {
//            latch.await();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    private static void storeProgramBinary(long program, String kernelName) {
        int ret;

        try {
            // get count of devices
            int numDevices = Info.clGetProgramInfoInt(program, CL10.CL_PROGRAM_NUM_DEVICES);

            // get all devices
            PointerBuffer devices = PointerBuffer.allocateDirect(numDevices);
            ret = CL10.clGetProgramInfo(program, CL10.CL_PROGRAM_DEVICES, devices, null);
            checkCLError(ret);

            // get index of current device
            int index = -1;
            for (int i = 0; i < numDevices; ++i) {
                if (devices.get(i) == device.address()) {
                    index = i;
                }
            }
            if (index == -1) return;

            // get size of binary
            PointerBuffer sizes = PointerBuffer.allocateDirect(numDevices);
            ret = CL10.clGetProgramInfo(program, CL10.CL_PROGRAM_BINARY_SIZES, sizes, null);
            checkCLError(ret);
            int size = (int) sizes.get(index);

            // set binary pointers
            PointerBuffer binaryPointers = PointerBuffer.allocateDirect(numDevices);
            for (int i = 0; i < binaryPointers.capacity(); ++i) {
                binaryPointers.put(0L);
            }
            binaryPointers.rewind();
            ByteBuffer binaries = ByteBuffer.allocateDirect(size);
            binaryPointers.put(index, binaries);

            // get binary file
            ret = CL10.clGetProgramInfo(program, CL10.CL_PROGRAM_BINARIES, binaryPointers, null);
            checkCLError(ret);

            File file = new File(kernelName);
            if(file.getParentFile() != null)
                file.getParentFile().mkdirs();

            FileChannel channel = new FileOutputStream(file, false).getChannel();
            channel.write(binaries);
            channel.close();
        } catch (Exception e) {
            System.out.println("Could not store kernel binary.");
        }
    }

    private static synchronized void printPlatformInfo(CLPlatform p) {
        System.out.println("\t" + "platform info");
        printPlatformInfo(p, "CL_PLATFORM_PROFILE", CL10.CL_PLATFORM_PROFILE);
        printPlatformInfo(p, "CL_PLATFORM_VERSION", CL10.CL_PLATFORM_VERSION);
        printPlatformInfo(p, "CL_PLATFORM_NAME", CL10.CL_PLATFORM_NAME);
        printPlatformInfo(p, "CL_PLATFORM_VENDOR", CL10.CL_PLATFORM_VENDOR);
    }

    private static synchronized void printPlatformInfo(CLPlatform p, String param_name, int param) {
        System.out.println("\t" + param_name + " = " + Info.clGetPlatformInfoStringUTF8(p.address(), param));
    }

    private static synchronized void printDeviceInfo(CLDevice d) {
        System.out.println("\t" + "device info");

        printDeviceInfo(d, "CL_DEVICE_NAME", CL10.CL_DEVICE_NAME);
        printDeviceInfo(d, "CL_DEVICE_VENDOR", CL10.CL_DEVICE_VENDOR);
        printDeviceInfo(d, "CL_DRIVER_VERSION", CL10.CL_DRIVER_VERSION);
        printDeviceInfo(d, "CL_DEVICE_PROFILE", CL10.CL_DEVICE_PROFILE);
        printDeviceInfo(d, "CL_DEVICE_VERSION", CL10.CL_DEVICE_VERSION);
    }

    private static synchronized void printDeviceInfo(CLDevice d, String param_name, int param) {
        System.out.println("\t" + param_name + " = " + Info.clGetDeviceInfoStringUTF8(d.address(), param));
    }

    private static String readKernelSource(String path) {
//        InputStream kernelPath = Training.class.getClassLoader().getResourceAsStream("classifier/" + path + ".cl");
//        if(DEBUG) System.out.println("Loading ocl_kernel: " + Training.class.getClassLoader().getResource("classifier/" + path + ".cl"));
//        Scanner scanner = new Scanner(kernelPath);
//
//        StringBuilder kernelSource = new StringBuilder();
//        while (scanner.hasNext()) {
//            kernelSource.append(scanner.nextLine()).append("\n");
//        }

        StringBuilder kernelSource = new StringBuilder();
        InputStream pathFile = PlatformUtil.class.getClassLoader().getResourceAsStream("opencl/" + path + ".cl");
        if(DEBUG) System.out.println("Loading kernel: " + pathFile);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(pathFile, Charset.defaultCharset()))){
            String line;
            while ((line = reader.readLine()) != null) {
                kernelSource.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return kernelSource.toString();
    }
}
