import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.Evaluation;
import mulan.evaluation.Evaluator;
import mulan.evaluation.measure.HammingLoss;
import mulan.evaluation.measure.Measure;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Option.Builder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import util.CliUtil;
import weka.classifiers.functions.MultilayerPerceptron;

public class MainMulanMLP
{
    private static final String JAR_NAME = "mulan-benchmark.jar";

    public static void main(String[] args) throws Exception {

        // Input and Output options
        Options options = new Options();
        options.addOption(Option.builder("train").hasArg().desc("training data file (required)").required().build());
        options.addOption(Option.builder("test").hasArg().desc("test data file (required)").required().build());
        options.addOption(Option.builder("l").longOpt("label").hasArg().desc("number of labels (required)").required().build());

        // collect all options
        options.addOption(Option.builder("layer").hasArg().desc("hidden layers: \"n1,n2,..\" (default: \"a\")").build());
        options.addOption(Option.builder("r").hasArg().desc("learning rate (default: 0.3)").build());
        options.addOption(Option.builder("m").hasArg().desc("momentum (default: 0.2)").build());
        options.addOption(Option.builder("e").hasArg().desc("epoch (default: 50)").build());
        options.addOption(Option.builder("s").hasArg().desc("seed (default: 0").build());
        options.addOption(Option.builder("n").hasArg().desc("chain count (default: 10)").build());
        options.addOption(Option.builder("b").hasArg().desc("bag size (default: 100)").build());

        for(Option option : options.getOptions()) options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.setOptionComparator(new MainMulanMLP.OptionComparator<>());
            formatter.printHelp("java -jar mulan-benchmark.jar [options] -train training_file -test test_file -l label_count", options);
            System.err.println("Error: " + e.getMessage()); return;
        }

        String input = cmd.getOptionValue("train");
        String testStr = cmd.getOptionValue("test");
        int labelCount = Integer.parseInt(cmd.getOptionValue("l"));

        MultiLabelInstances trainSet = new MultiLabelInstances(input, labelCount);
        MultiLabelInstances testSet = new MultiLabelInstances(testStr, labelCount);

        String layers = CliUtil.parseString("layer", cmd, "a");
        float learningRate = CliUtil.parseFloat("r", cmd, 0.3F);
        float momentum = CliUtil.parseFloat("m", cmd, 0.2F);
        int epoch = CliUtil.parseInt("e", cmd, 50);
        int seed = CliUtil.parseInt("s", cmd, 0);

        int chainCount = CliUtil.parseInt("n", cmd, 10);
        int subsetPercentage = CliUtil.parseInt("b", cmd, 100);

        MultilayerPerceptron mlp = new MultilayerPerceptron();
        mlp.setLearningRate(learningRate);
        mlp.setMomentum(momentum);
        mlp.setTrainingTime(epoch);
        mlp.setHiddenLayers(layers);
        mlp.setSeed(seed);

        EnsembleOfClassifierChains ecc = new EnsembleOfClassifierChains(mlp, chainCount, false, false);
        ecc.setSamplingPercentage(subsetPercentage);
        ecc.setBagSizePercent(subsetPercentage);

        long start = System.currentTimeMillis();
        ecc.build(trainSet);
        long end = System.currentTimeMillis();
        System.out.println("Training: " + (end - start) + " ms");

        Evaluator eval = new Evaluator();
        List<Measure> measures = new ArrayList<>();
        measures.add(new HammingLoss());

        start = System.currentTimeMillis();
        Evaluation evaluation = eval.evaluate(ecc, testSet, measures);
        end = System.currentTimeMillis();
        System.out.println("Testing: " + (end - start) + " ms");

        measures = evaluation.getMeasures();
        System.out.println("Hamming Score: " + new DecimalFormat("#.###").format(1 - measures.get(0).getValue()));
    }

    static class OptionComparator<T extends Option> implements Comparator<T> {
        private static final String OPTS_ORDER = "tlcgenb";

        public int compare(T o1, T o2)
        {
            return "tlcgenb".indexOf(o1.getOpt()) - "tlcgenb".indexOf(o2.getOpt());
        }
    }
}
