package model;

import weka.core.Instance;
import weka.core.Instances;

/**
 * This class represents input data of a single-label classifier.
 *
 * @author
 */
public class InputData {
    private int numAttributes;
    private int numLabels;
    private int numInstances;
    private float[][] instances;

    public InputData() {}

    public InputData(Instances data, int numAttributes, int numLabels) {
        this.numAttributes = numAttributes;
        this.numLabels = numLabels;
        numInstances = data.numInstances();
        instances = new float[numInstances][numAttributes + numLabels];

        for (int i = 0; i < numInstances; ++i) {
            Instance instance = data.get(i);
            for (int j = 0; j < numLabels + numAttributes; ++j) {
                if(j >= numAttributes) {
                    if (instance.value(j) == 0) instances[i][j] = -1;
                    else instances[i][j] = (float)instance.value(j);
                } else {
                    instances[i][j] = (float)instance.value(j);
                }
            }
        }
    }

    public InputData(float[][] input, int numAttributes, int numLabels, int numInstances){
        this.instances = input;
        this.numAttributes = numAttributes;
        this.numLabels = numLabels;
        this.numInstances = numInstances;
    }

    public float getAttribute(int instanceId, int indexAttr){
        return instances[instanceId][indexAttr];
    }

    public void setAttribute(int instanceId, int indexAttr, float newVal){
        instances[instanceId][indexAttr] = newVal;
    }

    public float getLabel(int instanceId, int indexLabel){
        return instances[instanceId][numAttributes + indexLabel];
    }

    public float getLabelAsAttr(int instanceId, int indexLabel){
        float r = instances[instanceId][numAttributes + indexLabel];
        return r == -1 ? 0 : r;
    }

    public int getNumAttributes() {
        return numAttributes;
    }

    public int getNumLabels() {
        return numLabels;
    }

    public int getNumInstances() {
        return numInstances;
    }

    public int length() {
        return numInstances;
    }

    public float[] getInstance(int index) {
        return instances[index];
    }

    public float[][] getInstances() {
        return instances;
    }
}
