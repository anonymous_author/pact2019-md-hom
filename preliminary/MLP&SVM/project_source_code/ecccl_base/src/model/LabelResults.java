package model;

public class LabelResults {
    private float epsilon;

    private int numInstances;
    private int numLabel;
    private float[][] results;

    public LabelResults(int numInstances, int numLabel) {
        this.results = new float[numInstances][numLabel];
        this.numInstances = numInstances;
        this.numLabel = numLabel;
    }

    public LabelResults(EccLabelResults eccResults, int numInstances, int numLabel) {
        this.results = eccResults.reduce();
        this.numInstances = numInstances;
        this.numLabel = numLabel;
        this.epsilon = 0.000001f;
    }

    public float get(int instanceId, int labelId) {
        return results[instanceId][labelId];
    }

    public void set(int instanceId, int labelId, float value) {
        results[instanceId][labelId] = value;
    }

    public void addResult(int label, int instance, float value) {
        results[instance][label] += value;
    }

    public void threshold(int chainCount, float threshold) {
        for(int i = 0; i < numInstances; i++) {
            for(int j = 0; j < numLabel; j++) {
                results[i][j] = (results[i][j] * 1.0f / chainCount) >= threshold + epsilon ? 1 : 0;
            }
        }
    }

    public float[] getResults(int label) {
        return results[label];
    }
}
