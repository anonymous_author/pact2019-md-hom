package model;

import model.InputData;

/**
 * This class represents data that is used in the classifier chain
 * in the training task.
 */
public class ChainData {
    private float[][] attributes;
    private float[][] label;

    private int labelId;
    private int[] labelOrder;
    private int labelAsAttrCount;

    private int initNumAttributes;
    private int numAttributes;
    private int numInstances;

    /**
     * Converts the input to data that is handled in the classifier chain.
     *
     * @param input data set
     * @param labelOrder order of labels in chain
     */
    public ChainData(InputData input, int[] labelOrder) {
        this.initNumAttributes = input.getNumAttributes();
        this.numAttributes = input.getNumAttributes();
        this.numInstances = input.getNumInstances();

        this.labelOrder = labelOrder;
        this.labelId = 0;
        this.labelAsAttrCount = 0;

        attributes = new float[numInstances][numAttributes];
        label = new float[numInstances][input.getNumLabels()];

        // convert data to order of the labels in current chain
        for(int i = 0; i < input.getNumInstances(); i++) {
            System.arraycopy(input.getInstance(i), 0, attributes[i], 0, numAttributes);

            for (int j = 0; j < input.getNumLabels(); j++) {
                float value = input.getLabel(i, labelOrder[j]);
                label[i][j] = value == -1 ? 0 : value;
            }
        }
    }

    public int length() {
        return numInstances;
    }

    public int getInitNumAttributes() {
        return initNumAttributes;
    }

    public int getNumAttributes() {
        return numAttributes;
    }

    public float[] getAttributes(int instanceId) {
        return attributes[instanceId];
    }

    public float getCurrentLabel(int instanceId) {
        float ret = label[instanceId][labelId];
        return ret == 0 ? -1 : ret;
    }

    public int getCurrentLabelIndex() {
        return labelOrder[labelId];
    }

    public float[] getLabel(int instanceId) {
        return label[instanceId];
    }

    public int getLabelAsAttrCount() {
        return labelAsAttrCount;
    }

    public void addNextLabel() {
        this.labelId++;
        this.numAttributes++;
        this.labelAsAttrCount++;
    }
}
