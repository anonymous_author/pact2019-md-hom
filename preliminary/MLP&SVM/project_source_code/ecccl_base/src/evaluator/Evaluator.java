package evaluator;

import model.InputData;
import model.LabelResults;
import util.Config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class Evaluator {

    public static float evaluate(InputData test, LabelResults results, String outputPath) {
        if(Config.PRINT_LEVEL > 0) System.out.println("\nEvaluation: ");

        int numLabels = test.getNumLabels();
        int numInstances = test.getNumInstances();
        float hammingScore = 0;

        // get accuracy of each label
        for(int labelId = 0; labelId < numLabels; labelId++) {

            // get number of correct classified instances
            int correct = getCorrectClassified(test, results, numInstances, labelId);

            // calculate accuracy and add to hamming score
            float accuracy = correct * 1.0f / numInstances;
            hammingScore += accuracy / numLabels;

            if(Config.PRINT_LEVEL > 0) System.out.printf("Label %d: %.3f (%d/%d) %n", labelId, accuracy, correct, numInstances);
        }

        System.out.println("\nHamming Score: " + String.format("%.3f", hammingScore));
        if(Config.PRINT_LEVEL > 0) System.out.println("\nStoring outputs to: " + outputPath);

        // store outputs to files
        storeOutputs(results, outputPath, numLabels, numInstances);

        return hammingScore;
    }

    /**
     * Stores files with the resulting predicted classes for every instance.
     *
     * @param results predicted classes for labels
     * @param outputPath path of output files
     * @param numLabels number of labels
     * @param numInstances number of instances
     */
    private static void storeOutputs(LabelResults results, String outputPath, int numLabels, int numInstances) {
        try {
            for (int i = 0; i < numLabels; i++) {
                File outFile = new File(outputPath + i);
                if(outFile.getParentFile() != null)
                    outFile.getParentFile().mkdirs();

                PrintWriter writer = new PrintWriter(outputPath + i, "UTF-8");
                for(int i1 = 0; i1 < numInstances; i1++) {
                    writer.println(results.get(i1, i));
                }
                writer.close ();
            }
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            System.err.print("Error storing outputs: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static int getCorrectClassified(InputData test, LabelResults results, int numInstances, int labelId) {
        int correct = 0;
        for(int j = 0; j < numInstances; j++) {
            float testLabel = test.getLabel(j, labelId);
            float resultLabel = results.get(j, labelId);
            if((results.get(j, labelId) == 0 && test.getLabel(j, labelId) == -1) ||
                    (results.get(j, labelId) == 1 && test.getLabel(j, labelId) == 1)) {
                correct++;
            }
        }
        return correct;
    }

}
