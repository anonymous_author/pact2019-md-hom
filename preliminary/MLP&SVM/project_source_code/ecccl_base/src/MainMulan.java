import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.Evaluation;
import mulan.evaluation.Evaluator;
import mulan.evaluation.measure.HammingLoss;
import mulan.evaluation.measure.Measure;
import org.apache.commons.cli.*;
import util.CliUtil;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.RBFKernel;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class MainMulan {
    private static final String JAR_NAME = "mulan-benchmark.jar";

    public static void main(String[] args) throws Exception {

        // Input and Output options
        Options options = new Options();
        options.addOption(Option.builder("train").hasArg().desc("training data file (required)").required().build());
        options.addOption(Option.builder("test").hasArg().desc("test data file (required)").required().build());
        options.addOption(Option.builder("l").longOpt("label").hasArg().desc("number of labels (required)").required().build());

        options.addOption(Option.builder("c").desc("c (default: 1)").build());
        options.addOption(Option.builder("g").desc("gamma (default: 0.01)").build());
        options.addOption(Option.builder("e").desc("tolerance (default: 0.001)").build());
        options.addOption(Option.builder("n").desc("chain count (default: 10)").build());
        options.addOption(Option.builder("b").desc("bag size (default: 100)").build());

        // collect all options
        for(Option option : options.getOptions()) options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.setOptionComparator(new OptionComparator<>());
            formatter.printHelp( "java -jar " + JAR_NAME + " [options] -train training_file -test test_file -l label_count", options);
            System.err.println("Error: " + e.getMessage());
            return;
        }

        // parse i/o
        String input = cmd.getOptionValue("train");
        String testStr = cmd.getOptionValue("test");
        int labelCount = Integer.parseInt(cmd.getOptionValue("l"));


        MultiLabelInstances trainSet = new MultiLabelInstances(input, labelCount);
        MultiLabelInstances testSet = new MultiLabelInstances(testStr, labelCount);

        // Parameter
        float C = CliUtil.parseFloat("c", cmd, 1);
        float tol = CliUtil.parseFloat("e", cmd, 0.001f);
        float gamma = CliUtil.parseFloat("g", cmd, 0.01f);

        int chainCount = CliUtil.parseInt("n", cmd, 10);
        int subsetPercentage = CliUtil.parseInt("b", cmd, 100);

        SMO smo = new SMO();
        smo.setC(C);
        smo.setToleranceParameter(tol);
        RBFKernel smoKernel = new RBFKernel();
        smoKernel.setCacheSize(0);
        smoKernel.setGamma(gamma);
        smo.setKernel(smoKernel);
        EnsembleOfClassifierChains ecc = new EnsembleOfClassifierChains(smo,  chainCount, false, false);
        ecc.setSamplingPercentage(subsetPercentage);
        ecc.setBagSizePercent(subsetPercentage);

        long start = System.currentTimeMillis();
        ecc.build(trainSet);
        long end = System.currentTimeMillis();
        System.out.println("Training: " + (end - start) + " ms");

        Evaluator eval = new Evaluator();
        List<Measure> measures = new ArrayList<>();
        measures.add(new HammingLoss());

        start = System.currentTimeMillis();
        Evaluation evaluation = eval.evaluate(ecc, testSet, measures);
        end = System.currentTimeMillis();
        System.out.println("Testing: " + (end - start) + " ms");

        measures = evaluation.getMeasures();
        System.out.println("Hamming Score: " + new DecimalFormat("#.###").format(1 - measures.get(0).getValue()));
    }

    static class OptionComparator<T extends Option> implements Comparator<T> {
        private static final String OPTS_ORDER = "tlcgenb"; // short option names

        public int compare(T o1, T o2) {
            return OPTS_ORDER.indexOf(o1.getOpt()) - OPTS_ORDER.indexOf(o2.getOpt());
        }
    }
}
