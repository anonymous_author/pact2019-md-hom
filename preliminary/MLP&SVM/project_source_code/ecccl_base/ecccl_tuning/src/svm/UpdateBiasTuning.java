package svm;

import com.ecccl.svm.executor.training.UpdateBiasExecutor;
import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params.ParamsR1;
import util.JSONUtil;

public class UpdateBiasTuning {
    private final static int WARM_UP_RUNS = 3;
    private final static int RUNS = 5;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // read Args
        int numInstances = Integer.parseInt(args[3]);
        String tpsJSON = args[4];

        try {
            // init TPs
            ParamsR1 params = tpsJSON.equals("") ? new ParamsR1("/tuning_results/svm/update_bias_kernel_tp.json") :
                    new ParamsR1(JSONUtil.readArgs(tpsJSON));

            // init memory on device
            long list = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances);
            long error = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances);

            // init executor
            UpdateBiasExecutor exec = UpdateBiasExecutor.getInstance(params, numInstances, list, error);

            // warm up
            for(int i = 0; i < WARM_UP_RUNS; i++) exec.profile();

            // profile
            long runtime = 0;
            for(int i = 0; i < RUNS; i++) runtime += exec.profile();
            runtime /= RUNS;

            System.out.println(runtime);
        } catch (Exception e) {
            err = true;
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }
}
