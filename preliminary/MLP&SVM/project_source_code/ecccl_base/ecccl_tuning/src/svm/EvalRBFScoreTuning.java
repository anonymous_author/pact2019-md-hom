package svm;

import com.ecccl.svm.executor.classifier.EvalScoreRBFExecutor;
import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params.ParamsL1R1;
import util.JSONUtil;

public class EvalRBFScoreTuning {
    private final static int WARM_UP_RUNS = 3;
    private final static int RUNS = 5;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // read Args
        int numSV = Integer.parseInt(args[3]);
        int numRange = Integer.parseInt(args[4]);
        String tpsJSON = args[5];

        try {
            // init TPs
            ParamsL1R1 paramsScore = tpsJSON.equals("") ? new ParamsL1R1("/tuning_results/svm/eval_rbf_score_tp.json") :
                    new ParamsL1R1(JSONUtil.readArgs(tpsJSON));

            // init memory on device
            long result = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numRange * numSV * 4);
            long alpha = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, numSV * 4);
            long label = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, numSV * 4);

            // create Executor
            EvalScoreRBFExecutor exec = EvalScoreRBFExecutor.getInstance(paramsScore, numRange);
            exec.updateArgs(numSV,0.01f, result, alpha, label);

            // warm up
            for(int i = 0; i < WARM_UP_RUNS; i++) exec.profile();

            // profile
            long runtime = 0;
            for(int i = 0; i < RUNS; i++) runtime += exec.profile();
            runtime /= RUNS;

            // release memory
            OCLUtil.releaseMem(result);
            OCLUtil.releaseMem(alpha);
            OCLUtil.releaseMem(label);

            System.out.println(runtime);
        } catch (Exception e) {
            err = true;
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }
}
