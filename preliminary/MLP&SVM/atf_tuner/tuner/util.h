//
// Created by  on 3/25/18.
//

#ifndef ATF_UTIL_H
#define ATF_UTIL_H

static void _mkdir(const char *dir) {
    char tmp[256];
    char *p = NULL;
    size_t len;

    snprintf(tmp, sizeof(tmp),"%s",dir);
    len = strlen(tmp);
    if(tmp[len - 1] == '/')
        tmp[len - 1] = 0;
    for(p = tmp + 1; *p; p++)
        if(*p == '/') {
            *p = 0;
            mkdir(tmp, S_IRWXU);
            *p = '/';
        }
    mkdir(tmp, S_IRWXU);
}

static void parse_layers(std::vector<int> *layers, std::string layersStr) {
    std::stringstream ss(layersStr);
    while(ss.good()) {
        std::string substr;
        getline(ss, substr, ';');
        layers->push_back( atoi(substr.c_str()));
    }
}

void get_ocl_device_max_local(const cl::Device &device, cl_ulong *max_local_mem) {
    if (max_local_mem != nullptr) {
        device.getInfo(CL_DEVICE_LOCAL_MEM_SIZE, max_local_mem);
    }
}

void get_ocl_device_max_mem_alloc(const cl::Device &device, cl_ulong *max_mem_alloc) {
    if (max_mem_alloc != nullptr) {
        device.getInfo(CL_DEVICE_MAX_MEM_ALLOC_SIZE, max_mem_alloc);
    }
}

void get_ocl_device_limits(const cl::Device &device, std::vector<size_t> *max_num_wi, size_t *max_wg_size) {
    if (max_num_wi != nullptr) {
        cl_uint dims;
        //check_error_fatal(device.getInfo(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, &dims));
        device.getInfo(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, &dims);
        *max_num_wi = std::vector<size_t>(dims);
        //check_error_fatal(device.getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, max_num_wi));
        device.getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, max_num_wi);
    }
    if (max_wg_size != nullptr) {
        //check_error_fatal(device.getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, max_wg_size));
        device.getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, max_wg_size);
    }
}

int getDevice(cl::Device &device) {
    // get platform
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    if (platforms.size() == 0) {
        std::cout << "Platform size 0\n";
        return -1;
    }

    // get context with gpu device
    cl_context_properties properties[] =
            {CL_CONTEXT_PLATFORM, (cl_context_properties) (platforms[0])(), 0};
    cl::Context context(CL_DEVICE_TYPE_GPU, properties);

    // get first gpu device
    std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
    device = devices[0];
}

int getDevice(cl::Device &device, int platformId, int deviceId, std::string deviceType) {
    // get platform
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    if (platforms.size() == 0) {
        std::cout << "Platform size 0\n";
        return -1;
    }

    // get context with gpu device
    cl_context_properties properties[] =
            {CL_CONTEXT_PLATFORM, (cl_context_properties) (platforms[platformId])(), 0};

    cl_device_type type;
    if (deviceType == "cpu" || deviceType == "CPU") {
        type = CL_DEVICE_TYPE_CPU;
    } else if (deviceType == "acc" || deviceType == "ACC" || deviceType == "accelerator" || deviceType == "ACCELERATOR") {
        type = CL_DEVICE_TYPE_GPU;
    } else if (deviceType == "gpu" || deviceType == "GPU"){
        type = CL_DEVICE_TYPE_GPU;
    } else {
        throw std::invalid_argument("Error: Could not recognize device type");
    }

    cl::Context context(type, properties);

    // get first gpu device
    std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
    device = devices[deviceId];
}

#endif //ATF_UTIL_H
