#include "../../../atf.h"
#include "../../util.h"

#ifndef ATF_TUNING_KERNEL_HPP
#define ATF_TUNING_KERNEL_HPP

int tuning_kernel(std::string bash, std::string result, int argc, char* argv[]) {
    int platformId = atoi(argv[2]);
    int deviceId = atoi(argv[3]);
    std::string deviceType = argv[4];

    int threshold = atoi(argv[5]);
    int max_time = atoi(argv[6]);
    int M_1 = atoi(argv[7]);
    int N_1 = atoi(argv[8]);
    int label = atoi(argv[9]);

    // get gpu device
    cl::Device device;
    getDevice(device, platformId, deviceId, deviceType);

    // get max devices sizes
    std::vector<size_t> max_wi_size;
    size_t max_wg_size;
    cl_ulong max_local_mem;
    get_ocl_device_max_local(device, &max_local_mem);
    get_ocl_device_limits(device, &max_wi_size, &max_wg_size);
    int combined_max_wi_size = std::max(max_wi_size[0], max_wi_size[1]);

    auto TP_CACHE_L_CB = atf::tp("CACHE_L_CB", {0, 1});
    auto TP_CACHE_P_CB = atf::tp("CACHE_P_CB", {0, 1});

    auto TP_G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto TP_L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {0, 1});
    auto TP_P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {0, 1}, [&](auto TP_P_CB_RES_DEST_LEVEL) { return TP_P_CB_RES_DEST_LEVEL <= TP_L_CB_RES_DEST_LEVEL; });

    auto TP_G_CB_SIZE_L_1 = atf::tp("G_CB_SIZE_L_1", {int(M_1)});
    auto TP_L_CB_SIZE_L_1 = atf::tp("L_CB_SIZE_L_1", atf::interval(0, (int) ceil(log2(M_1)), atf::pow_2),
                                    [&](auto TP_L_CB_SIZE_L_1) { return TP_G_CB_SIZE_L_1 >= TP_L_CB_SIZE_L_1; });
    auto TP_P_CB_SIZE_L_1 = atf::tp("P_CB_SIZE_L_1", atf::interval(0, (int) ceil(log2(M_1)), atf::pow_2),
                                    [&](auto TP_P_CB_SIZE_L_1) { return TP_L_CB_SIZE_L_1 >= TP_P_CB_SIZE_L_1; });

    auto TP_G_CB_SIZE_R_1 = atf::tp("G_CB_SIZE_R_1", {int(N_1)});
    auto TP_L_CB_SIZE_R_1 = atf::tp("L_CB_SIZE_R_1", atf::interval(0, (int) ceil(log2(N_1)), atf::pow_2),
                                    [&](auto TP_L_CB_SIZE_R_1) { return TP_G_CB_SIZE_R_1 >= TP_L_CB_SIZE_R_1; });
    auto TP_P_CB_SIZE_R_1 = atf::tp("P_CB_SIZE_R_1", atf::interval(0, (int) ceil(log2(N_1)), atf::pow_2),
                                    [&](auto TP_P_CB_SIZE_R_1) { return TP_L_CB_SIZE_R_1 >= TP_P_CB_SIZE_R_1; });

    auto TP_NUM_WG_L_1 = atf::tp("NUM_WG_L_1", atf::interval(0, (int) ceil(log2(M_1)), atf::pow_2));
    auto TP_NUM_WG_R_1 = atf::tp("NUM_WG_R_1", atf::interval(0, (int) ceil(log2(N_1)), atf::pow_2));
    //auto TP_NUM_WG_R_1 = atf::tp("NUM_WG_R_1", {1});

    auto TP_OCL_DIM_L_1 = atf::tp("OCL_DIM_L_1", {0, 1});
    auto TP_OCL_DIM_R_1 = atf::tp("OCL_DIM_R_1", {0, 1}, [&](auto OCL_DIM_R_1) { return TP_OCL_DIM_L_1 != OCL_DIM_R_1; });

    auto TP_NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          atf::interval(0, (int)ceil(log2(std::min(M_1, combined_max_wi_size))), atf::pow_2), [&](auto TP_NUM_WI_L_1) { return TP_OCL_DIM_L_1 == 0 ? (TP_NUM_WI_L_1 <= max_wi_size[0]) : (TP_NUM_WI_L_1 <= max_wi_size[1]); });
    auto TP_NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          atf::interval(0, (int)ceil(log2(std::min(N_1, combined_max_wi_size))), atf::pow_2), [&](auto TP_NUM_WI_R_1) { return (TP_OCL_DIM_R_1 == 0 ? (TP_NUM_WI_R_1 <= max_wi_size[0]) : (TP_NUM_WI_R_1 <= max_wi_size[1])) && TP_NUM_WI_L_1 * TP_NUM_WI_R_1 <= max_wg_size; });

    size_t search_space_size = 0;
    {
        auto tuner = atf::open_tuner(atf::cond::evaluations(0));
        tuner(TP_CACHE_L_CB);
        tuner(TP_CACHE_P_CB);
        tuner(TP_G_CB_RES_DEST_LEVEL, TP_L_CB_RES_DEST_LEVEL, TP_P_CB_RES_DEST_LEVEL);
        tuner(TP_G_CB_SIZE_L_1, TP_L_CB_SIZE_L_1, TP_P_CB_SIZE_L_1);
        tuner(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1, TP_P_CB_SIZE_R_1);
        tuner(TP_NUM_WG_L_1);
        tuner(TP_NUM_WG_R_1);
        tuner(TP_OCL_DIM_L_1, TP_OCL_DIM_R_1,
              TP_NUM_WI_L_1, TP_NUM_WI_R_1);
        search_space_size = tuner.search_space_size();
    }

    std::unique_ptr<atf::tuner_with_constraints> tuner;
    if(search_space_size <= threshold) {
        tuner = std::unique_ptr<atf::tuner_with_constraints>(new atf::exhaustive_class<>(atf::cond::evaluations(search_space_size) || atf::cond::duration<std::chrono::minutes>(max_time)));
    } else {
        tuner = std::unique_ptr<atf::tuner_with_constraints>(new atf::open_tuner_class<>(atf::cond::evaluations(std::min((size_t) threshold, size_t(0.25f * search_space_size))) || atf::cond::duration<std::chrono::minutes>(max_time)));
        if(0.25f * search_space_size < threshold)
            std::cout << "Small search space: Reducing # of configs to " << (0.25f * search_space_size) << std::endl;
    }

    (*tuner)(TP_CACHE_L_CB);
    (*tuner)(TP_CACHE_P_CB);
    (*tuner)(TP_G_CB_RES_DEST_LEVEL, TP_L_CB_RES_DEST_LEVEL, TP_P_CB_RES_DEST_LEVEL);
    (*tuner)(TP_G_CB_SIZE_L_1, TP_L_CB_SIZE_L_1, TP_P_CB_SIZE_L_1);
    (*tuner)(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1, TP_P_CB_SIZE_R_1);
    (*tuner)(TP_NUM_WG_L_1);
    (*tuner)(TP_NUM_WG_R_1);
    (*tuner)(TP_OCL_DIM_L_1, TP_OCL_DIM_R_1,
          TP_NUM_WI_L_1, TP_NUM_WI_R_1);

    std::string args_sh = std::to_string(M_1) + " " + std::to_string(N_1) + " " + std::to_string(label);
    std::string bashStr = bash + std::string(" ") + args_sh;
    auto cf = atf::cf::bash(bashStr, "runtime");

    auto config_check_local_mem = [&](atf::configuration config) {
        int CACHE_L_CB = config[TP_CACHE_L_CB.name()].value().int_val();
        int L_CB_SIZE_L_1 = config[TP_L_CB_SIZE_L_1.name()].value().int_val();
        int L_CB_SIZE_R_1 = config[TP_L_CB_SIZE_R_1.name()].value().int_val();
        int P_CB_RES_DEST_LEVEL = config[TP_P_CB_RES_DEST_LEVEL.name()].value().int_val();
        int L_CB_RES_DEST_LEVEL = config[TP_L_CB_RES_DEST_LEVEL.name()].value().int_val();
        int NUM_WI_R_1 = config[TP_NUM_WI_R_1.name()].value().int_val();

        size_t local_mem = 0;
        local_mem += CACHE_L_CB * (L_CB_SIZE_L_1 * L_CB_SIZE_R_1); // das in den Klammern musst du für deine Buffer anpassen. Hier nehme ich als Beispiel GEMM.
        if (P_CB_RES_DEST_LEVEL == 1 || L_CB_RES_DEST_LEVEL == 1)
            local_mem += L_CB_SIZE_L_1 * NUM_WI_R_1;
        local_mem *= sizeof(float);

        if (local_mem > max_local_mem) {
            std::cout << "Invalid config: Maximum of local memory exceeded.";
            throw std::exception();
        }
        return cf(config);
    };

    auto best_configuration = (*tuner)(config_check_local_mem);

    std::ostringstream result_oss;
    result_oss << "{\n"
               << "\t\"CACHE_L_CB\": " << best_configuration.at(TP_CACHE_L_CB.name()) << ",\n"
               << "\t\"CACHE_P_CB\": " << best_configuration.at(TP_CACHE_P_CB.name()) << ",\n"
               << "\t\"G_CB_RES_DEST_LEVEL\": " << best_configuration.at(TP_G_CB_RES_DEST_LEVEL.name()) << ",\n"
               << "\t\"L_CB_RES_DEST_LEVEL\": " << best_configuration.at(TP_L_CB_RES_DEST_LEVEL.name()) << ",\n"
               << "\t\"P_CB_RES_DEST_LEVEL\": " << best_configuration.at(TP_P_CB_RES_DEST_LEVEL.name()) << ",\n"
               << "\t\"L_CB_SIZE_L_1\": " << best_configuration.at(TP_L_CB_SIZE_L_1.name()) << ",\n"
               << "\t\"P_CB_SIZE_L_1\": " << best_configuration.at(TP_P_CB_SIZE_L_1.name()) << ",\n"
               << "\t\"NUM_WG_L_1\": " << best_configuration.at(TP_NUM_WG_L_1.name()) << ",\n"
               << "\t\"NUM_WI_L_1\": " << best_configuration.at(TP_NUM_WI_L_1.name()) << ",\n"
               << "\t\"OCL_DIM_L_1\": " << best_configuration.at(TP_OCL_DIM_L_1.name()) << ",\n"
               << "\t\"L_CB_SIZE_R_1\": " << best_configuration.at(TP_L_CB_SIZE_R_1.name()) << ",\n"
               << "\t\"P_CB_SIZE_R_1\": " << best_configuration.at(TP_P_CB_SIZE_R_1.name()) << ",\n"
               << "\t\"NUM_WG_R_1\": " << best_configuration.at(TP_NUM_WG_R_1.name()) << ",\n"
               << "\t\"NUM_WI_R_1\": " << best_configuration.at(TP_NUM_WI_R_1.name()) << ",\n"
               << "\t\"OCL_DIM_R_1\": " << best_configuration.at(TP_OCL_DIM_R_1.name()) << "\n"
               << "}\n";

    std::string resultStr = result_oss.str();
    std::ofstream out(result);
    out << resultStr;
    out.close();

    return 0;
}

#endif //ATF_TUNING_KERNEL_HPP
