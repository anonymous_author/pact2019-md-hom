# README

### Dependencies
The following dependencies were used to execute the programs to generate the kernels:

- GCC 6.4.0
- CMake 3.5.1

### Build
1. `mkdir build`
2. `cd build`
3. `cmake ..`
4. `make`

### Generate kernels 
Depending on the kernel, the generation may take some time (up to ~24 hours).

##### All kernel of ECC+MLP:

1. `./generate_ecc_mlp_kernel.sh`
2. The OpenCL kernels are stored in `./ecc_mlp_kernel/`.

##### All kernel of SVM:

1. `./generate_svm_kernel.sh`
2. The OpenCL kernels are stored in `./svm_kernel/`.

The two kernel of the Working Set Selection for SVM were merged into one kernel and was then heavily modified (they are not up to date here).

##### Specific kernel of ECC+MLP:
- Forward: `./build/eccmlp_forward`
- Forward Last: `./build/eccmlp_forward_last`
- Backward: `./build/eccmlp_backward`
- Weight Update: `./build/eccmlp_weight_update`
- Bias Update: `./build/eccmlp_weight_update`

##### Specific kernel of SVM:
- RBF Kernel (Training): `./build/svm_train_rbf`
- Error Update (Training): `./build/svm_error_update`
- RBF Kernel (Classifying): `./build/svm_classify_rbf`
- Working Set Selection (Up): `./build/svm_working_set_selection_up`
- Working Set Selection (Low): `./build/svm_working_set_selection_low`

### Source code
The source code of the kernels used for the implementation of ECC + MLP can be found in `./src/eccmlp` and for the implementation of SVM in `./src/svm`. 

