#include "../../md_hom_generator.hpp"

int main(){
    auto input_data = md_hom::input_buffer("input_data", {md_hom::L(1), md_hom::R(1)});
    auto input_line = md_hom::input_buffer("input_line", {md_hom::R(1)});
    auto result = md_hom::result_buffer("res", {md_hom::L(1)});

    auto md_hom_gemv =  md_hom::md_hom<1, 1>("svm_compute_linear",
                                             md_hom::inputs(input_data, input_line),
                                             md_hom::scalar_function("return input_data_val * input_line_val;"),
                                             md_hom::scalar_function(""),
                                             result
    );
    auto generator = md_hom::generator::ocl_generator(md_hom_gemv);
    std::ofstream kernel_file;
    kernel_file.open("svm_linear_kernel_var.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open("svm_linear_kernel_2_var.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}
