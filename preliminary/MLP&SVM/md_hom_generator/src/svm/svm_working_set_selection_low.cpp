
#include "../../md_hom_generator.hpp"

int generate_b_low(){
    auto error = md_hom::input_buffer("error", {md_hom::R(1)});
    auto result = md_hom::result_buffer("res", {});

    auto md_hom_min = md_hom::md_hom<0, 1>("md_hom_min",
                                           md_hom::inputs(error),
                                           md_hom::scalar_function("return error_val;"),
                                           md_hom::scalar_function(""),
                                           result
    );
    auto generator = md_hom::generator::ocl_generator(md_hom_min);
    std::ofstream kernel_file;
    kernel_file.open("svm_update_b_low.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open("svm_update_b_low_2.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}

int main(){
    generate_b_low();
}