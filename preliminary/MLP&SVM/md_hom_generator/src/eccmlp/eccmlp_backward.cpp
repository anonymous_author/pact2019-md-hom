#include "../../md_hom_generator.hpp"

/**
 * L1: Number of chains
 * L2: Size of previous layer (i-1)
 * R1: Size of current layer (i)
 */
int main(){
    // D values of current layer
    auto D = md_hom::input_buffer("D", {md_hom::L(1), md_hom::R(1)});

    // weights between previous and current layer
    // TODO: transpose W
    auto W = md_hom::input_buffer("W", {md_hom::L(1), md_hom::L(2), md_hom::R(1)});

    // S values of previous layer: use in second scalar function
    auto S = md_hom::input_buffer("S", {md_hom::L(1), md_hom::L(2)}, true);

    // B values of previous layer: use in second scalar function
    auto B = md_hom::input_buffer("B", {md_hom::L(1), md_hom::L(2)}, true);

    // after first scalar function: T of previous layer
    // after second scalar function: D of previous layer
    auto res = md_hom::result_buffer("result", {md_hom::L(1), md_hom::L(2)});

    // first scalar function to get T of prev layer: D * W
    auto f = md_hom::scalar_function("return D_val * W_val;");

    // result scalar function to get D of prev layer: theta'(S + B) * T
    auto g = md_hom::scalar_function("return 1 / (1 + exp(-(S_val + B_val))) * (1 - 1 / (1 + exp(-(S_val + B_val)))) * res;");

    auto md_hom = md_hom::md_hom<2, 1>("mlp_backward", md_hom::inputs(D, W, S, B), f, g, res, true);

    auto generator = md_hom::generator::ocl_generator(md_hom);
    std::ofstream kernel_file;
    kernel_file.open("mlp_backward_1.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open("mlp_backward_2.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}

