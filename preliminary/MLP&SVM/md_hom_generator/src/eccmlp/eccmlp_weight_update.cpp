#include "../../md_hom_generator.hpp"

/**
 * L1: Number of CC's
 * L2: Size of current layer
 * L3: Size of next layer
 */
int main(){
    // Z of current layer
    auto Z = md_hom::input_buffer("Z", {md_hom::L(1), md_hom::L(2)});

    // D of next layer
    auto D = md_hom::input_buffer("D", {md_hom::L(1), md_hom::L(3)});

    // weight gradient between current and next layer of previous iteration
    auto prevGrad = md_hom::input_buffer("prevGrad", {md_hom::L(1), md_hom::L(3), md_hom::L(2)}, true);

    // weights between current and next layer
    auto weight = md_hom::input_buffer("W", {md_hom::L(1), md_hom::L(3), md_hom::L(2)}, true);

    // learning rate and momentum
    auto learning_rate = md_hom::input_scalar("learning_rate", true);
    auto momentum = md_hom::input_scalar("momentum", true);

    // result buffer
    // TODO: replace res with "weight" after generating kernel
    auto weightRes = md_hom::result_buffer("res", {md_hom::L(1), md_hom::L(3), md_hom::L(2)});

    // compute weight gradient: Z * D
    auto f = md_hom::scalar_function("return Z_val * D_val;");

    // compute new weight: W + learning_rate * gradient + momentum * prev_gradient
    auto g = md_hom::scalar_function("return W_val + learning_rate_val * res + momentum_val * prevGrad_val;");

    auto md_hom_map = md_hom::md_hom<3, 0>("mlp_weight_update",
                                           md_hom::inputs(Z, D, prevGrad, weight, learning_rate, momentum),
                                           f, g, weightRes, true, true
    );

    auto generator = md_hom::generator::ocl_generator(md_hom_map);
    std::ofstream kernel_file;
    kernel_file.open("mlp_weight_update.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
}