#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&

mkdir $DIR/svm_kernel &&
cd $DIR/svm_kernel &&

echo "Generating kernel: RBF kernel for training..." &&
../build/svm_train_rbf &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: Error Update..." &&
../build/svm_error_update &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: Working Set Selection (Up)..." &&
../build/svm_working_set_selection_up &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: Working Set Selection (Low)..." &&
../build/svm_working_set_selection_low &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: RBF kernel for classifying..." &&
../build/svm_classify_rbf &&
echo "Finished generating kernel."
