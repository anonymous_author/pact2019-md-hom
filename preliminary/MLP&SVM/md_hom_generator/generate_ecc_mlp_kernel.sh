#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&

mkdir $DIR/ecc_mlp_kernel &&
cd $DIR/ecc_mlp_kernel &&

echo "Generating kernel: Forward..." &&
../build/eccmlp_forward &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: Forward Last..." &&
../build/eccmlp_forward_last &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: Backward..." &&
../build/eccmlp_backward &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: Weight Update..." &&
../build/eccmlp_weight_update &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: Bias Update..." &&
../build/eccmlp_bias &&
echo "Finished generating kernel." &&
echo &&

echo "Generating kernel: Forward Batch..." &&
../build/eccmlp_forward_batch &&
echo "Finished generating kernel."
