# README

### Dependencies
- OpenTuner (http://opentuner.org/tutorial/setup/)

### Build tuner files of atf:
- Open `atf_funer` folder.
- Build tuner files following the `README.md`.
- Copy files with `copy_svm.sh` in `atf_tuner` folder.

### Configure platform and device:
Edit `./settings.json` and fill in the platform and device id and device type (gpu, cpu or acc).

### Configure SVM:
Edit `./inputs.json`:
- "num_instances_train": Number of training instances
- "num_instances_test": Number of testing instances
- "num_attributes": Number of attributes
- "num_labels": Number of labels
- "num_sv_model": Average number of support vectors

### Configure Tuning:
Edit `./inputs.json`:
- {KERNEL-NUMBER}_{KERNEL-NAME}_tuning_configs: Maximal number of configs to tune
- {KERNEL-NUMBER}_{KERNEL-NAME}_max_time: Maximal time to tune

### Tune kernel for SVM:

Tune all kernel (RBF): `./01_tuning_rbf.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Default of `INPUT_FILE`: `./inputs.json`
- Default of `OUTPUT_FOLDER_NAME`: `.`

Tune all kernel (RBF): `./02_tuning_linear.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Default of `INPUT_FILE`: `./inputs.json`
- Default of `OUTPUT_FOLDER_NAME`: `.`

The optimal tuning configs found will be stored in `./tuning_results/{OUTPUT_FOLDER_NAME}` as JSON files.

### Tune specific kernel for SVM:

- Tune RBF (Training): `./scripts/01_train_rbf.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune Linear Kernel (Training): `./scripts/02_train_linear.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune Error Update: `./scripts/03_train_error_update.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune Working Set Selection: `./scripts/04_train_update_bias.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune RBF Batch (Classifiying): `./scripts/05_eval_rbf_main.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
- Tune RBF Score (Classifiying):: `./scripts/06_eval_rbf_score.sh {INPUT_FILE} {OUTPUT_FOLDER_NAME}`
