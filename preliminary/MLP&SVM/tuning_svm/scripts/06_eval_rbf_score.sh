#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&
cd $DIR/../tuner

PLATFORM_ID=`grep -oP "\"platform_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../settings.json`
DEVICE_ID=`grep -oP "\"device_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../settings.json`
DEVICE_TYPE=`grep -oP "\"device_type\"[ ]*:[ ]*\"\K[^ ,;\n\"]+" ../settings.json`

DEFAULTVALUE="inputs.json"
INPUT=${1:-$DEFAULTVALUE}

DEFAULT_OUTPUT="."
OUTPUT_PATH=${2:-$DEFAULT_OUTPUT}

TUNING_CONFIGS=`grep -oP "\"06_eval_rbf_score_configs\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
MAX_TIME=`grep -oP "\"06_eval_rbf_score_max_time\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
NUM_SV=`grep -oP "\"num_sv_model\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
NUM_RANGE=`grep -oP "\"RANGE\"[ ]*:[ ]*\K[^ ,;\n]+" ../tuning_results/$OUTPUT_PATH/eval_rbf_kernel_tp.json`

echo "Starting Tuner: RBF Scoring (Evaluation)"
echo "Tuning Configs: $TUNING_CONFIGS (Max: $MAX_TIME Minutes)"
echo "Number of Support Vectors: $NUM_SV"
echo "Batch: $NUM_RANGE"

./tuning_svm_eval_rbf_score $OUTPUT_PATH $PLATFORM_ID $DEVICE_ID $DEVICE_TYPE $TUNING_CONFIGS $MAX_TIME $NUM_RANGE $NUM_SV

$DIR/../clean.sh
