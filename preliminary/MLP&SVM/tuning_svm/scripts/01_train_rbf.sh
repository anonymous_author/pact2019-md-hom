#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&
cd $DIR/../tuner

PLATFORM_ID=`grep -oP "\"platform_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../settings.json`
DEVICE_ID=`grep -oP "\"device_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../settings.json`
DEVICE_TYPE=`grep -oP "\"device_type\"[ ]*:[ ]*\"\K[^ ,;\n\"]+" ../settings.json`

DEFAULTVALUE="inputs.json"
INPUT=${1:-$DEFAULTVALUE}

DEFAULT_OUTPUT="."
OUTPUT_PATH=${2:-$DEFAULT_OUTPUT}

TUNING_CONFIGS=`grep -oP "\"01_rbf_tuning_configs\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
MAX_TIME=`grep -oP "\"01_rbf_max_time\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
NUM_INSTANCES=`grep -oP "\"num_instances_train\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
NUM_ATTRIBUTES=`grep -oP "\"num_attributes\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
NUM_LABELS=`grep -oP "\"num_labels\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`

echo "Starting Tuner: RBF Kernel"
echo "Tuning Configs: $TUNING_CONFIGS (Max: $MAX_TIME Minutes)"
echo "Instances: $NUM_INSTANCES"
echo "Attributes: $NUM_ATTRIBUTES"
echo "Labels: $NUM_LABELS"

./tuning_svm_rbf_main $OUTPUT_PATH $PLATFORM_ID $DEVICE_ID $DEVICE_TYPE $TUNING_CONFIGS $MAX_TIME $NUM_INSTANCES $NUM_ATTRIBUTES $NUM_LABELS

$DIR/../clean.sh
