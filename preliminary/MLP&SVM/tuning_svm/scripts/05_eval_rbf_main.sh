#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&
cd $DIR/../tuner

PLATFORM_ID=`grep -oP "\"platform_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../settings.json`
DEVICE_ID=`grep -oP "\"device_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../settings.json`
DEVICE_TYPE=`grep -oP "\"device_type\"[ ]*:[ ]*\"\K[^ ,;\n\"]+" ../settings.json`

DEFAULTVALUE="inputs.json"
INPUT=${1:-$DEFAULTVALUE}

DEFAULT_OUTPUT="."
OUTPUT_PATH=${2:-$DEFAULT_OUTPUT}

TUNING_CONFIGS=`grep -oP "\"05_eval_rbf_configs\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
MAX_TIME=`grep -oP "\"05_eval_rbf_max_time\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
NUM_INSTANCES=`grep -oP "\"num_instances_test\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
NUM_ATTRIBUTES=`grep -oP "\"num_attributes\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
NUM_SV=`grep -oP "\"num_sv_model\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`

echo "Starting Tuner: RBF Kernel (Evaluation)"
echo "Tuning Configs: $TUNING_CONFIGS (Max: $MAX_TIME Minutes)"
echo "Instances: $NUM_INSTANCES, Attributes: $NUM_ATTRIBUTES"
echo "Number of Support Vectors: $NUM_SV"

./tuning_svm_eval_rbf $OUTPUT_PATH $PLATFORM_ID $DEVICE_ID $DEVICE_TYPE $TUNING_CONFIGS $MAX_TIME $NUM_INSTANCES $NUM_ATTRIBUTES $NUM_SV

$DIR/../clean.sh

