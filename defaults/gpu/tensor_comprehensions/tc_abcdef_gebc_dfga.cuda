
�>
tccg65
   "*2�8@ HP Xp� � " �H� " "(��`���0� " *Tesla V100-SXM2-16GB2(8e112e9dccda62c30ef29208a827e783b9a7f156:�;
template<typename T> inline __device__ T floord(T n, T d) {
  return n < 0 ? - (-n + d - 1)/d : n / d;
}
#define if_then_else(cond,a,b) (cond) ? (a) : (b);

// Halide type handling
typedef int int32;
typedef long int64;
typedef float float32;
typedef double float64;

#define inff __int_as_float(0x7f800000)
#define inf __longlong_as_double(0x7ff0000000000000LL)



namespace __tc {

// Re-implementing bits of type_traits because nvrtc no likes std includes
template <typename T, typename TT>
struct is_same {
  static constexpr bool value = false;
};

template <typename T>
struct is_same<T, T> {
  static constexpr bool value = true;
};

template <typename T>
struct numeric_limits {
};

template <>
struct numeric_limits<float> {
  static inline __device__ float max() {
    return 3.40282e+38;
  }
  static inline __device__ float min() {
    return -3.40282e+38;
  }
};

template <>
struct numeric_limits<int> {
  static inline __device__ int max() {
    return 0x7FFFFFFF;
  }
  static inline __device__ int min() {
    return 0xFFFFFFFF;
  }
};

enum class ReductionOp : int { Sum = 0, Prod = 1, Min = 2, Max = 3};

// Partial specialization is only allowed for classes...
template <typename T, ReductionOp R>
struct Reducer {
};

template <typename T>
struct Reducer<T, ReductionOp::Sum> {
  typedef T value_type;

  template<typename CubReduce>
  static inline __device__ T reduce(CubReduce red, T val) {
    return red.Sum(val);
  }
  static inline __device__ T reduce(T red, T val) {
    return red + val;
  }
  static constexpr T neutral = T(0);
};

template <typename T>
struct Reducer<T, ReductionOp::Prod> {
  template<typename CubReduce>
  static inline __device__ T reduce(CubReduce red, T val) {
    return red.Prod(val);
  }
  static inline __device__ T reduce(T red, T val) {
    return red * val;
  }
  static constexpr T neutral = T(1);
};

template <typename T>
struct Reducer<T, ReductionOp::Min> {
  template<typename CubReduce>
  static inline __device__ T reduce(CubReduce red, T val) {
    return red.Min(val);
  }
  static inline __device__ T reduce(T red, T val) {
    return red < val ? red : val;
  }
  static constexpr T neutral = numeric_limits<T>::max();
};

template <typename T>
struct Reducer<T, ReductionOp::Max> {
  template<typename CubReduce>
  static inline __device__ T reduce(CubReduce red, T val) {
    return red.Max(val);
  }
  static inline __device__ T reduce(T red, T val) {
    return red > val ? red : val;
  }
  static constexpr T neutral = numeric_limits<T>::min();
};

template <ReductionOp R, typename T>
__inline__ __device__ T warpReduce(T val) {
  for (int i = warpSize / 2; i >= 1; i /= 2) {
    val = Reducer<T, R>::reduce(val, __shfl_down(val, i));
  }
  return val;
}

template <typename Reducer>
struct WithBool {
  WithBool() : val(Reducer::neutral), b(false) {}
  WithBool(typename Reducer::value_type v_, bool b_) : val(v_), b(b_) {}
  typename Reducer::value_type  val;
  bool b;
};

template<typename Reducer>
struct SegmentedReducer {
  __device__ WithBool<Reducer> operator()(
      const WithBool<Reducer>& a, const WithBool<Reducer>& b) {
    return WithBool<Reducer>(
      b.b ? b.val : Reducer::reduce(a.val, b.val),
      a.b || b.b);
  }
};

} // namespace __tc


#include "cub/nvrtc_cub.cuh"

namespace __tc {

#define WARP_SIZE 32

template <int REDUCTION_SIZE, int BLOCKDIMY, int BLOCKDIMZ, ReductionOp R, typename T>
inline __device__ void CubReduceAlongXPowerOf2(T* dest, T val) {
  assert(REDUCTION_SIZE == blockDim.x && "blockDim.x size mismatch");

  using CubReduce = cub::BlockReduce<T, REDUCTION_SIZE>;
  __shared__ typename CubReduce::TempStorage temp_storage[BLOCKDIMY][BLOCKDIMZ];
  T aggregate = Reducer<T, R>::reduce(
    CubReduce(temp_storage[threadIdx.y][threadIdx.z]), val);
  __syncthreads();
  if (threadIdx.x == 0) {
    *dest = Reducer<T, R>::reduce(*dest, aggregate);
  }
  __syncthreads();
}

#define POWEROF2(X)                             \
  ((X) & ((X) - 1) == 0)

template <int REDUCTION_SIZE, int BLOCKDIMY, int BLOCKDIMZ, ReductionOp R, typename T>
inline __device__ void CubReduceAlongX(T* dest, T val) {
  __syncthreads();

  assert(REDUCTION_SIZE == blockDim.x && "blockDim.x size mismatch");

  // Except when blockDim.y == blockDim.z == 1 which seems fine
  bool allowCubReduce = ((blockDim.y == 1) and (blockDim.z == 1));
  if (allowCubReduce or POWEROF2(REDUCTION_SIZE)) {
    CubReduceAlongXPowerOf2<REDUCTION_SIZE, BLOCKDIMY, BLOCKDIMZ, R, T>(dest, val);
    return;
  }

  // CUB reductions do not allow general partial-block reductions.
  // Consider a case where threads(x,y,z) = (11, 12, 13); we want to perform
  // 12x13 parallel 11-wide reductions.
  // A workaround is to perform a full-block prefix-sum that is 11x12x13-wide
  // with a segmented reduction operator.
  using CubScan = cub::BlockScan<
    WithBool<Reducer<T, R>>,
    REDUCTION_SIZE,
    cub::BLOCK_SCAN_RAKING,
    BLOCKDIMY,
    BLOCKDIMZ>;

  __shared__ typename CubScan::TempStorage temp_storage;

  using SegmentedReducerType = SegmentedReducer<Reducer<T, R>>;
  SegmentedReducerType segmentedReducer;

  WithBool<Reducer<T, R>> res;
  // Head of the segment -> true
  WithBool<Reducer<T, R>> v(val, threadIdx.x == 0);
  CubScan(temp_storage).InclusiveScan(v, res, segmentedReducer);
  if (threadIdx.x == REDUCTION_SIZE - 1) {
    *dest = Reducer<T, R>::reduce(*dest, res.val);
  }
}

} // namespace __tc
extern "C" {
__global__ void tccg6_24_16_16_24_16_16_24(int32 A, int32 B, int32 C, int32 D, int32 E, int32 F, int32 G, float32* pO, float32* pL, float32* pR) {
  int b0 = blockIdx.x; int b1 = blockIdx.y; int b2 = blockIdx.z;
  int t0 = threadIdx.x; int t1 = threadIdx.y; int t2 = threadIdx.z;
  float32 (*O)[16][16][24][16][16] = reinterpret_cast<float32 (*)[16][16][24][16][16]>(pO);
  float32 (*L)[16][16][16] = reinterpret_cast<float32 (*)[16][16][16]>(pL);
  float32 (*R)[16][24][24] = reinterpret_cast<float32 (*)[16][24][24]>(pR);
  float32 acc_0;
  __shared__ float32 _R_0[24][16][24][1];
  __syncthreads();
  for (int c3 = t2; c3 <= 23; c3 += 8) {
    for (int c5 = t0; c5 <= 23; c5 += 3) {
      _R_0[c3][t1][c5][0] = R[c3][t1][c5][b0];
    }
  }
  __syncthreads();
  for (int c8 = 0; c8 <= 1; c8 += 1) {
    for (int c9 = 0; c9 <= 15; c9 += 1) {
      for (int c10 = t2; c10 <= 23; c10 += 8) {
        for (int c12 = t0; c12 <= 15; c12 += 3) {
          O[b0][2*b1 + c8][c9][c10][t1][c12] = 0.000000f;
        }
      }
    }
  }
  __syncthreads();
  for (int c8 = 0; c8 <= 1; c8 += 1) {
    for (int c9 = 0; c9 <= 15; c9 += 1) {
      for (int c10 = 0; c10 <= 23; c10 += 1) {
        for (int c11 = t2; c11 <= 15; c11 += 8) {
          acc_0 = 0.000000f;
          for (int c13 = t0; c13 <= 23; c13 += 3) {
            acc_0 = (acc_0 + (L[c13][c11][2*b1 + c8][c9]*_R_0[c10][t1][c13][0]));
          }
          __tc::CubReduceAlongX<3,16,8,__tc::ReductionOp::Sum>(&O[b0][2*b1 + c8][c9][c10][c11][t1], acc_0);
        }
      }
    }
  }
  __syncthreads();
  __syncthreads();
}
}

/*
Mapping Options:
tc::MappingOptions::makeNaiveMappingOptions()
    .outerScheduleFusionStrategy(tc::FusionStrategy::Preserve3Coincident)
    .outerScheduleAllowSkewing(false)
    .outerSchedulePositiveOrthant(true)
    .intraTileScheduleFusionStrategy(tc::FusionStrategy::Preserve3Coincident)
    .intraTileScheduleAllowSkewing(false)
    .intraTileSchedulePositiveOrthant(true)
    .tile(1, 2)
    .mapToThreads(3, 16, 8)
    .mapToBlocks(256, 8, 8)
    .unroll(2)
    .tileImperfectlyNested(false)
    .useSharedMemory(true)
    .usePrivateMemory(false)
    .unrollCopyShared(true)
    .matchLibraryCalls(true);
TC version: 8e112e9dccda62c30ef29208a827e783b9a7f156
*/
Btccg6_24_16_16_24_16_16_24H0H H H0H H H0RZ