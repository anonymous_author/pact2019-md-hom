// High-level hash: 134ba95f430b3b2f339c99f0eaf746a910c5005430cef6c4f89ba212bbfcc835
// Low-level hash: d0961838d9105ccc00b02d417d5bf86355c6dcb9253c8df983acc7e42206f966
float jacobi(float C, float N, float S, float E, float W, float F, float B){
  return 0.161f * E + 0.162f * W +
  0.163f * S + 0.164f * N +
  0.165f * B + 0.166f * F -
  1.67f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 254; v_gl_id_24 = (v_gl_id_24 + 256)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 254; v_gl_id_25 = (v_gl_id_25 + 256)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 254; v_gl_id_26 = (v_gl_id_26 + 128)) {
        v__29 = jacobi(v__27[(65793 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65537 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(66049 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65794 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65792 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(257 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(131329 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))]);
        v__30[(65793 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))] = id(v__29);
      }
    }
  }
}}