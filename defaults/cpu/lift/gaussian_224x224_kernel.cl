// High-level hash: 0c6caac1d8e948675ebb5a27d2bf43932457d27c6532f8369d8fb385cd077796
// Low-level hash: cc7c7cbecb820888f5848db8b08b1da7bc879dc4287e2a56164cf6d6a3b613ba
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__14, global float* v__17){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__16;
  for (int v_gl_id_12 = get_global_id(1); v_gl_id_12 < 220; v_gl_id_12 = (v_gl_id_12 + 64)) {
    for (int v_gl_id_13 = get_global_id(0); v_gl_id_13 < 220; v_gl_id_13 = (v_gl_id_13 + 8)) {
      v__16 = jacobi(v__14[(v_gl_id_13 + (224 * v_gl_id_12))], v__14[(1 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(2 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(3 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(4 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(224 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(225 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(226 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(227 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(228 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(448 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(449 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(450 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(451 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(452 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(672 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(673 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(674 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(675 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(676 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(896 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(897 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(898 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(899 + v_gl_id_13 + (224 * v_gl_id_12))], v__14[(900 + v_gl_id_13 + (224 * v_gl_id_12))]);
      v__17[(450 + v_gl_id_13 + (224 * v_gl_id_12))] = id(v__16);
    }
  }
}}