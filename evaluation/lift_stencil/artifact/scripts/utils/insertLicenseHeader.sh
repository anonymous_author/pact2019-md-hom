#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Directory required as a parameter!"
    exit -1
fi

DIR=$1

HEADER="This is licensed only for use in this project.\nTo obtain permission for use, email at: ."


FILES=$( find $DIR -type f -exec grep -Iq . {} \; -print )


for FILE in ${FILES} ; do
    sed -i "1 i\ ${HEADER}" $FILE
done

