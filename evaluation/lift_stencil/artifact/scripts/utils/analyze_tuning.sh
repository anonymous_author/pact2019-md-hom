#!/bin/bash
### README ########################################################################
# This script is supposed to be executed in the folder containing the cost files  #
# of tuned kernels (<hash>.cl.cost.csv)                                           #
#                                                                                 #
# prints kernels and best config in descending order (best is lowest)             #
###################################################################################

### CREATE SUMMARY ################################################################################
function process_file {
	FILE=$1
	LENGTH="$(cat $FILE | wc -l)"
	# throw away csv which only contain header -> invalid runs
	if [ $LENGTH -gt 1 ]
	then
		POSITION="$(cat $FILE | grep cost | sed -e 's/;/ /g' | wc -w)"
		echo -n $FILE | sed -e 's/.cost.csv//g'
		echo -n " -> "
		BEST="$(cat $FILE | \
			sort -n -r --field-separator=';' -k $POSITION | \
			tail -n 2 | head -1)"
		echo -n $BEST | sed -e 's/;/ /g' | awk -v pos=$POSITION '{printf $pos,"\t"}'
		echo -n ' ('
		echo -n $BEST
		echo -e ")"
	fi
}

export -f process_file

ls -l | awk '{print $9}' | grep cost.csv | xargs -n 1 bash -c 'process_file "$@"' _ | sort -n -k 3 -r > summary.txt
cat summary.txt
echo

### CREATE BEST KERNEL ############################################################################
KERNEL="$(cat summary.txt | awk '{print $1}' | tail -n 1)"
HEADER="$(cat ${KERNEL}.cost.csv | head -n 1)"
CONFIG="$(cat summary.txt | tail -n 1 | awk '{print $4}' | tr -d '()' | sed 's/;/ /g')"

cp $KERNEL best.cl
# remove atf directives
sed -i '/atf::/d' best.cl
sed -i '/\\/d' best.cl
sed -i '/^$/d' best.cl

echo $HEADER | sed 's/\_/\\\_/g' | tr ";" "\n" | head -n -1 > header
echo $CONFIG | tr " " "\n" | head -n -1 > config

# replace all tuning parameters with the best found values
paste header config | while read n k; do sed -i "s/$n/$k/g" best.cl; done 

# remove auxiliary files
rm header
rm config
