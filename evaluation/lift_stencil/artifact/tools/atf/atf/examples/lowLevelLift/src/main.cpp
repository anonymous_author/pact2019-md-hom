#include <atf.h>

int main() {
  const int N = 1024;
  auto GS0 = atf::tp("GS0", atf::interval<int>(1, N));
  //  auto GS1 = atf::tp( "GS1", atf::interval<int>(1,N));
  //  auto GS2 = atf::tp( "GS2", atf::interval<int>(1,N));

  auto LS0 = atf::tp("LS0", atf::interval<int>(1, N), atf::divides(GS0));
  //  auto LS1 = atf::tp( "LS1", atf::interval<int>(1,N),    atf::divides( GS1
  //  )); auto LS2 = atf::tp( "LS2", atf::interval<int>(1,N),    atf::divides(
  //  GS2 ));

  auto cf =
      atf::cf::ccfg("./compileScript.sh",
                    ::std::make_pair("./compileScript.sh", "./runScript.sh"),
                    true, "./costfile.txt");
  // auto cf = atf::cf::ccfg("./compile.sh", "./run.sh", true,
  // "./costfile.txt");

  //	auto best =
  //atf::exhaustive(atf::cond::duration<std::chrono::seconds>(2))(GS0,GS1,GS2,LS0,LS1,LS2)(cf);
  auto best = atf::exhaustive(atf::cond::duration<std::chrono::seconds>(30))(
      GS0, LS0)(cf);
}
