## ATF library Examples
##

cmake_minimum_required(VERSION 3.1)


# subdirlist - Retrieve a list of all subdirectories
macro(subdirlist result curdir)
  file(GLOB children RELATIVE ${curdir} ${curdir}/*)
  set(dirlist "")
  foreach(child ${children})
    if(IS_DIRECTORY ${curdir}/${child})
        list(APPEND dirlist ${child})
    endif()
  endforeach()
  set(${result} ${dirlist})
endmacro()
# ---


# Fetch list of subdirectories
subdirlist(DIRS ${CMAKE_CURRENT_LIST_DIR})

# Register all examples
foreach(SUBDIR ${DIRS})
	add_subdirectory(${SUBDIR})
endforeach()








