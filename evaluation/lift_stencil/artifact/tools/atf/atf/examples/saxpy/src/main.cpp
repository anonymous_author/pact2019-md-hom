#include <atf.h>
#include <string>

int main() {
  const int N = 1024;

  const ::std::string saxpy = R"cl__( 
                __kernel void saxpy( const int N,
                     const float a,
                     const __global float* x,
                      __global float* y)
                {       
                        for(int w = 0; w < WPT; ++w) {
                                const int id = w * get_global_size(0) 
                                                + get_global_id(0);
                                y[id] += a * x[id];
                        }
                }
        )cl__";

  auto WPT = atf::tp("WPT", atf::interval<int>(1, N), atf::divides(N));

  auto LS = atf::tp("LS", atf::interval<int>(1, N), atf::divides(N / WPT));

  auto cf_saxpy =
      atf::cf::ocl({0, atf::cf::device_info::GPU, 1}, {saxpy, "saxpy"},
                   inputs(atf::scalar<int>(N),    // N
                          atf::scalar<float>(),   // a
                          atf::buffer<float>(N),  // x
                          atf::buffer<float>(N)), // y
                   atf::cf::GS(N / WPT), atf::cf::LS(LS));

  auto best_config = atf::annealing(
      atf::cond::duration<std::chrono::minutes>(2))(WPT, LS)(cf_saxpy);
}
