#include <atf.h>

int main() {
  auto N = atf::tp("N", atf::interval<std::size_t>(1, 5));
  auto M = atf::tp("M", atf::interval<std::size_t>(1, 5));

  auto cf = atf::cf::ccfg("test.hs", "./run.sh", true, "costfile.txt");

  auto best =
      atf::exhaustive(atf::cond::duration<std::chrono::seconds>(5))(N, M)(cf);
}
