// This file is part of the Auto Tuning Framework (ATF).


//
//  atf.h
//  new_atf_lib
//
//  Created by   on 28/10/2016.
//  Copyright © 2016  . All rights reserved.
//

#ifndef atf_h
#define atf_h

#include "atf/abort_conditions.hpp"
#include "atf/range.hpp"
#include "atf/tp.hpp"
#include "atf/atfc_wrapper.hpp"

#include "atf/open_tuner.hpp"
#include "atf/open_tuner_flat.hpp"
#include "atf/exhaustive.hpp"
#include "atf/annealing.hpp"
#include "atf/annealing_tree.hpp"

#include "atf/operators.hpp"
#include "atf/predicates.hpp"

#include "atf/ocl_wrapper.hpp"
#include "atf/cuda_wrapper.hpp"
#include "atf/cpp_cf.hpp"


#endif /* atf_h */
