// This file is part of the Auto Tuning Framework (ATF).


#pragma once

#include <algorithm>
#include <fstream>
#include <iterator>
#include <limits>
#include <random>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

namespace atf {
// Tag types used to implement different sources of buffer data
namespace internal {
struct tag_base_t {};

struct from_file_t : tag_base_t {
  from_file_t(const ::std::string &p_str) : m_Path{p_str} {}

  ::std::string m_Path;
};

template <typename It> struct from_memory_t : tag_base_t {
  from_memory_t(It p_begin, It p_end) : m_Begin{p_begin}, m_End{p_end} {}

  It m_Begin;
  It m_End;
};

struct random_data_t : tag_base_t {
  random_data_t(::std::size_t p_size) : m_Size{p_size} {}

  ::std::size_t m_Size;
};
} // namespace internal

// Implementation of a random sequence generator
namespace internal {
auto entropy_source() -> ::std::random_device &;

class sequence_generator_base {
protected:
  sequence_generator_base();

public:
  sequence_generator_base(const sequence_generator_base &) = default;
  sequence_generator_base(sequence_generator_base &&) = default;

protected:
  auto engine() -> ::std::default_random_engine &;

protected:
  ::std::default_random_engine m_Engine;
};

template <typename Tdist>
class sequence_generator_impl : public sequence_generator_base {
  using this_type = sequence_generator_impl<Tdist>;
  using distribution_type = Tdist;
  using value_type = typename Tdist::result_type;

public:
  sequence_generator_impl() : m_Dist{min, max} {}

public:
  sequence_generator_impl(const this_type &) = default;
  sequence_generator_impl(this_type &&) = default;

public:
  template <typename It> auto fill(It p_begin, It p_end) -> void {
    ::std::generate(p_begin, p_end,
                    [this]() { return this->m_Dist(this->engine()); });
  }

private:
  static constexpr value_type max = ::std::numeric_limits<value_type>::max();
  static constexpr value_type min = ::std::numeric_limits<value_type>::min();

private:
  distribution_type m_Dist;
};

template <typename T>
using sequence_generator = ::std::conditional_t<
    ::std::is_integral<T>::value,
    sequence_generator_impl<::std::uniform_int_distribution<T>>,
    sequence_generator_impl<::std::uniform_real_distribution<T>>>;
} // namespace internal

// Buffer class implementation
template <typename T> class buffer_class {
  using this_type = buffer_class<T>;
  using value_type = T;
  using pointer = T *;
  using const_pointer = const T *;
  using reference = T &;
  using const_reference = const T &;
  using storage_type = ::std::vector<T>;
  using size_type = typename storage_type::size_type;
  using generator_type = internal::sequence_generator<T>;

public:
  template <typename It> buffer_class(internal::from_memory_t<It> &&);

  buffer_class(internal::from_file_t &&);
  buffer_class(internal::random_data_t &&);

public:
  buffer_class(const this_type &) = default;
  buffer_class(this_type &&) = default;

  this_type &operator=(const this_type &) = default;
  this_type &operator=(this_type &&) = default;

public:
  auto data() -> pointer;

public:
  auto size() const -> size_type;

  auto data() const -> const_pointer;

private:
  storage_type m_Storage;
  generator_type m_Generator;
};

template <typename T>
buffer_class<T>::buffer_class(internal::random_data_t &&p_tag)
    : m_Storage(p_tag.m_Size) {
  m_Generator.fill(m_Storage.begin(), m_Storage.end());
}

template <typename T>
template <typename It>
buffer_class<T>::buffer_class(internal::from_memory_t<It> &&p_tag)
    : m_Storage(p_tag.m_Begin, p_tag.m_End) {}

template <typename T>
buffer_class<T>::buffer_class(internal::from_file_t &&p_tag) {
  ::std::ifstream t_file{p_tag.m_Path};

  if (t_file.fail())
    throw ::std::runtime_error(
        "atf::buffer_class<T>: Failed to open specified file");

  ::std::copy(::std::istream_iterator<T>(t_file), ::std::istream_iterator<T>(),
              ::std::back_inserter(m_Storage));
}

template <typename T> auto buffer_class<T>::data() -> pointer {
  return m_Storage.data();
}

template <typename T> auto buffer_class<T>::data() const -> const_pointer {
  return m_Storage.data();
}

template <typename T> auto buffer_class<T>::size() const -> size_type {
  return m_Storage.size();
}

// Functions used by the user to create buffers
template <typename T, typename TTag,
          typename = ::std::enable_if_t<::std::is_base_of<
              internal::tag_base_t, ::std::decay_t<TTag>>::value>>
auto buffer(TTag &&p_tag) {
  return buffer_class<T>(::std::forward<TTag>(p_tag));
}

auto from_file(const ::std::string &p_path) -> internal::from_file_t;

auto random_data(::std::size_t p_size) -> internal::random_data_t;

template <typename T> auto buffer(::std::size_t p_size) {
  return buffer<T>(random_data(p_size));
}

template <typename It> auto from_memory(It p_begin, It p_end) {
  return internal::from_memory_t<It>{p_begin, p_end};
}

template <typename T>
auto from_memory(const T *p_begin, ::std::size_t p_count) {
  return internal::from_memory_t<const T *>{p_begin, p_begin + p_count};
}

template <typename T> auto from_memory(::std::initializer_list<T> p_list) {
  return internal::from_memory_t<
      typename ::std::initializer_list<T>::const_iterator>{p_list.begin(),
                                                           p_list.end()};
}
} // namespace atf
