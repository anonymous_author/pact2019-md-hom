// This file is part of the Auto Tuning Framework (ATF).


#ifndef atfc_wrapper_h
#define atfc_wrapper_h

#include <string>

#include "detail/process.hpp"
#include "detail/rope.hpp"
#include "tp_value.hpp"

/*
        TODO: Throw exceptions when inputs are bad
        TODO: Throw exceptions when script execution returns error code
*/

namespace atf {
namespace cf {
class atfc_callable {
public:
  atfc_callable(const ::std::string &p_srcFile,       //< Path to source file
                const ::std::string &p_compileScript, //< Path to compile script
                const ::std::string &p_runScript,     //< Path to run script
                bool p_useCf, const ::std::string &p_cf);

  ~atfc_callable();

public:
  // Do not allow copying, since that would invalidate the string views
  // stored in the rope.
  atfc_callable(const atfc_callable &) = delete;
  atfc_callable &operator=(const atfc_callable &) = delete;
  atfc_callable(atfc_callable &&) = default;
  atfc_callable &operator=(atfc_callable &&) = default;

public:
  auto operator()(const configuration &) -> ::std::size_t;

private:
  // Process using cost file
  auto process_cf(const configuration &) -> ::std::size_t;

  // Process using time measurment
  auto process_tm(const configuration &) -> ::std::size_t;

  // Load source code from file
  auto load_source() -> void;

  // Load latest cost entry from cost file
  auto load_cost() -> ::std::size_t;

  // Launches the run script and waits for it to finish
  auto launch_runscript() -> void;

  // Launches the compile script and waits for it to finish
  auto launch_compilescript() -> void;

  // Creates a new version of the source code with given tp values
  // and writes it to the original source file
  auto write_source(const configuration &) -> void;

  // Creates a new version of the source code with given tp values and
  // writes to given file path
public:
  auto write_source(const configuration &, const ::std::string &) -> void;

private:
  // Scan the source code for tuning parameters and modify the rope DS.
  auto process_source() -> void;

private:
  // Creates and splits the rope, using TPs listed in the configuration
  auto split_rope(const configuration &) -> void;

private:
  bool m_SplitRope{false};       //< Whether the rope was already split sor not
  ::std::string m_Source;        //< Source string
  detail::rope m_Rope;           //< Rope containing references to source string
  bool m_UseCostFile;            //< Whether to use a cost file or not
  ::std::string m_CostFile;      //< Name of cost file, if used.
  ::std::string m_RunScript;     //< Path to run script.
  ::std::string m_SourceFile;    //< Path to source file.
  ::std::string m_CompileScript; //< Path to compile script.
  ::std::string m_OriginalSrc;   //< Unaltered source file contents
};

auto ccfg(const ::std::string &p_sourceFile, const ::std::string &p_runScript,
          bool p_useCostFile = false,
          const ::std::string &p_costFile = ::std::string{}) -> atfc_callable;

auto ccfg(const ::std::string &p_sourceFile,
          ::std::pair<::std::string, ::std::string> p_scripts,
          bool p_useCostFile = false,
          const ::std::string &p_costFile = ::std::string{}) -> atfc_callable;
} // namespace cf
} // namespace atf

#endif
