// This file is part of the Auto Tuning Framework (ATF).


#include <algorithm>
#include <boost/program_options.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <vector>

#ifndef ATFC_NO_BOOST_PROCESS
#include <boost/process.hpp>
#endif

#include <boost/filesystem/convenience.hpp>
#include <code_generator.hxx>
#include <diagnostics.hxx>
#include <json_parser.hxx>
#include <parser/parser.hxx>

using namespace atfc;
namespace po = boost::program_options;
namespace fs = boost::filesystem;

#ifndef ATFC_NO_BOOST_PROCESS
namespace bp = boost::process;
#endif

// Data supplied from command line
static ::std::string g_fileName;
static ::std::string g_output = "tuner.cpp";
static bool g_inline;
static bool g_omit_tuning;
static bool g_debug;
static bool g_emit_source;
static bool g_emit_json;
static bool g_cleanup;
static bool g_help;
static bool g_json_input;
static bool g_redirect;
static ::std::vector<::std::string> g_passthrough_args;
//

// Handle arguments that are meant to be passed through to the tuner program
auto handle_passthrough(int &argc, char **argv) -> void {
  // Try to find the "--" separator

  // argv[0] is the program name
  const auto t_begin = argv + 1;
  const auto t_end = argv + argc;

  const auto t_iter =
      ::std::find_if(argv + 1, argv + argc, [](const char *t_str) -> bool {
        return ::std::strcmp(t_str, "--") == 0;
      });

  // If it wasn't found there is nothing to do
  if (t_iter == t_end)
    return;
  else {
    // There is a separator. Find its index
    const auto t_pos = t_iter - (t_begin - 1);

    // Copy all entries starting at that position, but dont copy the "--"
    for (auto t_ix = t_pos + 1; t_ix < argc; ++t_ix)
      g_passthrough_args.push_back(::std::string{argv[t_ix]});

    // Modify argc to hide the pass-through arguments from the the boost parser
    argc -= (t_end - t_iter);
  }
}

// Parse commandline and detect any misuse
auto handle_commandline(int argc, char **argv) -> void {
  // === Create command line handler and parse
  po::options_description t_desc("Allowed options");
  t_desc.add_options()(
      "inline,I", po::bool_switch(&g_inline),
      "Allows input to be a source file with inline directives")(
      "omit-tuning,O", po::bool_switch(&g_omit_tuning),
      "Don't execute generated tuner")(
      "emit-source,S", po::bool_switch(&g_emit_source),
      "Emit a source file with optimal tuning parameters")(
      "emit-json,J", po::bool_switch(&g_emit_json),
      "Emit a JSON file containg the best configuration")(
      "json,j", po::bool_switch(&g_json_input),
      "Allows input to be a JSON document containing directives")(
      "input,i", po::value<::std::string>(&g_fileName),
      "Input file")("output,o", po::value<::std::string>(&g_output),
                    "Output file. Can only be specified if tuning is omitted.")(
      "redirect,r", po::bool_switch(&g_redirect),
      "Redirect stdout and stderr of tuner to log file")(
      "clean,C", po::bool_switch(&g_cleanup),
      "Cleanup after tuning")("help,h", po::bool_switch(&g_help), "Print help")(
      "debug,d", po::bool_switch(&g_debug), "Debug mode");

  po::variables_map t_vm;

  try {
    po::store(po::parse_command_line(argc, argv, t_desc), t_vm);
  } catch (const ::std::exception &p_ex) {
    post_diagnostic(message_type::error, "atfc", p_ex.what());
    ::std::exit(EXIT_FAILURE);
  }

  po::notify(t_vm);
  // ===

  // === Handle help if supplied. This will exit the application, so it needs
  // 	   to be done first.
  if (g_help) {
    ::std::cout << "Usage: " << argv[0] << " [options] [ -- [tuner_arguments] ]"
                << ::std::endl;
    ::std::cout << t_desc;
    ::std::exit(EXIT_SUCCESS);
  }
  // ===

  // === Check for any misuse
  const auto t_supplied = [&t_vm](const ::std::string &p_name) -> bool {
    return t_vm.count(p_name);
  };

  // Input is always required
  if (!t_supplied("input")) {
    post_diagnostic(message_type::error, "atfc", "no input files");
    ::std::exit(EXIT_FAILURE);
  }

  // Specifying an output file path is only allowed if we omit tuning.
  if (t_supplied("output") && !g_omit_tuning) {
    post_diagnostic(message_type::error, "atfc",
                    "can't specify output file name if tuning is not omitted");
    ::std::exit(EXIT_FAILURE);
  }

  // Cannot specify both inline and json
  if (g_inline && g_json_input) {
    post_diagnostic(message_type::error, "atfc",
                    "can't use both inline and json input mode");
    ::std::exit(EXIT_FAILURE);
  }
  // ===
}

auto load_source_file() -> ::std::string {
  // Check if file exists
  if (!fs::exists(g_fileName)) {
    post_diagnostic(message_type::error, "atfc",
                    "specified source file not found");
    ::std::exit(EXIT_FAILURE);
  }

  // String stream used to read in the file
  ::std::stringstream t_inText{};

  {
    // Open specified source file
    ::std::ifstream t_inFile{g_fileName};

    // Copy all contents to stringbuffer
    t_inText << t_inFile.rdbuf();

    t_inFile.close();
  }

  // Retrieve file contents as string
  auto t_src = t_inText.str();

  // Transform all tab characters to single spaces to make diagnostics work
  // (tab characters count as one character in the column counter, but are
  // displayed as more than one. This screws up the diagnostics output
  // when underlines and carets are used)
  ::std::transform(t_src.begin(), t_src.end(), t_src.begin(),
                   [](const char c) -> char {
                     if (c == '\t')
                       return ' ';
                     else
                       return c;
                   });

  return t_src;
}

auto execute_tuner(const atfc::job &p_job) -> void {

#ifndef ATFC_NO_BOOST_PROCESS
#define SHELL_EXEC(x) bp::system(x, boost::this_process::environment())
#else
#define SHELL_EXEC(x) system(x)
#endif

  // Generate compile command
  ::std::ostringstream t_str{};
  t_str << "sh ./create_tuner.sh "
        << (p_job.m_Mode == atfc::mode::cuda ? "1" : "0");
  const auto t_compileCmd = t_str.str();

  // Compile and copy tuner to build directory
  if (SHELL_EXEC(t_compileCmd.c_str()) != EXIT_SUCCESS) {
    post_diagnostic(message_type::error, "atfc",
                    "failed to generate tuner, please refer to log file "
                    "\"create_tuner.log\"");
    ::std::exit(EXIT_FAILURE);
  }

  // Return code of tuner execution
  int t_rc{};

  // Base string used for tuner executing
  const ::std::string t_tunerCmd =
      g_redirect ? "./execute_tuner.sh 1" : "./execute_tuner.sh 0";

  // Build tuner command line
  if (g_passthrough_args.size() > 0) {
    ::std::ostringstream t_str{};

    t_str << t_tunerCmd;

    for (const auto &t_arg : g_passthrough_args)
      t_str << ' ' << ::std::quoted(t_arg);

    const auto t_cmd = t_str.str();

    t_rc = SHELL_EXEC(t_cmd.c_str());
  } else
    t_rc = SHELL_EXEC(t_tunerCmd.c_str());

  // Check return code
  if (t_rc != EXIT_SUCCESS) {
    post_diagnostic(
        message_type::error, "atfc",
        "failed to tune program, please refer to log file \"tuner.log\"");
    ::std::exit(EXIT_FAILURE);
  }
}

auto main(int argc, char **argv) -> int {
  // Handle passthrough
  handle_passthrough(argc, argv);

  // Parse commandline
  handle_commandline(argc, argv);

  // Set debug mode according to specified flags
  switch_debug(g_debug);

  // Load source file and process it
  const auto t_src = load_source_file();

  try {
    // Job object to be filled
    atfc::job t_job{};

    // Check if input is a json document
    if (g_json_input) {
      t_job = atfc::parse_json(g_fileName);
    } else // Configuration file or source file with inline directives
    {
      // Parse it
      t_job = atfc::parse({t_src}, {g_fileName}, g_inline);

      // If we are in inline mode we have to set the source information in the
      // job class, since the user is not required to do that in that case
      if (g_inline) {
        // TODO this is bad. We should just ADD it incase the user did specify
        // auxiliary source files. We should then remove all duplicates.
        t_job.m_SourceFiles.clear();
        t_job.m_SourceFiles.push_back(g_fileName);
      }

      // Make sure that there is indeed a source file registered
      if (t_job.m_SourceFiles.size() == 0) {
        post_diagnostic(message_type::error, {g_fileName},
                        "no source file specified");
        throw ::std::runtime_error("");
      }
    }

    // In the following operations, the name of the source file is needed.
    // Since we are just using one at the time, we can just take the first one.
    const ::std::string t_sourceFile = t_job.m_SourceFiles.at(0);

    // Set source emit info in job if requested
    if (g_emit_source) {
      t_job.m_OutputSrc = true;
      // We need to use the source file entry here because the input could be a
      // configuration file or a json file.
      t_job.m_OutSrcPath =
          fs::change_extension(t_sourceFile, ::std::string{"tuned"} +
                                                 fs::extension(t_sourceFile))
              .string();
    }

    // Set JSON emit info in job if requested
    if (g_emit_json) {
      t_job.m_OutputJson = true;
      t_job.m_OutJsonPath =
          fs::change_extension(t_sourceFile, "result.json").string();
    }

    if (t_job.m_Mode == atfc::mode::ocl) {
      // Inform user that we are working in OpenCL mode, if there were any
      // OpenCL directives
      post_diagnostic(
          message_type::note, "atfc",
          "input contains ocl directives, program is now in OpenCL mode");

      // If user asked us to emit a source file, inform him that's not possible
      // in OCL mode
      if (g_emit_source)
        post_diagnostic(
            message_type::warning, "atfc",
            "requested emit of source file, but is ignored in OpenCL mode");
    }

    // Create code generator object based on retrieved job information
    code_generator t_gen{t_job};

    // Generate code
    t_gen.generate_code(g_output);

    // Compile and execute tuner if not omitted
    if (!g_omit_tuning) {
      execute_tuner(t_job);

      post_empty();
      post_diagnostic(message_type::success, "atfc", "tuning completed");
    } else {
      post_empty();
      post_diagnostic(message_type::success, "atfc", "tuner generated");
    }
  } catch (...) {
    post_empty();
    post_diagnostic(message_type::error, "atfc", "compilation failed");
    ::std::exit(EXIT_FAILURE);
  }
}
