// This file is part of the Auto Tuning Framework (ATF).


#pragma once

#include <fstream>
#include <iostream>
#include <ostream>
#include <string>
#include <tuple>
#include <vector>

#include "job.hxx"

namespace atfc {
class code_generator {
  using tp_type = job::tp_type;
  using var_type = job::var_type;

public:
  code_generator(const job &p_job);
  ~code_generator() = default;

public:
  void generate_code(::std::ostream &p_stream);
  void generate_code(const ::std::string &p_path);

private:
  void insert_header(::std::ostream &p_stream);
  void insert_footer(::std::ostream &p_stream);
  void insert_source_gen(::std::ostream &p_stream);
  void insert_json_gen(::std::ostream &p_stream);
  void insert_json_func(::std::ostream &p_stream);
  void insert_kernel_read_func(::std::ostream &p_stream);
  void insert_tps(::std::ostream &p_stream);
  void insert_tp(::std::ostream &p_stream, const tp_type &p_tp);
  void insert_cf(::std::ostream &p_stream);
  void insert_cf_base(::std::ostream &p_stream);
  void insert_cf_ocl(::std::ostream &p_stream);
  void insert_cf_cuda(::std::ostream &p_stream);
  void insert_tune(::std::ostream &p_stream);
  void insert_convert_func(::std::ostream &p_stream);
  void insert_check_func(::std::ostream &p_stream);
  void insert_convert_call(::std::ostream &p_stream, const var_type &p_tp);
  void insert_vars(::std::ostream &p_stream);
  void insert_var(::std::ostream &p_stream, const var_type &p_var);
  void insert_argv_check(::std::ostream &p_stream, const var_type &p_var);

private:
  bool is_ocl_mode() const;
  bool is_cuda_mode() const;

private:
  job m_Job;
  bool m_NoConstraints{false};
};
} // namespace atfc
