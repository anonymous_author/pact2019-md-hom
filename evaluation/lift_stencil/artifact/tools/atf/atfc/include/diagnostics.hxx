// This file is part of the Auto Tuning Framework (ATF).


#pragma once

#include <exception>
#include <iostream>
#include <utility>

#include "console_color.hxx"
#include "string_view.hxx"

// TODO std::cerr
// TODO printf
// TODO stateful diagnostics

namespace atfc {
enum class message_type { error, warning, note, debug, success };

using position = ::std::pair<::std::size_t, ::std::size_t>;

// (start, len)
using caret = position;

namespace internal {
string_view get_line(string_view, ::std::size_t);
string_view get_type_str(message_type);
console_color get_type_clr(message_type);
void modify_counters(message_type);
} // namespace internal

void switch_debug(bool);

// 1 error and 2 warnings generated.
void post_summary();

void post_empty();

// internal compiler error
void post_ice(string_view p_sender, string_view p_message, bool p_abort = true);
void post_ice(string_view p_sender, const ::std::exception &p_ex,
              bool p_abort = true);

// atfc: error: bla
void post_diagnostic(message_type p_type, string_view p_sender,
                     string_view p_message);

// test.txt:1:4: error: bla
void post_diagnostic(message_type p_type, string_view p_sender, position p_pos,
                     string_view p_message);

// test.txt:1:4: error: bla
//     blablablabla
//	   ^~~~~
void post_diagnostic(message_type p_type, string_view p_sender, position p_pos,
                     caret p_caret, string_view p_src, string_view p_message);

// test.txt:1:4: error: bla
//     blablablabla
//	   ^~~~~
//	   bla
void post_diagnostic(message_type p_type, string_view p_sender, position p_pos,
                     caret p_caret, string_view p_src, string_view p_fix,
                     string_view p_message);
} // namespace atfc
