#!/bin/sh

# Determine if we should mute the tuners output or not
first_arg="$1"

if [[ $first_arg != 0 ]]
then	
	exec > tuner.log
	exec 2>&1
fi

# Remove first argument from argument list
shift

# Call tuner with rest
./tuner "$@"

# Report exit code
exit $?
