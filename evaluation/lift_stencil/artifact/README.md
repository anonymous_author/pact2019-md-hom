# Lift Stencil Artifact for CGO 2018
This repository presents the artifact and workflows to supplement the paper ["High Performance Stencil Code Generation With Lift"](https://gitlab.com/larisa.stoltzfus/liftstencil-cgo2018-artifact/raw/master/high-performance-stencil-code-generation-with-lift.pdf) to be presented at the [International Symposium on Code Generation and Optimization](http://cgo.org/cgo2018/) in 2018.

This artifact is a rather complicated setup with requires an OpenCL-enabled GPU as well as many software dependencies.
The setup, installation and running of the workflows involved with this artefact are outlined below and assume the user is using a **Debian** flavoured distribution of Linux.

**If you have any trouble installing the artifact or performing the evaulation, please create a [new issue](https://gitlab.com/larisa.stoltzfus/liftstencil-cgo2018-artifact/issues/new) on this repository or contact the [authors](#authors) directly via email.**

## Table of Content
* [List of hardware requirements](#hardware-requirements)
* [List of software requirements](#software-requirements)
* [Installation instructions](#install)
* [OpenCL configuration](#opencl-configuration)
* [Workflows to evaluate the artifact](#workflows) 
* [Reuse and Repurposing](#reuse-and-repurposing)

## Hardware Requirements
To evaluate this artifact you will need an OpenCL-enabled GPU.

## Software Requirements

The following software is required to run through the scripted workflows below.
There are instructions how to install this requirements below.
 
* git
* [git-lfs](https://git-lfs.github.com/)
* [R](https://www.r-project.org/) (including: data.table, plyr, reshape2, ggplot2)
* OpenCL
* [Lift](https://github.com/lift-project/lift); which requires:
    * Java 8 SDK
    * SBT
    * cmake (3.8+)
    * Python 2.7
* [OpenTuner](https://github.com/jansel/opentuner); which requires:
    * pip 2.7
    * sqlite (3.0)
* Auto Tuning Framework (ATF); which requires:
    * gcc (5.4+)
    * cmake (3.8+)
    * Boost (1.56+)
* [PPCG](http://ppcg.gforge.inria.fr/); which requires
    * clang / LLVM (3.8+)
    * libtool
    * libgmp3
    * libedit
    * pkg-config
* Stencil benchmarks from [SHOC](https://github.com/vetter/shoc) and other sources


## Install

On an Ubuntu (>= 14.04) system the following packages should satisfy the software requirements.

### Install git and git-lfs:
```
sudo apt-get install git curl wget
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
git lfs install
```

### Clone the Repository
1. Make sure that you do this step only **after** you have installed **git and git-lfs**!
  ```
  git clone https://gitlab.com/larisa.stoltzfus/liftstencil-cgo2018-artifact.git
  ```
  
2. Change directory to enter the repository:
  ```
  cd liftstencil-cgo2018-artifact
  ```
  
3. Initialize environment variables:
  ```
  source environment.env
  ```
From here on out `$ROOTDIR` will refer to the path where this repository resides.

### Install R and packages
* For **Ubunutu 17.04 and newer** it is sufficient to install these packages:
  ```
  sudo apt-get install r-base r-cran-plyr r-cran-data.table r-cran-reshape2 r-cran-ggplot2
  ```
  
* For **Ubuntu 16.04 or 16.10**:
  1. Install these packages: `sudo apt-get install r-base r-cran-plyr r-cran-reshape2 r-cran-ggplot2`
  2. and then install the `data.table` manually by opening an `R` shell and type: `install.packages("data.tables")`
  3. select a download mirror and finish the installation.


### Install OpenCL
The installation of OpenCL depends very heavy on your hardware.
Maybe you have already an OpenCL ready device installed.

* You can test if you have a working OpenCL installation by installing the `clinfo` tool:
```
sudo apt-get install clinfo
```
  and running it:
```
clinfo
```
  It the output reports 1 or more platforms and 1 or more devices than you have a working OpenCL installation.

* To install OpenCL for an **Nvidia GPU** install:
```
sudo apt-get install opencl-headers ocl-icd-opencl-dev ocl-icd-libopencl1 nvidia-opencl-icd-384
```

* For other hardware see the excelent instructions from Andreas Kloeckner: https://wiki.tiker.net/OpenCLHowTo

### Install Lift
Lift requires the Java 8 SDK, the Scala Build Tool (SBT), and cmake (3.8+).

1. To install the Java 8 SDK:
  ```
  sudo apt-get install openjdk-8-jdk wget
  ```
  Alternatively follow these instructions to install the Oracle Java 8 SDK: https://medium.com/coderscorner/installing-oracle-java-8-in-ubuntu-16-10-845507b13343

2. To install SBT:
  ```
  $ROOTDIR/scripts/installation/build_sbt.sh
  ```
  
3. To install cmake execute the provided installation script:
  ```
  $ROOTDIR/scripts/installation/build_cmake.sh
  ```
4. Install python:
  ```
  sudo apt-get install python
  ```
  
5. To install Lift itself:
  ```
  $ROOTDIR/scripts/installation/build_lift.sh
  ```


### Install OpenTuner
1. Install pip, and sqlite (3):
  ```
  sudo apt-get install python-pip sqlite3 libsqlite3-dev
  ```

2. Then install OpenTuner itself:
  * at the user level: `pip install --user opentuner`
  * or system wide: `sudo pip install opentuner`

More information are available at: https://github.com/jansel/opentuner

### Install ATF
The Auto Tuning Framework requires GCC, and Boost 1.56+.

1. To install GCC:
  ```
  sudo apt-get install gcc g++
  ```
  
2. To install boost:
  ```
  sudo apt-get install libboost-all-dev
  ```

3. To install the ATF itself:
  ```
  $ROOTDIR/scripts/installation/build_atf.sh
  ```
  
We also provide scripts which might help building `gcc-7.1.0` and `boost` from source in the `$ROOTDIR/scripts/installation` folder of this repository.

### Install PPCG
1. Install CLANG and LLVM 3.8+:
  ```
  sudo apt-get install clang-3.8 llvm-3.8-dev libclang-3.8-dev
  ```
  
2. Install libtool, libgmp3, libedit, and pkg-config:
  ```
  sudo apt-get install libtool libgmp3-dev libedit-dev pkg-config
  ```
  
3. Install PPCG itself:
  ```
  $ROOTDIR/scripts/installation/build_ppcg.sh
  ```

### Install Stencil benchmarks
```
$ROOTDIR/scripts/installation/build_benchmarks.sh
```


## OpenCL Configuration

There are two environment variables (`OCL_PLATFORM_ID` and `OCL_DEVICE_ID`) defined in `environment.env` which reflect which OpenCL device to use
* `OCL_PLATFORM_ID` defines which OpenCL platform to use 
* `OCL_DEVICE_ID` defines which OpenCL device to use 

If you want to use a devince other than the first device of the first platform please update these variables in `environment.env` and reload the file via: `source environment.env`


## Workflows

There are two workflows to validate this artifact:
1. Reproduce the results reported in Figures 7 and 8 of our paper by re-running the benchmarks with the OpenCL kernel and parameters we found in a time-intensive auto tuning process.
2. Regenerate the kernels generated by our _Lift_ compiler and then perform the time-intensive autotuning process using the Auto Tuning Framework (ATF).
 
We expect evaluators of this artifact to perform the first workflow fully (on one or more architectures).
We included the scripts to perform the second workflow and invite evaluators to perform this workflow to see how it works, but we do not expect evaluators to perform the same experiment as we did for the paper as this takes multiple days to complete.

### 1. Workflow: Reproduce Figures 7 and 8

Our experiments have been conducted using three different GPUs (referred to as Nvidia, AMD, ARM).
The experimental setup we used is explained in detail in Section 6 of the paper.

This workflow should take **not more than 1 hour** per platform.

* Preparation: To reproduce the our best found results:
	* Decide for which platform (`nvidia`, `amd` or `arm`) you like to rerun the benchmarks. This choice should match the hardware in your system.
	* Make sure that `OCL_PLATFORM_ID` and `OCL_DEVICE_ID` in `environment.env` are configured to use the desired OpenCL platform and device. You can use the `clinfo` program to identify the order of platforms and devices on your system.
	* You can change the number of `ITERATIONS` performed for each benchmark in `environment.env`. We report median numbers and the default `ITERATIONS` value is 2.

* To reproduce the results reported in _Figure 7_:
    * Run `$ROOTDIR/scripts/run_workflow1_figure7.sh <PLATFORM>`.
	* The generated plots can be found in `$ROOTDIR/plots/workflow1-figure7.pdf`
	* This experiment executes `6 * 2 * ITERATIONS` (benchmarks * (lift+reference) * iterations) many OpenCL kernels.

* To reproduce the results reported in _Figure 8_:
    * Run `$ROOTDIR/scripts/run_workflow1_figure8.sh <PLATFORM>` 
	* The generated plot can be found in `$ROOTDIR/plots/workflow1-figure8.pdf`
	* This experiment executes `8 * 2 * 2 * ITERATIONS` (benchmarks * input_size * (lift+ppcg) * iterations) many OpenCL kernels.

This workflow was fairly quickly executable since we only reran the best versions of the kernels we generated. 
In the following, we explain how to completely redo the experiments, which requires significantly more time.

### 2. Workflow

#### 2a: Lift Kernel Generation

In the previous workflow, only the best found Lift-generated OpenCL kernels for each benchmark have been executed.
In this workflow we regenerate the OpenCL kernels with Lift.
These can then be retuned for the current system as described in the second [step of this workflow](#2b-retrieve-performance-results-using-atf-for-selected-benchmarks).

1. Before generating the kernels, we need to decide on the maximum time we allow for tuning a single benchmark.
   <br><br>
   In the paper we tuned each benchmark for a maximum of three hours, this leads to a very long auto-tuning time of multiple days for all benchmarks.
   Our Lift compiler generates multiple OpenCL kernels for a single benchmark.
   The tuning time limit for the benchmark is evenly divided for each of the generated OpenCL kernel.
   For example, if we generate three OpenCL kernels for a single benchmark with an overall time limit of three hours, each OpenCL kernel is tuned for a maximum of one hour.
   <br><br>
   The maximum tuning time can be changed using `$ROOTDIR/scripts/utils/updateTimeout.sh <TIME_IN_SECONDS>`. 
   For this artifact, we set the **default maximum tuning time to 60s per benchmark**.
   This is not sufficient for properly tuning the generated OpenCL kernels but allows to verify the functionality of the workflow in a reasonable timeframe.
   <br><br>
   Note that the auto-tuning process comprises a non-deterministic search space exploration of the parameter space and, therefore, reducing the maximum tuning time significantly influences the results of the auto-tuning.

2. To regenerate all _Lift_ kernels evaluated in our experiments run the following script:
   ```
   $ROOTDIR/scripts/run_workflow2_kernel_generation.sh
   ```
   
This script will generate OpenCL kernels for each of the benchmarks mentioned in Table 1 of the paper.
The _Lift_ input files can be found using this command:
```
find $ROOTDIR/benchmarks -name "*.lift"
```
To compile a `bench.lift` file to OpenCL kernels, the script above performs the following steps:
 1. Algorithmic Rewriting (intermediate files are stored in a folder called `bench`)
 2. Low-Level Rewriting (intermediate files are stored in  a folder called `benchLower`)
 3. OpenCL Code Generation (kernels are stored in a folder called `benchCl`)
   
The generated OpenCL kernels will be stored in these two folders:
```
$ROOTDIR/benchmarks/figure8/workflow2
$ROOTDIR/benchmarks/figure7/workflow2
```
and can be listed via the following command executed in one of these directories:
```
find . -name "*.cl"
```
The generated kernels can now be compared to the ones used in workflow 1.
For example the directory `$ROOTDIR/benchmarks/figure8/workflow2/gaussian/big/kepler/gaussianCl` should now contain the following kernels:
```
06cec1425929ffa1528e0dfb51d6bf7b0ae826c77cfe2d7911c5d2cdc261e86a.cl
87d266939edd240dccde5cff86cf5a5c77e2ec456b00e03a6097670736707603.cl
9f6f6c64373c1b47bb1b00c340d4f9f147e4d04c094f43c29825d778261ac25e.cl
a69c9864ce394d6d6ce7fa3f03df00c1723a7fbd06a2b66b0a47105afc25e858.cl
```
The directory used in the last workflow (`$ROOTDIR/benchmarks/figure8/workflow1/gaussian/big/kepler/threeHours-gaussianCl`) contains the same kernels plus a `summary.txt` file which reports the tuning results and best found configuration for each kernel:
```
$ cat $ROOTDIR/benchmarks/figure8/workflow1/gaussian/big/kepler/threeHours-gaussianCl/summary.txt
06cec1425929ffa1528e0dfb51d6bf7b0ae826c77cfe2d7911c5d2cdc261e86a.cl -> 25567968 (8;8192;8;128;356;8188;25567968)
87d266939edd240dccde5cff86cf5a5c77e2ec456b00e03a6097670736707603.cl -> 11492320 (8192;8192;8;16;11492320)
9f6f6c64373c1b47bb1b00c340d4f9f147e4d04c094f43c29825d778261ac25e.cl -> 9297888 (1024;8192;32;4;4;89;9297888)
a69c9864ce394d6d6ce7fa3f03df00c1723a7fbd06a2b66b0a47105afc25e858.cl -> 7092000 (2048;8192;32;4;1;2;7092000)
```

#### 2b: Retrieve Performance Results Using ATF for Selected Benchmarks

As the second step of the second workflow, we provide scripts to measure perfomance numbers according to the methodology described in the paper.
This comprises performing auto-tuning of the _Lift_ and PPCG generated OpenCL kernels using the Auto-Tuning Framework (ATF).

For time reasons, we strongly advice to only tune selected benchmarks as it takes several days to retrieve performance numbers for all benchmarks this way.

To tune a specific benchmark of Figure 8, execute the following script:
```
$ROOTDIR/scripts/run_workflow2_figure8.sh <ARCHITECTURE> <BENCHMARK> <INPUT_SIZE>
```
e.g.,:
```
$ROOTDIR/scripts/run_workflow2_figure8.sh nvidia grad2d small
```

To tune a specific benchmark of Figure 7, execute the following script:
```
$ROOTDIR/scripts/run_workflow2_figure7.sh <ARCHITECTURE> <BENCHMARK> 
```
e.g.,:
```
$ROOTDIR/scripts/run_workflow2_figure8.sh nvidia srad2
```


The generated plots can be found in `$ROOTDIR/plots/workflow2-figure7.pdf` and `$ROOTDIR/plots/workflow2-figure8.pdf`

If multiple benchmarks are tuned one after each other, the plot will be extended and show the results of all tuned results so far.

##### Auto-tuning of the Mali GPU

The tuning we performed to get the performance results on the Mali GPU reported in Figure 8 follows a more complex approach.
The PPCG compiler needs to be invoked for each parameter configuration that is tested during the auto-tuning because it creates a specialised OpenCL kernel.
Performing the polyhedral compilation on the development board significantly limits the number of configurations that can be tested in the maximum tuning time.
Therefore, for our paper we used a dedicated server which performed the PPCG compilation while only the OpenCL kernel compilation was performed on the Odroid development board.

The tuning process for the Mali GPU was performed by:

1. For each configuration, a specialised OpenCL kernel was created using the PPCG compiler on the server.
2. The created kernel and current configuration was sent to the Odroid board via SSH and executed using a precompiled OpenCL host code.
3. The runtime was reported back to the server and used by the ATF to provide the next configuration to be tested.

Since this setup is quite complex to set up and not as easily scriptable, e.g., it requires SSH key generation and exchange to allow SSH connections without login.
If you want to obtain results on the Mali GPU using this methodolgy, please contact the authors and we will provide help for preparing and executing this tuning setup on your machine.

### Reuse and Repurposing
We created this artifact in a modular way to faciliate using our results as a comparison point for future research.
Here we describe the organisation of our scripts and where the performance results are stored.

Each of our workflow scripts is divided into three parts which can be executed on their own:
1. Run the _PPCG_ and handwritten reference implementations
2. Run the _Lift_ generated implementations
3. Plot the performance results

To only execute the _Lift_ generated implementations on the benchmarks used in Figure 8, one could run `$ROOTDIR/scripts/utils/run_workflow1_figure8_lift.sh`.
Generally, all workflow scripts come with the extension `_lift`, `_ref` and `_plot`.

The scripts for Figure 7 create files containing the measured runtimes in nanoseconds named after their associated benchmark, e.g., `hotspot.out`.
These are stored in the folders `lift` and `ref` and can be found using:
```
cd $ROOTDIR
find . -name "*.out"
```

The scripts for Figure 8 create files `lift.runtime` and `ref.runtime` containing the runtime in nanoseconds and are stored in the folders of the associated benchmark.
These can be found using:
```
cd $ROOTDIR
find . -name "*.runtime"
```

## Authors
* Bastian Hagedorn, University of Muenster (<mailto:b.hagedorn@wwu.de>)
* Larisa Stoltzfus, University of Edinburgh (<mailto:larisa.stoltzfus@ed.ac.uk>)
* Michel Steuwer, University of Glasgow (<mailto:michel.steuwer@glasgow.ac.uk>)
* Sergei Gorlatch, University of Muenster (<mailto:gorlatch@wwu.de>)
* Christophe Dubach, University of Edinburgh (<mailto:christophe.dubach@ed.ac.uk>)

## Acknowledgements
We thank the entire [Lift team](http://www.lift-project.org/index.html#team) for their development efforts on the Lift compiler.

We thank [Prashant Singh Rawat](http://web.cse.ohio-state.edu/~rawat.15) from Ohio State University for his effort in helping us on the comparison with the PPCG compiler.

We thank [Ari Rasch](https://www.uni-muenster.de/PVS/en/mitarbeiter/rasch.html) from the University of Muenster for allowing us to use his ATF framework.

We appreachiate the support the first author received in form of a [HiPEAC collaboration grant](https://www.hipeac.net/mobility/collaborations/) which made this collaborative work possible.
