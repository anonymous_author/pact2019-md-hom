cmake_minimum_required(VERSION 2.8.11)
project(mkl_jit)

set(CMAKE_CXX_STANDARD 14)

# MKL
find_package(MKL REQUIRED)
include_directories(${MKL_INCLUDE_DIR})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -L${MKL_LIBRARY_DIR} -L$ENV{ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/ -Wl,--no-as-needed")

add_executable(mkl_jit_gemm gemm.cpp)
target_include_directories(mkl_jit_gemm PUBLIC ${ARGPARSE_INCLUDE_DIR})
target_link_libraries(mkl_jit_gemm
        mkl_intel_ilp64
        mkl_intel_thread
        mkl_core
        iomp5
        pthread
        m
        dl)