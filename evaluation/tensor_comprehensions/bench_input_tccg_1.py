# tccg-1
import time
import tensor_comprehensions as tc
import torch
import os
import sys
torch.cuda.device(os.environ['CUDA_GPU_DEVICE_ID'])
if (not os.path.isfile("{}/results/gpu/tensor_comprehensions/tc_abcdef_gdab_efgc.cuda".format(os.environ["ARTIFACT_ROOT"]))) or \
		(not os.path.isfile("{}/results/gpu/tensor_comprehensions/tc_abcdef_gdab_efgc.options".format(os.environ["ARTIFACT_ROOT"]))):
	print("Unable to benchmark Tensor Comprehensions. Has it not yet been tuned for this input size?")
	sys.exit(1)

lang = """
def tccg1(float(G, D, A, B) L, float(E, F, G, C) R) -> (O) {
	O(a, b, c, d, e, f) +=! L(g, d, a, b) * R(e, f, g, c)
}
"""

print ("tccg-40: O(a, b, c, d, e, f) +=! L(g, d, a, b) * R(e, f, g, c)")
print ("(a,b,c,d,e,f,g) = (24,16,16,16,24,16,24)")
print ("# of Operations: ", 24*16*16*16*24*16*24)

matmul = tc.define(lang, cache="{}/results/gpu/tensor_comprehensions/tc_abcdef_gdab_efgc".format(os.environ["ARTIFACT_ROOT"]), name="tccg1")
mat1, mat2 = torch.randn(24, 16, 24, 16).cuda(), torch.randn(24, 16, 24, 16).cuda()
out = matmul(mat1, mat2)
torch.cuda.synchronize()
# warm ups
for i in range(0, 10):
	out = matmul(mat1, mat2)
#evaluations
times = []
for i in range(0, 200):
	start = time.process_time()
	out = matmul(mat1, mat2)
	torch.cuda.synchronize()
	end = time.process_time()
	times.append(int((end-start) * 1000000000))
torch.cuda.synchronize()

print ("time: ", min(times))
with open("{}/results/gpu/tensor_comprehensions/tc_abcdef_gdab_efgc_runtime".format(os.environ["ARTIFACT_ROOT"]), 'w') as file:
	file.write(str(min(times) / 1000000.0))