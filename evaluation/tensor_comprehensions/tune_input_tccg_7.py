# tccg-7
import time
import tensor_comprehensions as tc
import torch
import os
torch.cuda.device(os.environ['CUDA_GPU_DEVICE_ID'])

lang = """
def tccg7(float(G, F, A, B) L, float(D, E, G, C) R) -> (O) {
	O(a, b, c, d, e, f) +=! L(g, f, a, b) * R(d, e, g, c)
}
"""

print ("tccg-46: O(a, b, c, d, e, f) +=! L(g, f, a, b) * R(d, e, g, c)")
print ("(a,b,c,d,e,f,g) = (24,16,16,24,16,16,24)")
print ("# of Operations: ", 24*16*16*16*24*16*24)

matmul = tc.define(lang, name="tccg7")
mat1, mat2 = torch.randn(24, 16, 24, 16).cuda(), torch.randn(24, 16, 24, 16).cuda()
settings = {
"threads": 8, "generations": 1, "pop_size": 100, "number_elites": 10
}
torch.cuda.synchronize()
start = time.process_time()
matmul.autotune(mat1, mat2, cache="{}/results/gpu/tensor_comprehensions/tc_abcdef_gfab_degc".format(os.environ["ARTIFACT_ROOT"]),  **settings)
torch.cuda.synchronize()
end = time.process_time()
print ("autotune: ", end - start)