# tccg-4
import time
import tensor_comprehensions as tc
import torch
import os
torch.cuda.device(os.environ['CUDA_GPU_DEVICE_ID'])

lang = """
def tccg4(float(G, E, A, B) L, float(D, F, G, C) R) -> (O) {
	O(a, b, c, d, e, f) +=! L(g, e, a, b) * R(d, f, g, c)
}
"""

print ("tccg-43: O(a, b, c, d, e, f) +=! L(g, e, a, b) * R(d, f, g, c)")
print ("(a,b,c,d,e,f,g) = (24,16,16,24,16,16,24)")
print ("# of Operations: ", 24*16*16*16*24*16*24)

matmul = tc.define(lang, name="tccg4")
mat1, mat2 = torch.randn(24, 16, 24, 16).cuda(), torch.randn(24, 16, 24, 16).cuda()
settings = {
"threads": 8, "generations": 1, "pop_size": 100, "number_elites": 10
}
torch.cuda.synchronize()
start = time.process_time()
matmul.autotune(mat1, mat2, cache="{}/results/gpu/tensor_comprehensions/tc_abcdef_geab_dfgc".format(os.environ["ARTIFACT_ROOT"]),  **settings)
torch.cuda.synchronize()
end = time.process_time()
print ("autotune: ", end - start)