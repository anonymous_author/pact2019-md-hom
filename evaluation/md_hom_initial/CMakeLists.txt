cmake_minimum_required(VERSION 2.8.11)
project(md_hom_initial)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ftemplate-depth=1024")

# OpenCL
find_package(OpenCL REQUIRED)
include_directories(${OpenCL_INCLUDE_DIR})
link_directories(${OpenCL_LIBRARY})

# Python
find_package(PythonLibs 2.7 REQUIRED)
include_directories(${PYTHON_INCLUDE_DIR})
link_directories(${PYTHON_LIBRARY})

add_executable(md_hom_initial_gemm gemm.cpp)
target_include_directories(md_hom_initial_gemm PUBLIC ${ATF_INCLUDE_DIRS})
target_link_libraries(md_hom_initial_gemm atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(md_hom_initial_gemv gemv.cpp)
target_include_directories(md_hom_initial_gemv PUBLIC ${ATF_INCLUDE_DIRS})
target_link_libraries(md_hom_initial_gemv atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(md_hom_initial_gaussian_static gaussian.cpp)
target_include_directories(md_hom_initial_gaussian_static PUBLIC ${ATF_INCLUDE_DIRS})
target_link_libraries(md_hom_initial_gaussian_static atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(md_hom_initial_gaussian_dynamic gaussian.cpp)
target_include_directories(md_hom_initial_gaussian_dynamic PUBLIC ${ATF_INCLUDE_DIRS})
target_compile_definitions(md_hom_initial_gaussian_dynamic PUBLIC DYNAMIC_WEIGHTS)
target_link_libraries(md_hom_initial_gaussian_dynamic atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(md_hom_initial_j3d7pt j3d7pt.cpp)
target_include_directories(md_hom_initial_j3d7pt PUBLIC ${ATF_INCLUDE_DIRS})
target_link_libraries(md_hom_initial_j3d7pt atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(md_hom_initial_rl rl.cpp)
target_include_directories(md_hom_initial_rl PUBLIC ${ATF_RL_INCLUDE_DIRS})
target_link_libraries(md_hom_initial_rl atf_rl ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)

add_executable(md_hom_initial_tc tc.cpp)
target_include_directories(md_hom_initial_tc PUBLIC ${ATF_INCLUDE_DIRS})
target_link_libraries(md_hom_initial_tc atf ${OpenCL_LIBRARY} ${PYTHON_LIBRARY} pthread)