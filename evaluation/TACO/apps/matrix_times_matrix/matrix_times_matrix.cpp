#include <chrono>
#include <iostream>
#include <argparse.hpp>
#include <fstream>
#include <cstdlib>
#include "taco.h"

using namespace taco;

int main(int argc, const char* argv[]) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--input-size",  3, false);
    args.parse(static_cast<int>(argc), argv);
    std::vector<int> input_size  = args.retrieve_int_vector("input-size");
    const int M = input_size[0], N = input_size[1], K = input_size[2];

    // Create formats
    Format rm({Dense,Dense});

    // Create tensors
    Tensor<float> A({M,K}, rm); A.pack();
    Tensor<float> B({K,N}, rm); B.pack();
    Tensor<float> C({M,N}, rm); C.pack();

    // Form a tensor-vector multiplication expression
    IndexVar i, j, k;
    C(i,j) = A(i,k) * B(k,j);

    // Compile the expression
    C.compile();

    // Assemble A's indices and numerically compute the result
    C.assemble();
    std::cout << std::endl << "benchmarking TACO..." << std::endl;
    for (int i = 0; i < 10; ++i) {
        C.compute();
    }
    auto min_time = std::numeric_limits<long>::max();
    for (int i = 0; i < 200; ++i) {
        auto start = std::chrono::high_resolution_clock::now();
        C.compute();
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
        min_time = std::min(min_time, duration);
    }

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/cpu/taco/");
    runtime_file_name.append("gemm_");
    runtime_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << min_time / 1000000.0f;
    runtime_file.close();
}
