package ir.ast

import ir.interpreter.Interpreter._
import ir.{ArrayType, Type, TypeException}
import sun.reflect.generics.reflectiveObjects.NotImplementedException

case class UnPadEmpty(left: Int, right: Int)
  extends Pattern(arity = 1) with isGenerable {
  override def checkType(argType: Type,
                         setType: Boolean): Type = {
    argType match {
      case ArrayType(t, n) => ArrayType(t, n - left - right)
      case _ => throw new TypeException(argType, "ArrayType")
    }
  }

  override def eval(valueMap: ValueMap, args: Any*): Vector[_] = {
    assert(args.length == arity)
    args.head match {
      case a: Vector[_] => throw new NotImplementedException()
    }
  }
}