package cgo_artifact

import java.io.{File, PrintWriter}

import apart.arithmetic.SizeVar
import ir._
import ir.ast._
import ir.printer.DotPrinter
import opencl.executor.Compile
import opencl.ir._
import opencl.ir.pattern._
import rewriting.utils.Utils

import scala.sys.process._

object Listing1 {
  def main(args: Array[String]): Unit = {
    val N = SizeVar("N")

    val partialDot = \(
      ArrayType(Float, N),
      ArrayType(Float, N),
      (x, y) => Join() o MapWrg(

        Join() o toGlobal(MapLcl(MapSeq(id))) o Split(1) o

          Iterate(6)(Join() o
            MapLcl(toLocal(MapSeq(id)) o
            ReduceSeq(add, 0.0f)) o
            Split(2)) o

          Join() o MapLcl(toLocal(MapSeq(id)) o
            ReduceSeq(\((acc, next) => multAndSumUp(acc, next._0, next._1)), 0.0f)
        ) o Split(2)
      ) o Split(128) $ Zip(x, y)
    )

    val code = Compile(partialDot)

    Utils.dumpToFile(code, "partialDot.cl", ".")

    new DotPrinter(new PrintWriter(new File("partialDot.dot"))).print(partialDot)
    s"dot -Tpdf partialDot.dot -o partialDot.pdf".!
  }
}
