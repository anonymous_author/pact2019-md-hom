cmake_minimum_required(VERSION 2.8.11)
project(cublaslt)

set(CMAKE_CXX_STANDARD 14)

# cuBLASLt
find_package(CUDA REQUIRED)
include_directories(${CUDA_INCLUDE_DIRS})

add_executable(cublaslt_gemm gemm.cpp)
target_include_directories(cublaslt_gemm PUBLIC ${ARGPARSE_INCLUDE_DIR})
target_link_libraries(cublaslt_gemm ${CUDA_CUDART_LIBRARY} ${CUDA_cublas_LIBRARY} cublasLt)