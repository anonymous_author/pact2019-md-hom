#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <cuda_runtime.h>
#include <cublasLt.h>
#include <fstream>

#define CUDA_SAFE_CALL(x)                                   \
do {                                                        \
    cudaError_t cuda_call_result = x;                       \
    if (cuda_call_result != cudaSuccess) {                  \
        std::cerr << "error: " #x " failed with error "     \
                  << cudaGetErrorString(cuda_call_result);  \
        exit(1);                                            \
    }                                                       \
} while(false)

const char *cublasGetErrorString(cublasStatus_t status) {
    switch (status) {
        case CUBLAS_STATUS_SUCCESS:
            return "CUBLAS_STATUS_SUCCESS";
        case CUBLAS_STATUS_NOT_INITIALIZED:
            return "CUBLAS_STATUS_NOT_INITIALIZED";
        case CUBLAS_STATUS_ALLOC_FAILED:
            return "CUBLAS_STATUS_ALLOC_FAILED";
        case CUBLAS_STATUS_INVALID_VALUE:
            return "CUBLAS_STATUS_INVALID_VALUE";
        case CUBLAS_STATUS_ARCH_MISMATCH:
            return "CUBLAS_STATUS_ARCH_MISMATCH";
        case CUBLAS_STATUS_MAPPING_ERROR:
            return "CUBLAS_STATUS_MAPPING_ERROR";
        case CUBLAS_STATUS_EXECUTION_FAILED:
            return "CUBLAS_STATUS_EXECUTION_FAILED";
        case CUBLAS_STATUS_INTERNAL_ERROR:
            return "CUBLAS_STATUS_INTERNAL_ERROR";
        case CUBLAS_STATUS_NOT_SUPPORTED:
            return "CUBLAS_STATUS_NOT_SUPPORTED";
        case CUBLAS_STATUS_LICENSE_ERROR:
            return "CUBLAS_STATUS_LICENSE_ERROR";
    }
    return "unknown error";
}

#define CUBLAS_SAFE_CALL(x)                                 \
do {                                                        \
    cublasStatus_t cublas_call_result = x;                  \
    if (cublas_call_result != CUBLAS_STATUS_SUCCESS) {      \
        std::stringstream error;                            \
        error << "error: " #x " failed with error ";        \
        error << cublasGetErrorString(cublas_call_result);  \
        std::cerr << error.str() << "\n";                   \
        exit(1);                                            \
    }                                                       \
} while(false)

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--device-id",   1, false);
    args.addArgument("--input-size",  3, false);
    args.parse(static_cast<size_t>(argc), argv);
    const size_t device_id          = args.retrieve_size_t("device-id");
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t M = input_size[0], N = input_size[1], K = input_size[2];

    // set device
    CUDA_SAFE_CALL(cudaSetDevice(device_id));

    // create cuBLASLt handle
    cublasLtHandle_t handle;
    CUBLAS_SAFE_CALL(cublasLtCreate(&handle));

    // prepare inputs
    float *dev_a;
    float *dev_b;
    float *dev_c;
    void *workspace;
    size_t workspaceSize = std::atoll(std::getenv("CUBLASLT_WORKSPACE_SIZE"));
    std::vector<float> a(M * K); for (int i = 0; i < a.size(); ++i) a[i] = (i % 100) + 1;
    std::vector<float> b(K * N); for (int i = 0; i < b.size(); ++i) b[i] = (i % 100) + 1;
    CUDA_SAFE_CALL(cudaMalloc(&dev_a, a.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMalloc(&dev_b, b.size() * sizeof(float)));
    CUDA_SAFE_CALL(cudaMalloc(&dev_c, M * N * sizeof(float)));
    CUDA_SAFE_CALL(cudaMalloc(&workspace, workspaceSize));
    CUBLAS_SAFE_CALL(cublasSetMatrix(M, K, sizeof(float), a.data(), M, dev_a, M));
    CUBLAS_SAFE_CALL(cublasSetMatrix(K, N, sizeof(float), b.data(), K, dev_b, K));
    CUDA_SAFE_CALL(cudaDeviceSynchronize());

    // prepare API call
    cublasLtMatmulDesc_t operationDesc = NULL;
    cublasLtMatrixLayout_t Adesc = NULL;
    cublasLtMatrixLayout_t Bdesc = NULL;
    cublasLtMatrixLayout_t Cdesc = NULL;
    cublasLtMatmulPreference_t preference = NULL;
    cublasLtMatmulHeuristicResult_t heuristicResult[1000] = {};
    CUBLAS_SAFE_CALL(cublasLtMatmulDescCreate(&operationDesc, CUDA_R_32F));
    CUBLAS_SAFE_CALL(cublasLtMatrixLayoutCreate(&Adesc, CUDA_R_32F, N, K, N));
    CUBLAS_SAFE_CALL(cublasLtMatrixLayoutCreate(&Bdesc, CUDA_R_32F, K, M, K));
    CUBLAS_SAFE_CALL(cublasLtMatrixLayoutCreate(&Cdesc, CUDA_R_32F, N, M, N));
    CUBLAS_SAFE_CALL(cublasLtMatmulPreferenceCreate(&preference));
    CUBLAS_SAFE_CALL(cublasLtMatmulPreferenceSetAttribute(preference, CUBLASLT_MATMUL_PREF_MAX_WORKSPACE_BYTES, &workspaceSize, sizeof(workspaceSize)));
    int returnedResults = 0;
    CUBLAS_SAFE_CALL(cublasLtMatmulAlgoGetHeuristic(handle, operationDesc, Adesc, Bdesc, Cdesc, Cdesc, preference, 1000, heuristicResult, &returnedResults));

    // benchmark API call
    std::cout << std::endl << "benchmarking NVIDIA cuBLASLt..." << std::endl;
    float runtime = std::numeric_limits<float>::max();
    for (int algorithm = 0; algorithm < returnedResults; ++algorithm) {
        bool next_algo = false;
        for (int i = 0; i < 10; ++i) {
            float ALPHA = 1.0f;
            float BETA = 0.0f;
            auto status = cublasLtMatmul(handle,
                                         operationDesc,
                                         &ALPHA,
                                         dev_a,
                                         Adesc,
                                         dev_b,
                                         Bdesc,
                                         &BETA,
                                         dev_c,
                                         Cdesc,
                                         dev_c,
                                         Cdesc,
                                         &heuristicResult[algorithm].algo,
                                         workspace,
                                         workspaceSize,
                                         0);
            if (status == CUBLAS_STATUS_INVALID_VALUE || status == CUBLAS_STATUS_NOT_SUPPORTED || status == CUBLAS_STATUS_ARCH_MISMATCH || status == CUBLAS_STATUS_EXECUTION_FAILED) {
                next_algo = true;
                break;
            } else {
                CUBLAS_SAFE_CALL(status);
            }
        }
        if (next_algo) continue;
        for (int i = 0; i < 200; ++i) {
            cudaEvent_t start, stop;
            CUDA_SAFE_CALL(cudaEventCreate(&start));
            CUDA_SAFE_CALL(cudaEventCreate(&stop));
            CUDA_SAFE_CALL(cudaEventRecord(start));
            float ALPHA = 1.0f;
            float BETA = 0.0f;
            CUBLAS_SAFE_CALL(cublasLtMatmul(handle,
                                            operationDesc,
                                            &ALPHA,
                                            dev_a,
                                            Adesc,
                                            dev_b,
                                            Bdesc,
                                            &BETA,
                                            dev_c,
                                            Cdesc,
                                            dev_c,
                                            Cdesc,
                                            &heuristicResult[algorithm].algo,
                                            workspace,
                                            workspaceSize,
                                            0));
            CUDA_SAFE_CALL(cudaEventRecord(stop));
            CUDA_SAFE_CALL(cudaEventSynchronize(stop));
            float tmp = 0;
            CUDA_SAFE_CALL(cudaEventElapsedTime(&tmp, start, stop));
            runtime = std::min(runtime, tmp);
            CUDA_SAFE_CALL(cudaEventDestroy(start));
            CUDA_SAFE_CALL(cudaEventDestroy(stop));
        }
    }
    CUDA_SAFE_CALL(cudaFree(dev_a));
    CUDA_SAFE_CALL(cudaFree(dev_b));
    CUDA_SAFE_CALL(cudaFree(dev_c));

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/gpu/cublaslt/");
    runtime_file_name.append("gemm_");
    runtime_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << runtime;
    runtime_file.close();
}